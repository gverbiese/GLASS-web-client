'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc overview
 * @name glasswebuiApp
 * @description
 * # glasswebuiApp
 *
 * Main module of the application.
 */


/**
app.js is the default file name for storing the functions which doesn't belong to the different views
*/

var app = angular
  .module('glasswebuiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.filters',
    'angular.filter',
    'ui.grid',
    'ui.grid.selection', 
    'ui.grid.exporter',
    'ui.grid.autoResize',
    'ui.grid.moveColumns',
    'ui.grid.resizeColumns',
    'mgcrea.ngStrap',
    'mgcrea.ngStrap.datepicker',
    'ui.slider',
    'mgcrea.ngStrap.tab',
    'ui.layout',
    'angular-tour'
  ]);


  app.config(["$routeProvider", "$locationProvider", function ($routeProvider,$locationProvider) {
    $locationProvider.hashPrefix(''); // needed by the latest angularJS to make the routing works
    $routeProvider	
      .when('/site', {
        templateUrl: 'views/site.html',
        controller: 'SiteCtrl',
        controllerAs: 'site'
      })
      .when('/metadata/:querystring*', {
        templateUrl: 'views/metadata.html',
        controller: 'MetadataCtrl',
        controllerAs: 'metadata'
      })
      .when('/file/:querystring*', {
        templateUrl: 'views/file.html',
	controller: 'FileCtrl',
        controllerAs: 'file'
      })
      .when('/network/:querystring*', {
        templateUrl: 'views/network.html',
        controller: 'NetworkCtrl',
        controllerAs: 'network'
      })
      .otherwise({
        redirectTo: '/site'
      });
  }]);


// app.run start automatically at the beginning
 app.run(["$rootScope", "$window", "$cookies", "$uibModal", "serviceHistory", "$timeout", "$http", function($rootScope,$window,$cookies,$uibModal,serviceHistory,$timeout,$http) {
         
        /**************************************************
	* routeLogin function allows to handle authentification  
	***************************************************/
	$rootScope.routeLogin = function () {
            alert("Login");          
	};
        /*******************************************
	* routeHistory function allows to handle history  
	*******************************************/
	$rootScope.routeHistory = function () {
	   var cookies = $cookies.getAll();
           $rootScope.historydata = [];
           angular.forEach(cookies, function (v, k) {
                    var myDateStr= new Date(k);
                    if( ! isNaN ( myDateStr.getMonth() )) {
    			$rootScope.historydata.push(v);
		    }	
	   });
	   $rootScope.opendialog(); 
	};

	
	/******************************************************************************************
	* opendialog function allows to open a modal when the users click on "request query" button 
	*******************************************************************************************/
	$rootScope.opendialog = function() {
	    return $rootScope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalHistory.html',
		size: 'lg',
		scope: $rootScope,
                windowClass: 'center-modal'
	    });	
	};

	/***************************************
	* cancel function allows to cancel modal
	****************************************/
	$rootScope.cancel = function () {
    		$rootScope.modalInstance.dismiss("cancel");
  	};
	
	/*******************************************************
	* downloadText function allows to download text on modal
	*******************************************************/
	$rootScope.downloadText = function(){
                var mytext = $('#divid').text();
                var mystring = String(mytext);
                var out = mystring.replace(/http/g, '\n http');
		var blob = new Blob([out], {type: 'text/plain'});		
		if(window.navigator.msSaveOrOpenBlob) {//IE
			window.navigator.msSaveBlob(blob, "history.txt");
		}else{
			var elem = window.document.createElement('a');
			elem.href = window.URL.createObjectURL(blob);
			elem.download = "history.txt";        
			document.body.appendChild(elem);
			elem.click();        
			document.body.removeChild(elem);
		}
    	};
        
        /***********************************************
	* clearhistory function allows to clear history
	***********************************************/
	$rootScope.clearhistory = function() {
             $("#divid").html(""); 
	     serviceHistory.clearhistory(); //delete the cookie
	};

	/***********************************************
	* help function allows to show demonstrate tour
	***********************************************/
	$rootScope.help = function() { // initiate the start of the tour
             $rootScope.currentStep = 0;
             $timeout(function() {
             	angular.element('#myselector').triggerHandler('click');
	     }, 0);
	};

	/********************************************
	* tutoriel function allows to show the video
	********************************************/
	$rootScope.tutoriel = function() {
             return $rootScope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalVideo.html',
		size: 'lg',
		scope: $rootScope,
                windowClass: 'center-modal'
	    });	
	};
	/********************************************
	* doc function allows to show the document
	********************************************/
	$rootScope.doc = function() {
             return $rootScope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalDoc.html',
		size: 'lg',
		scope: $rootScope,
                windowClass: 'center-modal'
	    });	
	};
  }]);

/****************************************************************************************
* removeSpaces filter allows to delete all spaces in network name for id in input element
****************************************************************************************/
// code potentially unused now. don't delete before carefully checking

app.filter('removeSpaces', [function() {
    return function(string) {
        if (!angular.isString(string)) {
            return string;
        }
        string = string.replace('&', ''); 
        return string.replace(/[\s]/g, '');
    };
}]);


'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:SiteCtrl
 * @description
 * # SiteCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
 .controller('SiteCtrl', ["$scope", "serviceHistory", "serviceResource", "$uibModal", "uiGridConstants", "$window", "$compile", "$resource", "$location", "uiGridExporterService", "uiGridExporterConstants", "$templateCache", function ($scope, serviceHistory, serviceResource, $uibModal, uiGridConstants,$window,$compile,$resource,$location,uiGridExporterService
, uiGridExporterConstants, $templateCache) {
       

	/****************************************************************
	* loadResults function allows to load the short json to the grid 
	* and to place all markers in the map
	****************************************************************/
	$scope.loadResults = function () {
		//init $scope
		$scope.selected = true;//flag for advanced search button
		//values of filters in the grid
                $scope.markerselected = undefined;
		$scope.sitenameselected = undefined;
                $scope.latminselected = undefined;
                $scope.latmaxselected = undefined;
                $scope.lonminselected = undefined;
                $scope.lonmaxselected = undefined;
                $scope.altminselected = undefined;
                $scope.altmaxselected = undefined;
		$scope.networkselected = undefined;
                $scope.inversenetworkselected = undefined;
		$scope.fromdateminselected = undefined;
		$scope.fromdatemaxselected = undefined;
		$scope.todateminselected = undefined;
		$scope.todatemaxselected = undefined;
		$scope.agencyselected = undefined;
		$scope.countryselected = undefined;
		$scope.stateselected = undefined;
		$scope.cityselected = undefined;
		$scope.filterrowflag = false;//flag allow to know if we select or unselect station from the grid or the map 

		//values of the forms in the advanced search tab
		$scope.receivertypeselected = '';
		$scope.antennatypeselected = '';
		$scope.radometypeselected = '';
		$scope.satellitesystemselected = '';
		$scope.filefromdateselected = '';
		$scope.filetodateselected = '';
		$scope.publishfromdateselected = '';
		$scope.publishtodateselected = '';
                $scope.dataavailabilityselected = '';
		$scope.stationlifetimeselected = '';
                $scope.datasamplingselected = '';
                $scope.filetypeselected = '';
		$scope.statusfileselected = '';
		$scope.dataqualityselected = '';
                $scope.satellitesystemt3selected = '';
                $scope.filelatencyselected = '';
		$scope.typeofsignalselected = '';
		$scope.frequencyselected = '';
		$scope.channelselected = '';
		$scope.epochselected = '';
		$scope.elevangleselected = '';
		$scope.multipathselected = '';
		$scope.cycleslipsselected = '';
		$scope.ssprmsselected = '';
		$scope.clockjumpselected = '';
		$scope.activePanel = 0; // which panel from the advanced tab is selected
             
		$scope.networkStr = [];	// array of network, formated from $scope.networksdict
                $scope.simpleoradvanced = 'simple';//flag for advanced search or simple search
		
		//values of the forms in the geographic selection panel
		$scope.Radius = '';
		$scope.latitude = '';
		$scope.longitude = '';
		$scope.latLong = '';		
		$scope.North = '';
                $scope.Sud = '';
                $scope.West = '';
                $scope.East = '';
		$scope.slider = '';


		//values of the selection tools in the map (leaflet draw)
		$scope.layer = '';
                $scope.drawcircle = '';
		$scope.drawrectangle = '';
		$scope.drawpolygon = '';
		$scope.polygonpoints = [];// store selection polygon coordinates
		$scope.layernetworksdict = {};// dictionnary to manage the network dropbox in the map. key: network, value: id des markers?
		$scope.resultnetworksdict = {};// dictionnary to manage the networks in the popup. key: network, value:json


		$scope.markerselecteddict = {};//store the selected markers. Associated to $scope.layerid. key: marker, value: marker id on the map
		$scope.layerid = '';// store the marker id from the map. Associated to $scope.markerselecteddict	
                $scope.onlyonelayerid = false; // flag to store is only one marker or several are selected 
		
		$scope.networkentity = '';//Memorize the original color of the marker before selection
                $scope.results = ''; // json from the GLASS-API request
		$scope.loading = true; // flag for the spinner

		$scope.networksdict = {}; // dictionnary of networks from the json

        	$scope.firstload = true;// flag to check if it's the first load
                
                

		//load the map
                $scope.mymap = L.map('mapid').setView([51.505, -0.09], 3);
		L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
		
                //call gridHandler function to load the grid
		$scope.gridHandler();
		//first query to load all stations
		var query = serviceResource.site().query();
                $scope.loadingdialog();	
		query.$promise.then(function(data) {
			$scope.ajaxresults = data;//allow to store the original json to be reused if needed
			$scope.results = data;	// store the json to be processed	
			if ($scope.results!== '')
			{       
				$scope.currentdate = $scope.currentDateFormat();
				for (var i = 0; i < $scope.ajaxresults.length; i++) {
					if ($scope.ajaxresults[i]['date_to']===null)
					{
						$scope.ajaxresults[i]['date_to']=$scope.currentdate;
					}
				}
				//call all function regarding networks
				$scope.createColorNetworksDict($scope.results);
				$scope.createNetworkControl();
				//call markersHandler function to draw markers in the map with their caracteristics
				$scope.markersHandler($scope.results);
				//call a function to handle polygon,circle and rectangle button
				$scope.drawHandler();
                                $scope.addEraseDraw();
				$scope.switchmap();
				//initialize the grid data with what's we get from ajax response                
				$scope.gridOptions.data = $scope.results;
				$scope.gridApi.core.refresh();//update the grid
				
				$scope.modalInstance.opened.then(function () {
					$scope.loading = false;
                                	$scope.close();//stop the spinner loading
				});
				
			}
		});
		//create a watcher for radius of circle to follow the changement
		$scope.$watch("Radius", function(){
			$scope.changeRadiusCircle();
		});
		
        };
	
        /*************************************************************
	* loadingdialog function allows to open a modal with a spinner 
	**************************************************************/
	$scope.loadingdialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalLoading.html',
		size: 'm',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/*************************************************************************
	* createNetworkControl function handles networks drop down list on the map
	**************************************************************************/
	$scope.createNetworkControl= function () {
		var MyControl = L.Control.extend({
		    options: {
			position: 'topright'
		    },
		    onAdd: function (map) {
		    	var container = L.DomUtil.create('div', 'my-custom-control');
			container.innerHTML += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Networks <span class="caret"></span></button><ul class="dropdown-menu"> <li><input type="checkbox" value="select-all" id="select-all" checked ="checked"/> Select/Deselect All</li> <li ng-repeat="net in networkStr"><input type="checkbox" value="{{net}}" id="{{net|removeSpaces}}" checked ="checked" ng-click="networkFilter()"> <div class="circle" ng-style="{\'background-color\':\'{{colorNetworks[net].color}}\'}"></div>{{net}}<br/></li><li> <img src="images/black.png" height="15" width="15"> Deactivated Station<br /></li></ul>';
		    	       	
			$compile( container )($scope); // needed to comile html to template because 'ng-*' functions in the html
			return container;
		    }
		});
		$scope.mymap.addControl(new MyControl());
		$('.my-custom-control #select-all').click(function(event) {
			  if(this.checked) {
			      $('.my-custom-control .dropdown-menu li :checkbox').each(function() {
				  this.checked = true;
			      });
			      $scope.networkFilter();
			  }else {
			      $('.my-custom-control .dropdown-menu li :checkbox').each(function() {  
				  this.checked = false; 
			      });
		              $scope.networkFilter();
			  }
		});
	};
	
	/******************************************************************
	* loadAdvanced function allows to get the response from server and 
	* to fill the content in the listboxes  
	****************************************************************/
	$scope.loadAdvanced = function () {
                serviceResource.receiver().query(function(data){
			$scope.receiver_types = data;
		})
                serviceResource.antenna().query(function(data){
			$scope.antenna_types = data;
		})
                serviceResource.radome().query(function(data){
			$scope.radome_types = data;
		})
                serviceResource.filetype().query(function(data){
			$scope.filetype_result = data;
                        $scope.filetype = [];
                        angular.forEach($scope.filetype_result,function(item){
				$scope.filetype.push({"name":item.format + " - " + item.sampling_window + " - " + item.sampling_frequency});     
			});
		})
		$scope.satellitesystem = [{"name":"GPS"},{"name":"GLONASS"},{"name":"GALILEO"},{"name":"BEIDOU"},{"name":"SBAS"},{"name":"IRNSS"},{"name":"QZSS"}];
		$scope.satellitesystemt3 = [{"name":"GPS"},{"name":"GLONASS"},{"name":"GALILEO"},{"name":"BEIDOU"},{"name":"SBAS"},{"name":"IRNSS"},{"name":"QZSS"}];
		$scope.typeofsignal = [{"name":"P/C"},{"name":"L"},{"name":"D"},{"name":"S"}];
		$scope.frequencies = [{"name":"1"},{"name":"2"},{"name":"5"},{"name":"6"},{"name":"7"},{"name":"8"},{"name":"9"}];
		$scope.channel = [{"name":"No Attr"},{"name":"P"},{"name":"C"},{"name":"D"},{"name":"Y"},{"name":"M"},{"name":"N"},{"name":"A"},{"name":"B"},{"name":"C"},{"name":"I"},{"name":"Q"},{"name":"S"},{"name":"L"},{"name":"X"},{"name":"W"},{"name":"Z"}];
		$scope.statusfile = [{"name":"-3"},{"name":"-2"},{"name":"-1"},{"name":"0"},{"name":"1"},{"name":"2"},{"name":"3"}];     
	};

	
        /**********************************************************************************************
	* changedReferenceFromDate function allows to know what's users selected in "file date install"  
	**********************************************************************************************/
	$scope.changedReferenceFromDate = function(item) {
		if(item==undefined){
			$scope.filefromdateselected=undefined;
		}else{
	    		$scope.filefromdateselected = $scope.convertDateFormat(item);
		}
	};
        /*******************************************************************************************
	* changedReferenceToDate function allows to know what's users selected in "file date remove"  
	*******************************************************************************************/
	$scope.changedReferenceToDate = function(item) {
		if(item==undefined){
			$scope.filetodateselected=undefined;
		}else{
	    		$scope.filetodateselected = $scope.convertDateFormat(item);
		}
	};
	

	/*********************************************************************
	* routeMetadatalink function handles on click links to access metadata
	**********************************************************************/
	$scope.routeMetadatalink = function (code) {
		var param = "marker=";
		//open the view metadata with the parameter code
                var mypass = $window.open('#/metadata/'+ param + code,'_blank'); //'_blank' open on another page		
	};

	/*******************************************************************************
	* routeMetadataOrFiles function allows to access metadata or to access the files
	*******************************************************************************/
	$scope.routeMetadataOrFiles = function (MetadataOrFiles) {
		//1)Get all the criteria enter by the user
		var params = '';
                var selectedRows = $scope.gridApi.selection.getSelectedRows();
		if(selectedRows.length >= 1){//case row selected
				var codes='';
				angular.forEach(selectedRows,function(row){
					codes = codes + row.marker + ",";     
				});
				codes = codes.substring(0,codes.length-1);
				var param = "marker=";
				params = params + "&" + param + codes;
				//$scope.nodata = false;
		}else if(($scope.markerselected!==undefined)&&($scope.markerselected!==null)){//case filter 4 char id code
			    if($scope.markerselected.length>0){
				var param = "marker=";
				params = params + "&" + param + $scope.markerselected; 
			    }
		}
                if((selectedRows.length >= 1)||($scope.markerselected !==undefined)||($scope.sitenameselected !==undefined)||($scope.latminselected !==undefined)||($scope.latmaxselected !==undefined)||($scope.lonminselected !==undefined)||($scope.lonmaxselected !==undefined)||($scope.altminselected !==undefined)||($scope.altmaxselected !==undefined)||($scope.networkselected !==undefined)||($scope.inversenetworkselected !==undefined)||($scope.fromdateminselected !==undefined)||($scope.fromdatemaxselected !==undefined)||($scope.todateminselected !==undefined)||($scope.todatemaxselected !==undefined)||($scope.agencyselected !==undefined)||($scope.countryselected !==undefined)||($scope.stateselected !==undefined)||($scope.cityselected !==undefined)||($scope.receivertypeselected !=='')||($scope.antennatypeselected !=='')||($scope.radometypeselected !=='')||($scope.satellitesystemselected !=='')||($scope.drawcircle==='draw')||($scope.drawrectangle==='draw')||($scope.drawpolygon==='draw')||($scope.filefromdateselected !==undefined)||($scope.filetodateselected !==undefined)||($scope.dataavailabilityselected !==undefined)||($scope.stationlifetimeselected !==undefined)||($scope.filetypeselected !==undefined)||($scope.datasamplingselected !==undefined)||($scope.statusfileselected !==undefined)||($scope.typeofsignalselected !==undefined)||($scope.frequencyselected !==undefined)||($scope.channelselected !==undefined)||($scope.satellitesystemt3selected !==undefined)||($scope.epochselected !==undefined)||($scope.elevangleselected !==undefined)||($scope.multipathselected !==undefined)||($scope.cycleslipsselected !==undefined)||($scope.ssprmsselected !==undefined)||($scope.clockjumpselected !==undefined))
		{
			if($scope.drawcircle==='draw')
			{      
			    if(($scope.latitude!=='')&&($scope.longitude !=='')&&($scope.Radius !=='')){
				var param = "coordinates=circle";
				params = params + "&" + param + "&centerLat=" + $scope.latitude + "&centerLon=" + $scope.longitude + "&radius=" + $scope.Radius;
			    } 
			}
			if($scope.drawrectangle==='draw')
			{      
			    if(($scope.South!=='')&&($scope.North !=='')&&($scope.West !=='')&&($scope.East !=='')){
				var param = "coordinates=rectangle";
				params = params + "&" + param + "&minLat=" + $scope.South + "&maxLat=" + $scope.North + "&minLon=" + $scope.West + "&maxLon=" + $scope.East;
			    } 
			}
			if($scope.drawpolygon==='draw')
			{      
			    if($scope.polygonpoints.length>0){
				var param = "coordinates=polygon";
				var polygoncoord = '';
				var i = 0;
                                while(i<$scope.polygonpoints.length-1)
				{
					if(MetadataOrFiles==="Metadata")
					{
						polygoncoord = polygoncoord + $scope.polygonpoints[i] + "," + $scope.polygonpoints[i+1] + ";";
					}else if(MetadataOrFiles==="Files"){
						polygoncoord = polygoncoord + $scope.polygonpoints[i] + "!" + $scope.polygonpoints[i+1] + ",";
					}
					i = i + 2; 
				}
				polygoncoord = polygoncoord.substring(0,polygoncoord.length-1);  
				params = params + "&" + param + "&polygon=" + polygoncoord;
			    } 
			}
			if(($scope.sitenameselected!==undefined)&&($scope.sitenameselected!==null))
			{      
			    if($scope.sitenameselected.length>0){//check if there's something in the grid because the grid keeps in memory null value if we deleted the value in filter 
				var param = "site=";
				params = params + "&" + param + $scope.sitenameselected;
			    } 
			}if((($scope.latminselected!==undefined)&&($scope.latminselected!==null))||(($scope.latmaxselected!==undefined)&&($scope.latmaxselected!==null)))
			{
				var latitudes = '';
				if(($scope.latminselected!==undefined)&&($scope.latminselected!==null))
				{
					if($scope.latminselected.length>0){
						latitudes = $scope.latminselected;
					}
				}
                                if(($scope.latmaxselected!==undefined)&&($scope.latmaxselected!==null))
				{	if($scope.latmaxselected.length>0){
						latitudes = latitudes + "," + $scope.latmaxselected;
					}
				}
				if(latitudes!=''){
					var param = "latitude=";
					params = params + "&" + param + latitudes;
				}              
			}
			
			if((($scope.lonminselected!==undefined)&&($scope.lonminselected!==null))||(($scope.lonmaxselected!==undefined)&&($scope.lonmaxselected!==null)))
			{   
			    	var longitudes = '';   
			    	if(($scope.lonminselected!==undefined)&&($scope.lonminselected!==null))
			    	{
					if($scope.lonminselected.length>0){
	                        		longitudes = $scope.lonminselected;
					}
			    	}
				if(($scope.lonmaxselected!==undefined)&&($scope.lonmaxselected!==null))
			    	{
					if($scope.lonmaxselected.length>0){
	                        		longitudes = longitudes + "," + $scope.lonmaxselected;
					}
			    	}
				if(longitudes!=''){
				    	var param = "longitude=";
					params = params + "&" + param + longitudes;
				}
			}
			
			if((($scope.altminselected!==undefined)&&($scope.altminselected!=null))||(($scope.altmaxselected!==undefined)&&($scope.altmaxselected!=null)))
			{
			    	var altitudes = '';      
			    	if(($scope.altminselected!==undefined)&&($scope.altminselected!=null))
			        {
					if($scope.altminselected.length>0){
	                        		altitudes = $scope.altminselected;
					}
				}
				if(($scope.altmaxselected!==undefined)&&($scope.altmaxselected!=null))
			        {
					if($scope.altmaxselected.length>0){
	                        		altitudes = altitudes + "," + $scope.altmaxselected;
					}
				}
				if(altitudes!=''){
					var param = "altitude=";
					params = params + "&" + param + altitudes;
				} 
			}
			
			if(($scope.networkselected!==undefined)&&($scope.networkselected!=null))
			{
			    if($scope.networkselected.length>0){	
				var param = "network=";
				params = params + "&" + param + $scope.networkselected; 
		  	    }
			}if(($scope.inversenetworkselected!==undefined)&&($scope.inversenetworkselected!=null))
			{
			    if($scope.inversenetworkselected.length>0){	
				var param = "invertedNetworks=";
				params = params + "&" + param + $scope.inversenetworkselected; 
		  	    }
			}
			if(($scope.fromdateminselected!==undefined)&&($scope.fromdateminselected!=null))
			{
				var param = "installedDateMin=";
				params = params + "&" + param + $scope.fromdateminselected;
			}
			if(($scope.fromdatemaxselected!==undefined)&&($scope.fromdatemaxselected!=null))
			{
				var param = "installedDateMax=";
				params = params + "&" + param + $scope.fromdatemaxselected;
			}
			
			if(($scope.todateminselected!==undefined)&&($scope.todateminselected!=null))
			{
				var param = "removedDateMin=";
				params = params + "&" + param + $scope.todateminselected;
			}
			if(($scope.todatemaxselected!==undefined)&&($scope.todatemaxselected!=null))
			{
				var param = "removedDateMax=";
				params = params + "&" + param + $scope.todatemaxselected;
			}
			if(($scope.agencyselected!==undefined)&&($scope.agencyselected!=null))
			{
			   if($scope.agencyselected.length>0){	
				var param = "agency=";
				params = params + "&" + param + $scope.agencyselected; 
                            }
			}if(($scope.countryselected!==undefined)&&($scope.countryselected!=null))
			{
			   if($scope.countryselected.length>0){	
				var param = "country=";
				params = params + "&" + param + $scope.countryselected;
                           }
			}if(($scope.stateselected!==undefined)&&($scope.stateselected!=null))
			{
			   if($scope.stateselected.length>0){	
				var param = "state=";
				params = params + "&" + param + $scope.stateselected;
                           } 
			}if(($scope.cityselected!==undefined)&&($scope.cityselected!=null))
			{
			   if($scope.cityselected.length>0){	
				var param = "city=";
				params = params + "&" + param + $scope.cityselected;
                            } 
			}
			if($scope.receivertypeselected.length >= 1)
			{	
				var receiver='';
				angular.forEach($scope.receivertypeselected,function(item){
					receiver = receiver + item.name + ",";     
				});
                                receiver = receiver.substring(0,receiver.length-1);
				var param = "receiver=";
				params = params + "&" + param + receiver;  
			}if($scope.antennatypeselected.length >= 1)
			{	
				var antenna='';
				angular.forEach($scope.antennatypeselected,function(item){
					antenna = antenna + item.name + ",";
				});
				antenna = antenna.substring(0,antenna.length-1);
				var param = "antenna=";
				params = params + "&" + param + antenna; 
			}if($scope.radometypeselected.length >= 1)
			{	
				var radome='';
				angular.forEach($scope.radometypeselected,function(item){
					radome = radome + item.name + ",";     
				});
				radome = radome.substring(0,radome.length-1);
				var param = "radome=";
				params = params + "&" + param + radome; 
			}if($scope.satellitesystemselected.length >= 1)
			{	
				var satellite='';
				angular.forEach($scope.satellitesystemselected,function(item){
					satellite = satellite + item.name.substr(0, 3) + ",";     
				});
				satellite = satellite.substring(0,satellite.length-1);
				var param = "satelliteSystem=";
				params = params + "&" + param + satellite;
			}if(($scope.filefromdateselected !==undefined)||($scope.filetodateselected !==undefined))
			{
				var filedate = ''; 
				if($scope.filefromdateselected !==undefined)
				{
					if ($scope.filefromdateselected.length >= 1)
					{
						filedate = $scope.filefromdateselected + ",";
					}	
					
				}
				if($scope.filetodateselected !==undefined)
				{
					if ($scope.filetodateselected.length >= 1)
					{
						if (filedate.substring(filedate.length-1)==",")
						{
							filedate = filedate + $scope.filetodateselected;
						}else{
							filedate = filedate +  "," + $scope.filetodateselected;
						}
					}	
					
				}
				if(filedate!=''){ 
					var param = "dateRange=";
					params = params + "&" + param + filedate;
				}
			}if($scope.dataavailabilityselected.length >= 1)
			{	
				var param = "dataAvailability=";
				params = params + "&" + param + $scope.dataavailabilityselected; 
			}if($scope.stationlifetimeselected.length >= 1)
			{	
				var param = "minimumObservationYears=";
				params = params + "&" + param + $scope.stationlifetimeselected; 
			}if($scope.filetypeselected.length >= 1)
			{	
				var filetype='';
                                var sampling_window='';
                                var sampling_frequecy='';
				angular.forEach($scope.filetypeselected,function(item){
					var item_split = item.name.split(" - ");
					filetype = filetype + item_split[0] + ",";
                                        sampling_window = sampling_window + item_split[1] + ",";
					sampling_frequecy = sampling_frequecy + item_split[2] + ",";     
				});
				filetype = filetype.substring(0,filetype.length-1);
				sampling_window = sampling_window.substring(0,sampling_window.length-1);
				sampling_frequecy = sampling_frequecy.substring(0,sampling_frequecy.length-1);
				var param1 = "fileType=";
				var param2 = "samplingWindow=";
				var param3 = "samplingFrequency=";
				params = params + "&" + param1 + filetype + "&" + param2 + sampling_window + "&" + param3 + sampling_frequecy; 
			}if($scope.statusfileselected.length >= 1)
			{	
				var statusfiles='';
				angular.forEach($scope.statusfileselected,function(item){
					statusfiles = statusfiles + item.name + ",";     
				});
				statusfiles = statusfiles.substring(0,statusfiles.length-1);
				var param = "statusfile=";
				params = params + "&" + param + statusfiles;
			}if($scope.typeofsignalselected.length >= 1)
			{	
				var typofsignal='';
				angular.forEach($scope.typeofsignalselected,function(item){
					typofsignal = typofsignal + item.name + ",";     
				});
				typofsignal = typofsignal.substring(0,typofsignal.length-1);
		
				var param = "observationtype=";
				params = params + "&" + param + typofsignal; 
			}if($scope.frequencyselected.length >= 1)
			{	
				var frequency='';
				angular.forEach($scope.frequencyselected,function(item){
					frequency = frequency + item.name + ",";     
				});
				frequency = frequency.substring(0,frequency.length-1);
		
				var param = "frequency=";
				params = params + "&" + param + frequency; 
			}if($scope.channelselected.length >= 1)
			{	
				var channel='';
				angular.forEach($scope.channelselected,function(item){
					channel = channel + item.name + ",";     
				});
				channel = channel.substring(0,channel.length-1);
		
				var param = "channel=";
				params = params + "&" + param + channel; 
			}if($scope.satellitesystemt3selected.length >= 1)
			{	
				var constellation='';
				angular.forEach($scope.satellitesystemt3selected,function(item){
					constellation = constellation + item.name + ",";     
				});
				constellation = constellation.substring(0,constellation.length-1);
		
				var param = "constellation=";
				params = params + "&" + param + constellation; 
			}if($scope.epochselected.length > 0)
			{	
				var param = "ratioepoch=";
				params = params + "&" + param + $scope.epochselected; 
			}if($scope.elevangleselected.length > 0)
			{	
				var param = "elevangle=";
				params = params + "&" + param + $scope.elevangleselected; 
			}if($scope.multipathselected.length > 0)
			{	
				var param = "multipathvalue=";
				params = params + "&" + param + $scope.multipathselected; 
			}if($scope.cycleslipsselected.length > 0)
			{	
				var param = "nbcycleslips=";
				params = params + "&" + param + $scope.cycleslipsselected; 
			}if($scope.ssprmsselected.length > 0)
			{	
				var param = "spprms=";
				params = params + "&" + param + $scope.ssprmsselected; 
			}if($scope.clockjumpselected.length > 0)
			{	
				var param = "nbclockjumps=";
				params = params + "&" + param + $scope.clockjumpselected; 
			}
			if (params!==''){    
					//2)open the view with criteria enter
					params = params.substr(1);
					if(MetadataOrFiles==="Metadata")
					{
						var mypass = $window.open('#/metadata/' + params,'_blank');
					}else if(MetadataOrFiles==="Files"){
						var mypass = $window.open('#/file/' + params,'_blank');
					}
                                        
			}else{
				$scope.openmessage($scope.messageinfogrid);
			}
		}else{
			$scope.openmessage($scope.messageinfogrid);	
		}
				
			        
    	};

	/**************************************************************
	* findUnique function allows to return array with unique values
	***************************************************************/
	$scope.findUnique = function (arr) {
	    var distinct=[];

	    for(var i=0;i<arr.length;i++)
		{
		var str=arr[i];
		if(distinct.indexOf(str)===-1)
		    {
		    distinct.push(str);
		    }
		}
             return distinct;
             
	};

	/********************************************************************************************************************
	* createColorNetworksDict function allows to have a dictionary of color for each network and get an array of networks
	********************************************************************************************************************/
        $scope.createColorNetworksDict = function (data){
		var dicttemp = {};
		var arrtemp = [];
		var networkStr = ''; 
            	var arrcolor = ["#FFFF00","#0000FF","#FF0000","#ED7F10","#FD6C9E","#FF00FF","#00FE7E","#79F8F8","#660099",
"#FCDC12","#EDFF0C","#AD4F09","#007FFF","#F4661B","#DE3163","#F9429E","#16B84E","#54F98D","#6600FF",
"#D2CAEC","#2E006C","#FEBFD2","#FF007F","#F88E55","#318CE7","#73C2FB","#FFFF6B","#F7230C","#D473D4",
"#E0115F","#01796F","#6C0277","#9E0E40","#9EFD38","#22780F","#FF4901","#B3B191","#83A697","#DF6D14",
"#00CCCB","#2C75FF","#FEFEE2","#E7F00D","#DC143C","#997A8D","#25FDE9","#0131B4","#F0C300","#00561B","#E00DEF"];
			
		var k = 0;
		for(var i=0;i<data.length;i++){
			networkStr = data[i].network.name;
			arrtemp.push(networkStr);
			if (!(networkStr in dicttemp)){
				dicttemp[networkStr] = {"color":arrcolor[k]}
				k = k +1;
		    }
			networkStr = '';
		}
                $scope.colorNetworks = dicttemp;
		$scope.networkStr = $scope.findUnique(arrtemp);		
	};

        /*************************************************************************
	* convertDateFormat function allows to convert a date in format yyyy-mm-dd
	**************************************************************************/
	$scope.convertDateFormat = function(date) {
	    var mydate = new Date(date);
	    var dd = mydate.getDate();
	    var mm = mydate.getMonth()+1;
	    var yyyy = mydate.getFullYear();
	    if(dd<10){
		dd='0'+dd
	    } 
	    if(mm<10){
		mm='0'+mm
	    }
	       
	    return mydate = yyyy+'-'+mm+'-'+dd;
	};
	
	/**************************************************************************************
	* currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
	**************************************************************************************/
	$scope.currentDateFormat = function() {
	    var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1;
	    var HH = today.getHours();
	    var MM = today.getMinutes();
	    var SS = today.getSeconds();
	    var yyyy = today.getFullYear();
	    if(dd<10){
		dd='0'+dd
	    } 
	    if(mm<10){
		mm='0'+mm
	    }
	    if (HH < 10) {
		HH = "0" + HH;
	    }
	    if (MM < 10) {
		MM = "0" + MM;
	    }
	    if (SS < 10) {
		SS = "0" + SS;
	    }   
	    return today = yyyy+'-'+mm+'-'+dd+' '+HH+':'+MM+':'+SS;
	};

	/******************************************************************************************
	* opendialog function allows to open a modal when the users click on "request query" button 
	*******************************************************************************************/
	$scope.opendialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalRequest.html',
		size: 'lg',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/***************************************
	* cancel function allows to cancel modal
	****************************************/
	$scope.cancel = function () {
    		$scope.modalInstance.dismiss("cancel");
  	};
	
	/************************************************
	* messageinfogrid variable to grid error message
	************************************************/
	$scope.messageinfogrid = {	
    		textAlert : "You have to select at least one thing in grid or in lisboxes",
    		mode : 'info'
	  };
	/*******************************************************
	* messageinfonodata variable to advanced error message
	********************************************************/
	$scope.messageinfonodata = {
	    textAlert : "No data available",
	    mode : 'info'
	  };
        /**************************************************
	* messageinfomissing variable to grid error message
	**************************************************/
	$scope.messageinfomissing = {	
    		textAlert : "You have to specify at least one station in the criteria",
    		mode : 'info'
	  };
	/*****************************************************
	* messageinfonopossible variable to grid error message
	****************************************************/
	$scope.messageinfonopossible = {	
    		textAlert : "You could not fill both date range and publish date",
    		mode : 'info'
	  };
  	/*******************************************************************
	* openmessage function allows to open a modal when there's an error
	*******************************************************************/
	$scope.openmessage = function(message) {
		return $scope.modalInstance = $uibModal.open({
	      	      templateUrl: 'views/messageBox.html',
                      scope: $scope,
		      size: 'lg',
		      resolve: {
			data: function () {
				$scope.messageinfo = message;
			  	return $scope.messageinfo;
			}
		      }
		 });
	};

	/**************************************
	* close function allows to close modal
	***************************************/
	$scope.close = function(){
	    $scope.modalInstance.close();
	};
	
	//Potentially deprecated
	/******************************************************************************
	* highlightFilteredHeader function handles grid highlight filter header style
	*******************************************************************************/
	$scope.highlightFilteredHeader = function( row, rowRenderIndex, col, colRenderIndex ) {
	    if( col.filters[0].term ){
	      return 'header-filtered';
	    } else {
	      return '';
	    }
	  };
	//Potentially deprecated
	/***************************************************************************
	* downloadPdf function allows to exporter selected row in grid in format pdf
	***************************************************************************/
	$scope.downloadPdf = function() {
	  var grid = $scope.gridApi.grid;
	  var rowTypes = uiGridExporterConstants.SELECTED;
	  var colTypes = uiGridExporterConstants.SELECTED;
	  uiGridExporterService.pdfExport(grid, rowTypes, colTypes);
	};
	
	

	/*****************************************
	* gridHandler function handles grid part
	******************************************/
	$scope.gridHandler = function(data) {
		//In the short json and the full json the field country is not defined same thing 
		if($scope.simpleoradvanced==='simple')//case short json
		{
			$scope.country = 'location.country.name';
		}else if($scope.simpleoradvanced==='advanced')//case full json
		{
			$scope.country = 'location.city.state.country.name';
		}
                 
		//custom selection on grid with checkbox
		$templateCache.put('ui-grid/selectionRowHeaderButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-row-selected': row.isSelected}\" ><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"row.isSelected\" ng-click=\"row.isSelected=!row.isSelected;selectButtonClick(row, $event)\">&nbsp;</div>"
  );

  		$templateCache.put('ui-grid/selectionSelectAllButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-all-selected': grid.selection.selectAll}\" ng-if=\"grid.options.enableSelectAll\"><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"grid.selection.selectAll\" ng-click=\"grid.selection.selectAll=!grid.selection.selectAll;headerButtonClick($event)\"></div>"
  ); 

		//define all grid options and their columns
		$scope.gridOptions = {
                        enableGridMenu: false,
		    	enableFiltering: true,
			enableSorting: true,
			enableRowSelection: true,
			enableSelectAll: true,
			multiSelect: true,
                        exporterMenuCsv: false,
			exporterMenuPdf: false,
                        enableColumnMoving: true,
                        enableColumnResizing: true, 
                        showGridFooter: true,
        		//gridFooterTemplate: "<div>Total Items: {{grid.appScope.totalItems}}</div>",
			//all function regarding the ui-grid
                        onRegisterApi: function(gridApi){ 
      				$scope.gridApi = gridApi;
				//function allows to get all the marker displayed in the grid and then display them into the map
				$scope.gridApi.core.on.rowsRendered($scope, function() {
					if($scope.filterrowflag === true){
						$scope.filteredRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
						var resulttemp = [];
                                                var networktemp = [];
						for(var i=0;i<$scope.filteredRows.length;i++)
						{
							resulttemp.push($scope.filteredRows[i].entity);
                                                        networktemp.push($scope.filteredRows[i].entity.network.name);
						}
					        
						$scope.mymap.eachLayer(function (layer) { 
							
                                                        if(layer.hasOwnProperty("_latlng")&&layer!=$scope.layer){//test to avoid to delete tiles 
								$scope.mymap.removeLayer(layer);
							}
						});
						
						if (resulttemp.length >0){
							$scope.markersHandler(resulttemp);
						}
 						
						if (networktemp.length >0){
							
							var networkuniquearray = $scope.findUnique(networktemp);
                                                        
							$('.my-custom-control .dropdown-menu li input:checked').each(function() {
							   
							    this.checked = false;
							   
							});
							
							for(var i=0;i<networkuniquearray.length;i++)
							{
								var networkname = networkuniquearray[i].replace('&', '');
								networkname = networkname.replace(/[\s]/g, '');
								
								$( '#'+ networkname ).prop('checked', true);
							}
													
							if(networkuniquearray.length===Object.keys($scope.layernetworksdict).length)
							{								        
								$( '#select-all' ).prop('checked', true);	
							}
						}else{
							$('.my-custom-control .dropdown-menu li input:checked').each(function() {
							   
							    this.checked = false;
							   
							});
							
						}
					}
				});
				
				//function allows to get what's the user writed in the filter, this content will store in the different $scope
				$scope.gridApi.core.on.filterChanged($scope, function () {
				     $scope.filterrowflag = true;
				     var grid = this.grid;
				     if($scope.drawrectangle == 'draw'){
							$scope.gridOptions.data = $scope.results;
							$scope.gridApi.core.refresh();
							setTimeout(function(){
							        var newDataList = []; 
								var bounds = L.latLngBounds($scope.layer._latlngs[0], $scope.layer._latlngs[2]);
								$scope.mymap.eachLayer(function (icon) {//loop throw the markers to check all markers inside the rectangle
									if(icon.hasOwnProperty("_latlng")){
										var layerlatlong = icon.getLatLng();
										if (bounds.contains(layerlatlong))
										{
											newDataList.push(icon.result);
										}
									}	
								});
								
								$scope.gridOptions.data = newDataList;//new data for the grid
								$scope.filterrowflag = false;
								$scope.gridApi.core.refresh();//allow to refresh the grid 
							},100);
				     }
				     if($scope.drawcircle == 'draw'){
							$scope.gridOptions.data = $scope.results;
							$scope.gridApi.core.refresh();
							setTimeout(function(){
							        var newDataList = []; 
								$scope.mymap.eachLayer(function (icon) {//loop throw the markers to check all markers inside the radius
									if(icon.hasOwnProperty("_latlng")){
										var layerlatlong = icon.getLatLng();
										var distancefromcenterPoint = layerlatlong.distanceTo($scope.latLong);//$scope.latLong is the center
										if (distancefromcenterPoint <= $scope.layer._mRadius)
										{
											newDataList.push(icon.result);
										}
									}	
								});
								
								$scope.gridOptions.data = newDataList;//new data for the grid
								$scope.filterrowflag = false;
								$scope.gridApi.core.refresh();//allow to refresh the grid 
							},100);
				     }
				     if($scope.drawpolygon == 'draw'){
							$scope.gridOptions.data = $scope.results;
							$scope.gridApi.core.refresh();
							setTimeout(function(){
								var newDataList = []; 
								for (var key in $scope.networksdict)
								{
									for(var i=0;i<$scope.networksdict[key].length;i++)
									{
										if ($scope.isMarkerInsidePolygon($scope.networksdict[key][i],$scope.layer))//call the function to check it
										{
											newDataList.push($scope.networksdict[key][i].result);
										}
									}
								}
								for(var i=0;i<$scope.layer._latlngs.length;i++)
								{
									$scope.polygonpoints.push($scope.layer._latlngs[i].lat.toFixed(6));
									$scope.polygonpoints.push($scope.layer._latlngs[i].lng.toFixed(6));
								}	
								$scope.gridOptions.data = newDataList;//new data for the grid
								$scope.filterrowflag = false;
								$scope.gridApi.core.refresh();//allow to refresh the grid  
							},100);
				     }
				     $.each(grid.columns, function (index, column) {
					   switch (column.field) {
						case 'marker':
						     $scope.markerselected = column.filters[0].term;
						     break;
						case 'name':
						     $scope.sitenameselected = column.filters[0].term;
						     break;
						case 'location.coordinates.lat':
						     $scope.latminselected = column.filters[0].term;
						     $scope.latmaxselected = column.filters[1].term;
						     break;
						case 'location.coordinates.lon':
						     $scope.lonminselected = column.filters[0].term;
                                                     $scope.lonmaxselected = column.filters[1].term;
						     break;
						case 'location.coordinates.altitude':
						     $scope.altminselected = column.filters[0].term;
						     $scope.altmaxselected = column.filters[1].term;
						     break;
						case 'date_from':
						     $scope.fromdateminselected = column.filters[0].term;
						     $scope.fromdatemaxselected = column.filters[1].term;
						     break;
						case 'date_to':
						     $scope.todateminselected = column.filters[0].term;
						     $scope.todatemaxselected = column.filters[1].term;
						     break;
						case 'location.country.name':
						     $scope.countryselected = column.filters[0].term;
						     break;
						case 'location.state.name':
						     $scope.stateselected = column.filters[0].term;
						     break;
						case 'location.city.name':
						     $scope.cityselected = column.filters[0].term;
						     break;
						case 'agency.name':
						     $scope.agencyselected = column.filters[0].term;
						     break;
                                                case 'network.name':
						     $scope.networkselected = column.filters[0].term;
                                                     $scope.inversenetworkselected = column.filters[1].term;
						     break;
					   }
				    });
					
				});
				//function allows to get the row selected in the grid and display the corresponding marker into the map with a circle bigger
				$scope.gridApi.selection.on.rowSelectionChanged($scope, function(row){ 
					if(row.entity.date_to===$scope.currentdate)
                                        {
						$scope.mymap.eachLayer(function(layer) {
						if(layer.hasOwnProperty("_latlng")){
							if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
							{
							
								var defaultoption = {
									    		radius: 7,
                   									fillColor: $scope.colorNetworks[row.entity.network.name].color,
											color: "#000",
											weight: 1,
											opacity: 1,
											fillOpacity: 0.8
									};
								var mylayerid = layer._leaflet_id;
								if (row.isSelected){ 
									var highlight = {
									    		radius: 12,
											fillColor: "#2CDBF6",
											color: "#000",
											weight: 1,
											opacity: 1,
											fillOpacity: 0.8
									};
									$scope.mymap._layers[mylayerid].setStyle(highlight);
									$scope.mymap._layers[mylayerid].bringToFront();
									$scope.markerselecteddict[mylayerid] = row.entity.marker;//store the marker in the dictionnary to use later
								}else{
									$scope.mymap._layers[mylayerid].setStyle(defaultoption);
									delete $scope.markerselecteddict[mylayerid];
								}
							
							}
						}	
						});
					}
					
				});
				//function allows to get several row selected in the grid and display them into the map with a circle bigger
				 $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
					angular.forEach(rows, function(row, key) {
					 	if(row.entity.date_to===$scope.currentdate)
		                                {
							$scope.mymap.eachLayer(function(layer) {
							if(layer.hasOwnProperty("_latlng")){
								if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
								{
							
									var defaultoption = {
										    		radius: 7,
												fillColor: $scope.colorNetworks[row.entity.network.name].color,
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
										};
									var mylayerid = layer._leaflet_id;
									if (row.isSelected){ 
										var highlight = {
										    		radius: 12,
												fillColor: "#2CDBF6",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
										};
										$scope.mymap._layers[mylayerid].setStyle(highlight);
										$scope.mymap._layers[mylayerid].bringToFront();
										$scope.markerselecteddict[mylayerid] = row.entity.marker;
									}else{
										$scope.mymap._layers[mylayerid].setStyle(defaultoption);
										delete $scope.markerselecteddict[mylayerid];
									}
							
								}
							}	
							});
						}
					});	
				});

    			},
			//definition of each column in the grid
			columnDefs: [
				  { name:'Marker', field: 'marker',  enableColumnMenu: false,headerCellClass:'colorheader',
		                                                        filter: {
										  condition:  function(searchTerm, cellValue) {//function handles regular expression
										      		      var separators = [','];
											      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
											      	      var bReturnValue = false;
												      for(var iIndex in strippedValue){
												    		var sValueToTest = strippedValue[iIndex];
												   		if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
												      		bReturnValue = true;
												      }
											    	return bReturnValue;
										  	      }
				  },cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.routeMetadatalink(row.entity.marker)">{{ COL_FIELD }}</a></div>'},
				  { name:'Site name', field: 'name', enableColumnMenu: false,headerCellClass:'colorheader', 
                                                                        filter: {
										  condition:  function(searchTerm, cellValue) {
										      		      var separators = [','];
											      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
											      	      var bReturnValue = false;
												      for(var iIndex in strippedValue){
												    		var sValueToTest = strippedValue[iIndex];
												   		if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
												      		bReturnValue = true;
												      }
											    	return bReturnValue;
										  	      }
                                  						}},
				  { name:'Lat', field: 'location.coordinates.lat', type: 'number', cellFilter: 'number: 3', enableColumnMenu: false,headerCellClass:'colorheader', 
		                                                        filters: [
										{
										  condition: uiGridConstants.filter.GREATER_THAN,
										  placeholder: 'greater than'
										},
										{
										  condition: uiGridConstants.filter.LESS_THAN,
										  placeholder: 'less than'
										}
									      ]},
		                  { name:'Lon', field: 'location.coordinates.lon', type: 'number', cellFilter: 'number: 3', enableColumnMenu: false,headerCellClass:'colorheader', 
		                                                        filters: [
										{
										  condition: uiGridConstants.filter.GREATER_THAN,
										  placeholder: 'greater than'
										},
										{
										  condition: uiGridConstants.filter.LESS_THAN,
										  placeholder: 'less than'
										}
									      ]},
		                  { name:'Alt', field: 'location.coordinates.altitude', type: 'number', cellFilter: 'number: 3', enableColumnMenu: false,headerCellClass:'colorheader', 
		                                                        filters: [
										{
										  condition: uiGridConstants.filter.GREATER_THAN,
										  placeholder: 'greater than'
										},
										{
										  condition: uiGridConstants.filter.LESS_THAN,
										  placeholder: 'less than'
										}
									      ]},
		                  { name:'Install Date', field: 'date_from',  enableColumnMenu: false,headerCellClass:'colorheader', 
		                                                        filters: [{condition: function(term, value){
														      if (!term) return true;
														      var valueDate = new Date(value);
														      var replaced = term.replace(/\\/g,'');
														      var termDate = new Date(replaced);
														      return valueDate > termDate;
														  },placeholder:'greater than'},
										 {condition: function(term, value){
														      if (!term) return true;
														      var valueDate = new Date(value);
														      var replaced = term.replace(/\\/g,'');
														      var termDate = new Date(replaced);
														      return valueDate < termDate;
														  },placeholder:'less than'}
										]},
		                  { name:'End Date', field: 'date_to', enableColumnMenu: false,headerCellClass:'colorheader', 
		                                                        filters: [{condition: function(term, value){
														      if (!term) return true;
														      var valueDate = new Date(value);
														      var replaced = term.replace(/\\/g,'');
														      var termDate = new Date(replaced);
														      return valueDate > termDate;
														  },placeholder:'greater than'},
										{condition: function(term, value){
														      if (!term) return true;
														      var valueDate = new Date(value);
														      var replaced = term.replace(/\\/g,'');
														      var termDate = new Date(replaced);
														      return valueDate < termDate;
														  },placeholder:'less than'}],
									cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) 
										{
											if (grid.getCellValue(row ,col) == $scope.currentdate) 
											 {
												return 'white';
											 }
		                                                                      
			        						}
		                  },
				  { name:'Country', field: 'location.country.name' , enableColumnMenu: false,headerCellClass:'colorheader',
		                                                        filter: {
										  condition:  function(searchTerm, cellValue) {
										      		      var separators = [','];
											      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
											      	      var bReturnValue = false;
												      for(var iIndex in strippedValue){
												    		var sValueToTest = strippedValue[iIndex];
												   		if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
												      		bReturnValue = true;
												      }
											    	return bReturnValue;
										  	      }
				  						}},
                                  { name:'State', field: 'location.state.name' , enableColumnMenu: false,headerCellClass:'colorheader',
		                                                        filter: {
										  condition:  function(searchTerm, cellValue) {
										      		      var separators = [','];
											      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
											      	      var bReturnValue = false;
												      for(var iIndex in strippedValue){
												    		var sValueToTest = strippedValue[iIndex];
												   		if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
												      		bReturnValue = true;
												      }
											    	return bReturnValue;
										  	      }
				  						}},
				  { name:'City', field: 'location.city.name' , enableColumnMenu: false,headerCellClass:'colorheader',
		                                                        filter: {
										  condition:  function(searchTerm, cellValue) {
										      		      var separators = [','];
											      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
											      	      var bReturnValue = false;
												      for(var iIndex in strippedValue){
												    		var sValueToTest = strippedValue[iIndex];
												   		if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
												      		bReturnValue = true;
												      }
											    	return bReturnValue;
										  	      }
				  						}},
				  { name:'Agency', field: 'agency.name' , enableColumnMenu: false,headerCellClass:'colorheader',
		                                                        filter: {
										  condition:  function(searchTerm, cellValue) {
										      		      var separators = [','];
											      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
											      	      var bReturnValue = false;
												      for(var iIndex in strippedValue){
												    		var sValueToTest = strippedValue[iIndex];
												   		if (cellValue.toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
												      		bReturnValue = true;
												      }
											    	return bReturnValue;
										  	      }
				  						}
				   },
				   { name:'Network', field: 'network.name', enableColumnMenu: false,headerCellClass:'colorheader', 
		                                                        filters: [{
										  condition:  function(searchTerm, cellValue) {
												      var networkslist = cellValue.split(" & ");
												      var bReturnValue = false;
												      for (var i=0; i < networkslist.length; i++){ 
											      	      	      var separators = [','];
												      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
													      for(var iIndex in strippedValue){
													    		var sValueToTest = strippedValue[iIndex];
													   		if (networkslist[i].toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
													      		bReturnValue = true;
													      }
													}
											    	return bReturnValue;
										  	      }
				  						},{
										  condition:  function(searchTerm, cellValue) {
												      var networkslist = cellValue.split(" & ");
												      var bReturnValue = true;
												      for (var i=0; i < networkslist.length; i++){ 
											      		      var separators = [','];
												      	      var strippedValue = searchTerm.split(new RegExp(separators, 'g'));
													      for(var iIndex in strippedValue){
													    		var sValueToTest = strippedValue[iIndex];
													   		if (networkslist[i].toLowerCase().indexOf(sValueToTest.toLowerCase()) == 0)
													      		bReturnValue = false;
													      }
													}
											    	return bReturnValue;
										  	      },
                                                                                  placeholder: 'inverse filter'
				  						}],cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[0])">{{ grid.appScope.splitNetworklink(row.entity.network.name)[0] }}</a> <a id="gridlink" ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[1])">{{ grid.appScope.splitNetworklink(row.entity.network.name)[1] }}</a> <a id="gridlink" ng-click="grid.appScope.routeNetworklink(grid.appScope.splitNetworklink(row.entity.network.name)[2])">{{ grid.appScope.splitNetworklink(row.entity.network.name)[2] }}</a></div>'
				 }
				   
				],
			data : data,
			rowTemplate: '<div ng-mouseover="grid.appScope.onRowHover(row);" ng-mouseleave="grid.appScope.onRowLeave(row);"><div ng-repeat="col in colContainer.renderedColumns track by col.colDef.name"  class="ui-grid-cell" ui-grid-cell></div></div>'	
        	};
	};

	/****************************************************************
	* onRowHover function handles the mouse over the row of grid
	****************************************************************/
	$scope.onRowHover = function(row) {
	    	if(row.entity.date_to===$scope.currentdate)//handle only the active station
		{
			$scope.mymap.eachLayer(function(layer) {
				if(layer.hasOwnProperty("_latlng")){
					if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
					{
					   if (!(layer._leaflet_id in $scope.markerselecteddict)){ //avoid to change markers selected
						var highlight = {
						    		radius: 12,
								fillColor: "#FCFC33",
								color: "#000",
								weight: 1,
								opacity: 1,
								fillOpacity: 0.8
						};
						if((layer._leaflet_id!=='')&&($scope.onlyonelayerid==false)){
		                                        $scope.onlyonelayerid = true;
							$scope.layerid = layer._leaflet_id;//keep in $scope the layer id
							$scope.networkentity = row.entity.network.name;//keep in $scope the network to allow to back to the default color
							$scope.mymap._layers[layer._leaflet_id].setStyle(highlight);//change the color of the marker
							$scope.mymap._layers[layer._leaflet_id].bringToFront();
						}
		                            }
					}
				}	
			});
		}
	};
	/****************************************************************
	* onRowLeave function handles the mouse leave the row of grid
	****************************************************************/
	$scope.onRowLeave = function(row) {
		if ((!($scope.layerid in $scope.markerselecteddict))&&($scope.layerid!='')){ //avoid to change markers selected
			var defaultoption = {
				    		radius: 7,
                                                fillColor: $scope.colorNetworks[$scope.networkentity].color,
						color: "#000",
						weight: 1,
						opacity: 1,
						fillOpacity: 0.8
			};
			if($scope.layerid!=''){
				$scope.mymap._layers[$scope.layerid].setStyle(defaultoption);//back to default color of marker
			}
			$scope.onlyonelayerid = false;
			$scope.layerid = '';
		}
	};

	/*******************************************************************
	* zoomInitial function allows to back to the initial view of the map
	*******************************************************************/
	$scope.zoomInitial = function () {
    		$scope.mymap.setView([51.505, -0.09], 3);
	};
	
	/******************************************
	* clickClear function handles clear button
	******************************************/
	$scope.clickClear = function () {
		$scope.drawcircle = '';
		$scope.drawrectangle = '';
		$scope.latitude = '';
		$scope.longitude = '';
		$scope.Radius = '';
		$scope.latLong = '';
                $scope.North = '';
		$scope.South = '';
                $scope.East = '';
		$scope.West = '';
		$scope.mymap.removeLayer($scope.layer); 
		$scope.layer = '';
		$scope.gridApi.core.clearAllFilters();
		$scope.receivertypeselected = '';
                $scope.antennatypeselected = '';
		$scope.radometypeselected = '';
                $scope.satellitesystemselected = '';
		$scope.filefromdateselected = '';
		$scope.filetodateselected = '';
		$scope.publishfromdateselected = '';
		$scope.publishtodateselected = '';
                $scope.dataavailabilityselected = '';
		$scope.stationlifetimeselected = '';
                $scope.datasamplingselected = '';
                $scope.filetypeselected = '';
		$scope.statusfileselected = '';
		$scope.dataqualityselected = '';
                $scope.satellitesystemt3selected = '';
                $scope.filelatencyselected = '';
		$scope.typeofsignalselected = '';
		$scope.frequencyselected = '';
		$scope.channelselected = '';
		$scope.epochselected = '';
		$scope.elevangleselected = '';
		$scope.multipathselected = '';
		$scope.cycleslipsselected = '';
		$scope.ssprmsselected = '';
		$scope.clockjumpselected = '';
		$scope.gridApi.selection.clearSelectedRows();
		$scope.markerselecteddict = {};
                $scope.polygonpoints = [];
		$scope.drawpolygon = '';
                $scope.gridOptions.data = $scope.ajaxresults;
		$scope.gridApi.core.refresh();
                $scope.zoomInitial();
	};
	
	
	
	/********************************************************************
	* markersHandler function handles the display of markers in the map
	********************************************************************/
	$scope.markersHandler = function(data) {
		var marker = '';
		var networkStr = '';
		var markerarray = [];
		var date_end = '';
		angular.forEach(data, function(result) {
			networkStr = result.network.name;
                        //active station 
                        if (result.date_to === $scope.currentdate){
				var optionMarker = {
							radius: 7,
							fillColor: $scope.colorNetworks[networkStr].color,
							color: "#000",
							weight: 1,
							opacity: 1,
							fillOpacity: 0.8							
						};
				
                                marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon],optionMarker);
				date_end = "";
                            
			}else{//desactive station
                                var LeafIcon = L.Icon.extend({
					options: {
						 iconSize:     [15, 15],
						iconAnchor:   [15, 15]
					}
				});
                                var mycolor = $scope.colorNetworks[networkStr].color;
				var mypathicon  = "images/black.png";
                               	marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon],{icon: new LeafIcon({iconUrl: mypathicon})});
				date_end = result.date_to;
			}
			
			var container = $('<div />');//create a container for popup
                        marker.result = result;//allow to store in the marker all the informations
			
			var markerdouble = '';
			var datefromdouble = '';
			var datetodouble = '';
			var altitudedouble = '';
			
			for (var i = 0; i < data.length; i++) {
			  if ((data[i].location.coordinates.lat === result.location.coordinates.lat)&&(data[i].location.coordinates.lon === result.location.coordinates.lon))
				{
					if (data[i].marker!=result.marker)
					{
						markerdouble = data[i].marker;
						datefromdouble = data[i].date_from;
						if (data[i].date_to === $scope.currentdate){
							datetodouble = "";
						}else{
							datetodouble = data[i].date_to;
						}
						altitudedouble = data[i].location.coordinates.altitude;
					}
				}
			}
			if (markerdouble!='') {//case two stations at a same coordinates
				
				container.on('click', '.popupLink', function() {
		                        $scope.routeMetadatalink(result.marker);
				});
				container.on('click', '.popupLink2', function() {
		                        $scope.routeMetadatalink(markerdouble);
				});

				container.html('<div id="popupcontainer"><div><span><b>Marker:</b> ' + result.marker +  
		                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) +
		                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from + 
		                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end + 
		                                 '</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.country.name + 	                         
                                                 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + result.network.name +  
		                                 '</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a></div>' + 
						 '<div><span><b>Marker:</b> ' + markerdouble + 
		                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3)	+  
		                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
		                                 '</span><br style="line-height:0px;"><span><b>Alt:</b> ' + altitudedouble.toFixed(3) +
		                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + datefromdouble + 
		                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + datetodouble +
		                                 '</span><br style="line-height:0px;"><span><b>Country:</b> '  + result.location.country.name +		                                 
						 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + result.network.name +
		                                 '</span><br style="line-height:0px;"><a class="popupLink2">Metadata details</a></div></div>'); 

			   	
			}else{
				
				container.on('click', '.popupLink', function() {
		                        $scope.routeMetadatalink(result.marker);
				});

				container.html('<span><b>Marker:</b> ' + result.marker + 
		                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from + 
		                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end + 
		                                 '</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.country.name + 		                                 
						 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + result.network.name +  
		                                 '</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a>');
				
			}
			
			marker.bindPopup(container[0]);//bind the marker to the popup
		        marker.on('click', function(e) {
				var markerselectedflag = false;
				for (var key in $scope.markerselecteddict) {
					if($scope.markerselecteddict[key]===result.marker){
						markerselectedflag = true;	
					}
				}
				if(markerselectedflag === true){//if we click on marker always selected, we unselected this marker
					$scope.latitude = '';
					$scope.longitude = '';
					angular.forEach($scope.gridOptions.data, function (data, index) {
						if(data.marker===result.marker){
							$scope.gridApi.selection.unSelectRow($scope.gridOptions.data[index]);
						}
					});
					$scope.mymap.closePopup();
				}else{//if we click on marker no selected, we selected also in the grid
			                $scope.latitude = result.location.coordinates.lat;
					$scope.longitude = result.location.coordinates.lon;
					angular.forEach($scope.gridOptions.data, function (data, index) {
						if(data.marker===result.marker){
							$scope.gridApi.selection.selectRow($scope.gridOptions.data[index]);//select the row in the grid
                					$scope.gridApi.grid.element[0].getElementsByClassName("ui-grid-viewport")[0].scrollTop = index * $scope.gridApi.grid.options.rowHeight;//the row selected in the grid is showed on the top of the grid
						}
					});
				}
				$scope.$apply();//allow to make an update
			});
			
			if($scope.firstload === true)
			{
				if (networkStr in $scope.networksdict)
				{
					$scope.networksdict[networkStr].push(marker);
					$scope.resultnetworksdict[networkStr].push(result);
					
				}else{
				
					$scope.networksdict[networkStr] = [marker];
					$scope.resultnetworksdict[networkStr] = [result];
				}
			}else{
				markerarray.push(marker)
			}
			networkStr = '';
			
		});
                var refnetwork = ''
		if($scope.firstload === true)
		{
			for (var key in $scope.networksdict)
			{
				$scope.layernetworksdict[key] = new L.FeatureGroup($scope.networksdict[key]).addTo($scope.mymap);
				
			}
			$scope.firstload = false;
		}else{
			refnetwork = new L.FeatureGroup(markerarray).addTo($scope.mymap);
			$scope.mymap.fitBounds(refnetwork.getBounds(),{maxZoom:7});
		}
		
		
	};

	/*************************************************
	* addEraseDraw function allow to remove the draw
	*************************************************/
	$scope.addEraseDraw= function () {
		var MyControl = L.Control.extend({
		    options: {
			position: 'topleft'
		    },
		    onAdd: function (map) {
			var container = L.DomUtil.create('div', 'erase-control');
			container.innerHTML += '<button style="background-image:url(images/erase.png);width:28px;height:28px;border-radius:5px;" title="Erase"></button>';       	
			$compile( container )($scope);
			return container;
		    }
		});
		$scope.mymap.addControl(new MyControl());
		$('.erase-control').click(function(event) {
			$scope.gridOptions.data = $scope.ajaxresults;
			$scope.gridApi.core.refresh();
			$scope.drawcircle = '';
			$scope.drawrectangle = '';
			$scope.latitude = '';
			$scope.longitude = '';
			$scope.Radius = '';
			$scope.latLong = '';
		        $scope.North = '';
			$scope.South = '';
		        $scope.East = '';
			$scope.West = '';
			$scope.mymap.removeLayer($scope.layer); 
			$scope.layer = '';
                        $scope.polygonpoints = [];
			$scope.drawpolygon = '';
		});
	};

	/*******************************************************************
	* switchmap function allow to switch between satellite or simple map
	*******************************************************************/
	$scope.switchmap = function () {
		var MyControl = L.Control.extend({
		    options: {
			position: 'bottomleft'
		    },
		    onAdd: function (map) {
			var container = L.DomUtil.create('div', 'switchmap-control');
			container.innerHTML += '<button id="mapbuttonid" style="background-image:url(images/satellitemap.png);width:70px;height:70px;"></button>';
			$scope.imagemap = "satellitemap";       	
			$compile( container )($scope);
			return container;
		    }
		});
		$scope.mymap.addControl(new MyControl());
		$('.switchmap-control').click(function(event) {
			if($scope.imagemap == "satellitemap")
			{
				L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
				document.getElementById("mapbuttonid").style.backgroundImage= "url(images/simplemap.png)";
				$scope.imagemap = "simplemap";
			}else{
				L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
				document.getElementById("mapbuttonid").style.backgroundImage= "url(images/satellitemap.png)";
				$scope.imagemap = "satellitemap";
			}
		});
	};

	
	/*********************************************************************************
	* drawHandler function handles the figure polygon, rectangle and circle in the map
	*********************************************************************************/
	$scope.drawHandler = function () {
		var MyControl = L.Control.extend({
		    options: {
			position: 'topleft'
		    },
		    onAdd: function (map) {
			var container = L.DomUtil.create('div', 'figure-control');
			container.innerHTML += '<div><button id="polygon" style="background-image:url(images/polygon.png);width:28px;height:28px;border-radius:5px;" title="draw polygon"></button></div><div><button id="rectangle" style="background-image:url(images/rectangle.png);width:28px;height:28px;border-radius:5px;" title="draw rectangle"></button></div><div></button><button id="circle" style="background-image:url(images/circle.png);width:28px;height:28px;border-radius:5px;" title="draw circle"></button></div>';       	
			$compile( container )($scope);
			return container;
		    }
		});
		$scope.mymap.addControl(new MyControl());
		$('.figure-control #polygon').click(function(event) {
			var polygon = new L.Draw.Polygon($scope.mymap, {
			    allowIntersection: false,
			    shapeOptions: {
			      color: '#007FFF'
			    }
			  });
			polygon.enable();
		});
		$('.figure-control #rectangle').click(function(event) {
			var rectangle = new L.Draw.Rectangle($scope.mymap, {
			    shapeOptions: {
			      color: '#007FFF'
			    }
			  });
			rectangle.enable();
		});
		$('.figure-control #circle').click(function(event) {
			var circle = new L.Draw.Circle($scope.mymap, {
			    shapeOptions: {
			      color: '#007FFF'
			    }
			  });
			circle.enable();
		});
                
		$scope.mymap.on('draw:created', function (e) {//all events for each figure
                        var newDataList=[];
			var type = e.layerType;
			$scope.layer = e.layer;
			if (type === 'circle') {
                                $scope.drawcircle = 'draw';
				$scope.latLong=[$scope.layer._latlng.lat,$scope.layer._latlng.lng];
				var theCenterPt = $scope.latLong;
				var theRadius = $scope.layer._mRadius;
				$scope.mymap.eachLayer(function (icon) {//loop throw the markers to check all markers inside the radius
					if(icon.hasOwnProperty("_latlng")){
						var layerlatlong = icon.getLatLng();
						var distancefromcenterPoint = layerlatlong.distanceTo(theCenterPt);
						if (distancefromcenterPoint <= theRadius)
						{
							newDataList.push(icon.result);
						}
					}	
				});
				$scope.Radius = (theRadius/1000).toFixed(1);
                                $scope.latitude = $scope.layer._latlng.lat.toFixed(6);
                                $scope.longitude = $scope.layer._latlng.lng.toFixed(6); 
                                $scope.gridOptions.data = newDataList;//new data for the grid
                                $scope.filterrowflag = false;
                                $scope.gridApi.core.refresh();//allow to refresh the grid  
			}
                        if (type === 'rectangle') {
                                $scope.drawrectangle = 'draw';
				var bounds = L.latLngBounds($scope.layer._latlngs[0], $scope.layer._latlngs[2]);
				$scope.mymap.eachLayer(function (icon) {//loop throw the markers to check all markers inside the rectangle
					if(icon.hasOwnProperty("_latlng")){
						var layerlatlong = icon.getLatLng();
						if (bounds.contains(layerlatlong))
						{
							newDataList.push(icon.result);
						}
					}	
				});
				$scope.South = $scope.layer._latlngs[0].lat.toFixed(6);
				$scope.North = $scope.layer._latlngs[2].lat.toFixed(6);
                                $scope.West = $scope.layer._latlngs[0].lng.toFixed(6);
				$scope.East = $scope.layer._latlngs[2].lng.toFixed(6);
				var middlelat = (parseFloat($scope.South) + parseFloat($scope.North))/2;
				var middlelon = (parseFloat($scope.East) + parseFloat($scope.West))/2;
				$scope.latLong=[middlelat,middlelon];
                                $scope.gridOptions.data = newDataList;//new data for the grid
				$scope.filterrowflag = false;
                                $scope.gridApi.core.refresh();//allow to refresh the grid  
			}
     		      if (type === 'polygon') 
                      {
				$scope.drawpolygon = 'draw';
				for (var key in $scope.networksdict)
				{
					for(var i=0;i<$scope.networksdict[key].length;i++)
					{
						if ($scope.isMarkerInsidePolygon($scope.networksdict[key][i],$scope.layer))//call the function to check it
						{
							newDataList.push($scope.networksdict[key][i].result);
						}
					}
				}
				for(var i=0;i<$scope.layer._latlngs.length;i++)
				{
					$scope.polygonpoints.push($scope.layer._latlngs[i].lat.toFixed(6));
					$scope.polygonpoints.push($scope.layer._latlngs[i].lng.toFixed(6));
				}	
        			$scope.gridOptions.data = newDataList;//new data for the grid
				$scope.filterrowflag = false;
        			$scope.gridApi.core.refresh();//allow to refresh the grid  
		      }               
			$scope.mymap.addLayer($scope.layer);
		});
                $scope.mymap.on('draw:drawstart', function (e) {//at the beginning of drawing
                        $scope.mymap.removeLayer($scope.layer);
                        $scope.layer = '';
                        $scope.drawcircle = '';
   			$scope.drawrectangle = '';
   			$scope.latitude = '';
   			$scope.longitude = '';
   			$scope.Radius = '';
   			$scope.latLong = '';
			$scope.North = '';
			$scope.South = '';
                        $scope.East = '';
			$scope.West = '';
			$scope.polygonpoints = [];
                        $scope.drawpolygon = '';
                        $scope.$apply();//apply an update
		});
	};

	/********************************************************************************
	* networkFilter function handles the selected network in "network" drop down list
	*********************************************************************************/
	$scope.networkFilter = function () {
		var networkselectresults = [];
                var selected = [];
                var networkStr = '';
		$('.my-custom-control .dropdown-menu li input:checked').each(function() {//take all checked networks in the dropdown list
		    selected.push($(this).attr('value'));
		});
		
                if (selected.length>=1)//case there's something selected
                {
			for (var key in $scope.layernetworksdict)
			{
				// put/remove makers belonging to the selected networks in the map dropdown	
				if (selected.indexOf(key) > -1)
				{	
					networkselectresults = networkselectresults.concat($scope.resultnetworksdict[key]);
					
				}
					
			}
			
                	$scope.results = networkselectresults;
	    
		}else{//case nothing is selected (deselectall)
		   
			$scope.results = [];
		}
                $scope.filterrowflag = true;
          
                $scope.gridOptions.data = $scope.results;//update the data in grid
                $scope.gridApi.core.refresh();//refresh the grid
                
	};
	
	/*******************************************************************************************
	* isMarkerInsidePolygon function allows to know if the marker is inside in the polygon draw 
	*******************************************************************************************/
	$scope.isMarkerInsidePolygon = function (marker, poly) {
    		var polyPoints = poly.getLatLngs();       
    		var x = marker.getLatLng().lat, y = marker.getLatLng().lng;

	    	var inside = false;
	    	for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) 
		{
			var xi = polyPoints[i].lat, yi = polyPoints[i].lng;
			var xj = polyPoints[j].lat, yj = polyPoints[j].lng;

			var intersect = ((yi > y) !== (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
			if (intersect) {inside = !inside;}
		}

	    return inside;
	};

	/******************************************************************
	* changeRadiusCircle function handles the changing radius of circle
	*******************************************************************/
	$scope.changeRadiusCircle = function(){
	    	if (($scope.latitude!=='') && ($scope.longitude!==''))
		{
		    	$scope.mymap.removeLayer($scope.layer);  
		  	$scope.layer = L.circle([parseFloat($scope.latitude),parseFloat($scope.longitude)], $scope.Radius * 1000);
		  	$scope.layer.addTo($scope.mymap);
		    	var theCenterPt = [parseFloat($scope.latitude),parseFloat($scope.longitude)];
		    	var theRadius = $scope.Radius * 1000;
		    	var newDataList=[];
	
		    	$scope.mymap.eachLayer(function (icon) {
				if(icon.hasOwnProperty("_latlng")){
					var layerlatlong = icon.getLatLng();
					var distancefromcenterPoint = layerlatlong.distanceTo(theCenterPt);
					if ((distancefromcenterPoint <= theRadius)&&(icon.result!==undefined))
					{
						newDataList.push(icon.result);		
					}
				}
			});
			$scope.filterrowflag = false;	
	    		$scope.gridOptions.data = newDataList;
	    		$scope.gridApi.core.refresh();
		}
	};

	/****************************************************************
	* changeRectangle function handles the changing size of rectangle
	****************************************************************/
	$scope.changeRectangle = function(){
		if(($scope.South!=='')&&($scope.North!=='')&&($scope.East!=='')&&($scope.West!==''))
		{
			$scope.mymap.removeLayer($scope.layer);
			var middlelat = (parseFloat($scope.South) + parseFloat($scope.North))/2;
			var middlelon = (parseFloat($scope.East) + parseFloat($scope.West))/2;
			$scope.latLong=[middlelat,middlelon];	
			var bounds = L.latLngBounds([$scope.North, $scope.West], [$scope.South, $scope.East]);  
		  	$scope.layer = L.rectangle(bounds);
		  	$scope.layer.addTo($scope.mymap);
      			$scope.mymap.fitBounds($scope.layer.getBounds());
		    	var newDataList=[];
		    	$scope.mymap.eachLayer(function (icon) {
				if(icon.hasOwnProperty("_latlng")){
					var layerlatlong = icon.getLatLng();
						
					if (bounds.contains(layerlatlong))
					{
						newDataList.push(icon.result);
					}
				}
			});
			$scope.filterrowflag = false;
			$scope.gridOptions.data = newDataList;
			$scope.gridApi.core.refresh();
		}			
	};

	/*****************************************************
	* simple function handles the on click "simple" button
	******************************************************/
	$scope.simple = function(){
		$scope.simpleoradvanced = 'simple';	
		$scope.selected = !$scope.selected;
                $scope.receivertypeselected = '';
                $scope.antennatypeselected = '';
		$scope.radometypeselected = '';
                $scope.satellitesystemselected = '';
		$scope.filefromdateselected = '';
		$scope.filetodateselected = '';
		$scope.publishfromdateselected = '';
		$scope.publishtodateselected = '';
                $scope.dataavailabilityselected = '';
		$scope.stationlifetimeselected = '';
                $scope.datasamplingselected = '';
                $scope.filetypeselected = '';
		$scope.statusfileselected = '';
		$scope.dataqualityselected = '';
                $scope.satellitesystemt3selected = '';
                $scope.filelatencyselected = '';
		$scope.typeofsignalselected = '';
		$scope.frequencyselected = '';
		$scope.channelselected = '';
		$scope.epochselected = '';
		$scope.elevangleselected = '';
		$scope.multipathselected = '';
		$scope.cycleslipsselected = '';
		$scope.ssprmsselected = '';
		$scope.clockjumpselected = '';
        };	
	/*********************************************************
	* advanced function handles the on click "advanced" button
	**********************************************************/
	$scope.advanced = function(){
		$scope.simpleoradvanced = 'advanced';
		$scope.selected = !$scope.selected;
                $scope.loadAdvanced();
        };	
	

        /**************************************************************************
	* routeNetworklink function handles on click links to access network detail
	**************************************************************************/
	$scope.routeNetworklink = function (network_name) {
                var network_name_repl = network_name.replace(" ", ",");
		//open the view network with the parameter network
                var mypass = $window.open('#/network/'+ network_name_repl,'_blank');		
	};
 
        /*****************************************************************************
	* splitNetworklink function allows to split network name when there're several
	*****************************************************************************/
        $scope.splitNetworklink = function (network_name) {
                var res = network_name.split(' ');
		return res;		
	};
		
        $scope.loadResults();
	
  }]);
 
 



'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc service
 * @name glasswebuiApp.serviceResource
 * @description
 * # serviceResource
 * Factory in the glasswebuiApp.
 */

// service for "static" endpoints

angular.module('glasswebuiApp')
  .factory('serviceResource', ["$http", "$resource", "CONFIG", function serviceResource($http,$resource,CONFIG) {
    var server = CONFIG.server;
    var clientname = CONFIG.clientname;
    var glassapi = CONFIG.glassapi;
    
    return{
        site: function(){
	    //return $resource(server + glassapi + "/webresources/stations/stations-short");
	    return $resource(server + glassapi + "/webresources/stations/v2/station/short/json");
        },
        receiver: function(){
            //return $resource(server + glassapi + "/webresources/stations/receiver-type-list");
            return $resource(server + glassapi + "/webresources/stations/v2/list/receiver");      
        },
        antenna: function(){
            //return $resource(server + glassapi + "/webresources/stations/antenna-type-list");
	    return $resource(server + glassapi + "/webresources/stations/v2/list/antenna");      
        },
        radome: function(){
	    //return $resource(server + glassapi + "/webresources/stations/radome-type-list");
	    return $resource(server + glassapi + "/webresources/stations/v2/list/radome");      
        },
	filetype: function(){
	    //return $resource(server + glassapi + "/webresources/stations/files/file-type-list");
	    return $resource(server + glassapi + "/webresources/stations/v2/list/files_type");      
        }	
    };
  }]);

'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:MetadataCtrl
 * @description
 * # MetadataCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
  .controller('MetadataCtrl', ["$scope", "serviceHistory", "serviceResource", "$uibModal", "$window", "$timeout", "$resource", "$routeParams", "$compile", "$templateCache", "CONFIG", "$http", function ($scope, serviceHistory, serviceResource, $uibModal,$window,$timeout,$resource,$routeParams,$compile,$templateCache,CONFIG,$http) {
	$scope.results = '';
	$scope.navitem ='infoitem';
	$scope.metadataviewchoice = 'blank';
	$scope.myInterval = 5000;
	$scope.noWrapSlides = false;
	$scope.active = 0;
	var slides = $scope.slides = [];
	var currIndex = 0;
        $scope.markerselecteddict = {};
        $scope.colorNetworks = {};
        $scope.networkentity = '';
        $scope.onlyonelayerid = false;
        var mainchart;
        var markerchartdict = {};
        var markerchartarray = [0];
        $scope.detailflag = false;
        $scope.timeseries_dict_ingv = {};
        $scope.timeseries_dict_uga = {};
	$scope.pictures_dict = {};
        
	$scope.filefromdateselected = '';
	$scope.filetodateselected = '';
	$scope.fileT3fromdateselected = '';
	$scope.fileT3todateselected = '';
	$scope.shiftkeypressed = false;
        /*********************************************
	* addhistory function allows to add in history
	*********************************************/
	$scope.addhistory = function() {
	    serviceHistory.addhistory(CONFIG.server + CONFIG.clientname + "/#/metadata/" + $routeParams.querystring);
	};


        /*******************************************************
	* getPictures function allows to get pictures of station
	********************************************************/
	/*$scope.getPictures = function() {
		for(var i=0;i<$scope.results.length;i++)
		{
			for(var j=0;j<$scope.results[i].documents.length;j++)
			{
				if ($scope.results[i].documents[j].document_type.name=="timeseries")
				{
					$scope.timeseries_dict[$scope.results[i].marker.toUpperCase()] = CONFIG.server + CONFIG.glassapi + $scope.results[i].documents[j].link;
				}else{//take the first image on the list
					if( $scope.results[i].documents.length>0)
					{
						$scope.pictures_dict[$scope.results[i].marker.toUpperCase()] = CONFIG.server + CONFIG.glassapi + $scope.results[i].documents[0].link;
					}
				}
				slides.push({
						image: CONFIG.server + CONFIG.glassapi + $scope.results[i].documents[j].link,
						text: $scope.results[i].documents[j].title,
						id: currIndex++
					    });
			}	
		}
	};*/

        $scope.getPictures = function() {
		$http.get(CONFIG.server + CONFIG.glassapi + '/images/timeseries/UGA/timeseries_' + $scope.results[0].marker.toUpperCase() + '.png').then(function (response){
			if (response.data.length>0){
				$scope.slides.push({
					image: CONFIG.server + CONFIG.glassapi + '/images/timeseries/UGA/timeseries_' + $scope.results[0].marker.toUpperCase() + '.png',
					text: 'UGA',
					id: currIndex++
				    });
			}	
		});
		$http.get(CONFIG.server + CONFIG.glassapi + '/images/timeseries/INGV/timeseries_' + $scope.results[0].marker.toUpperCase() + '.png').then(function (response){
			if (response.data.length>0){
				$scope.slides.push({
					image: CONFIG.server + CONFIG.glassapi + '/images/timeseries/INGV/timeseries_' + $scope.results[0].marker.toUpperCase() + '.png',
					text: 'INGV',
					id: currIndex++
				    });
			}	
		});
	};
	
	/**************************************************************************************
	* currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
	**************************************************************************************/
	$scope.currentDateFormat = function() {
	    var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1;
	    var HH = today.getHours();
	    var MM = today.getMinutes();
	    var SS = today.getSeconds();
	    var yyyy = today.getFullYear();
	    if(dd<10){
		dd='0'+dd
	    } 
	    if(mm<10){
		mm='0'+mm
	    }
	    if (HH < 10) {
		HH = "0" + HH;
	    }
	    if (MM < 10) {
		MM = "0" + MM;
	    }
	    if (SS < 10) {
		SS = "0" + SS;
	    }   
	    return today = yyyy+'-'+mm+'-'+dd+' '+HH+':'+MM+':'+SS;
	};
       
	/*************************************************************
	* loadingdialog function allows to open a modal with a spinner 
	**************************************************************/
	$scope.loadingdialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalLoading.html',
		size: 'm',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/*****************************************************
	* requestMetadata function allows to request metadata
	******************************************************/
	$scope.requestMetadata = function () {
		var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/json?';
		var req = $resource(adress +':myquery' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: true}});
		$scope.request_json = adress + $routeParams.querystring;//$routeParams.querystring is the parameter defined in app.js for routing
		/*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/" + $routeParams.querystring;
		$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/" + $routeParams.querystring;
		$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/" + $routeParams.querystring;
		$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/" + $routeParams.querystring;
		$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/" + $routeParams.querystring;
		$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/" + $routeParams.querystring;
		$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/" + $routeParams.querystring;
		$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/" + $routeParams.querystring;*/
		$scope.loading = true;
		$scope.loadingdialog();
		req.query({myquery:$routeParams.querystring}).$promise.then(function(data) 
		{
			if (data.length>0)
			{	
				$scope.nodata = false;				
				if (data.length === 1)//case 1 station
				{
					$scope.metadataviewchoice = 'onestation';
					$scope.results = data;
					$scope.currentdate = $scope.currentDateFormat();
					for(var i=0;i<$scope.results.length;i++){
						if ($scope.results[i].date_to === null)
						{
							$scope.results[i]['date_to'] = $scope.currentdate;
							$scope.status = "Active";
						}else{
							$scope.status = "Deactivated";
						}
					}
					
                                        $timeout(function(){
						$scope.mymap = L.map('metamapid');
						L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
						$scope.switchmap();
						$scope.markersHandler($scope.results);
					},100);//set timeout 100ms to wait the DOM is loading
                                        $scope.getPictures();
			                $scope.station_equipments = $scope.equipment();
					$scope.modalInstance.opened.then(function () {
						$scope.loading = false;
		                        	$scope.modalInstance.close();//stop the spinner loading
					});
				}else if (data.length > 1){//case several stations
					$scope.metadataviewchoice = 'severalstation';
					$scope.results = data;
					$scope.currentdate = $scope.currentDateFormat();
					for(var i=0;i<$scope.results.length;i++){
						if ($scope.results[i].date_to === null)
						{
							$scope.results[i]['date_to'] = $scope.currentdate;
							$scope.status = "Active";
						}else{
							$scope.status = "Deactivated";
						}
					}
					$scope.results = $scope.getlastequipment();
					$scope.network = $scope.getNetwork($scope.results);

					$scope.createColorNetworksDict($scope.results);
                                        
                                        $timeout(function(){
						$scope.mymap = L.map('metamapid2');
						L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
						$scope.switchmap();
						$scope.markersHandler_multi($scope.results);
					},100);//set timeout 100ms to wait the DOM is loading
					
					//loop to display in the grid in the format string the networks like "RENAG RGP" 
					angular.forEach($scope.results,function(row){
					  row.getNetwork = function()
					  {
						var networkStr = ''; 
						for(var j=0;j<row.station_networks.length;j++)
						{
						  networkStr = networkStr + " " + row.station_networks[j].network.name;
						}
						if (networkStr.substr(0,1)===" ")
						{
							networkStr = networkStr.substr(1);
						}
						return networkStr;		
					  };
					});
                                        markerchartdict = $scope.fillmarkerdict($scope.results);
                                        $scope.gridHandler($scope.results);
                                        $timeout(function(){
                                        	$scope.temporalcharts();
                                        },100);//set timeout 100ms to wait the DOM is loading
					$scope.getPictures();
				}
			}else{
				$scope.modalInstance.close();//stop the spinner loading
				$scope.openmessage($scope.messageinfonodata);
			}
		});
		$(document).keydown(function (e) {
		    if (e.keyCode == 16) {
			$scope.shiftkeypressed = true;
		    }else{
			$scope.shiftkeypressed = false;
		    }
		});
	};
        $scope.requestMetadata();
	
	/*******************************************************************************
	* sortequipment function allows to sort receivers, antennas and radomes per date
	*******************************************************************************/
	$scope.sortequipment = function() {
                var precreceiverdate = '';
		var precantennadate = '';
		var precradomedate = '';
              
		for(var i=0;i<$scope.results.length;i++)
		{
			for(var j=0;j<$scope.results[i].station_items.length;j++)
			{
				if($scope.results[i].station_items[j].item.item_type.name==="antenna")
    				{
					if(precantennadate!='')
					{
						var d1 = new Date(precantennadate.replace(/-/g,'/'));
						var d2 = new Date($scope.results[i].station_items[j].item.item_attributes[0].date_from.replace(/-/g,'/'));
						if(d1.getTime()>d2.getTime())
						{
							var temp = $scope.results[i].station_items[j];
							$scope.results[i].station_items[j] = $scope.results[i].station_items[j-1];
							$scope.results[i].station_items[j-1] = temp;
						}
					}
					precantennadate = $scope.results[i].station_items[j].item.item_attributes[0].date_from;
				}
				if($scope.results[i].station_items[j].item.item_type.name==="receiver")
    				{
					if(precreceiverdate!='')
					{
						var d1 = new Date(precreceiverdate.replace(/-/g,'/'));
						var d2 = new Date($scope.results[i].station_items[j].item.item_attributes[0].date_from.replace(/-/g,'/'));
						if(d1.getTime()>d2.getTime())
						{
							var temp = $scope.results[i].station_items[j];
							$scope.results[i].station_items[j] = $scope.results[i].station_items[j-1];
							$scope.results[i].station_items[j-1] = temp;
						}
					}
					precreceiverdate = $scope.results[i].station_items[j].item.item_attributes[0].date_from;
				}
				if($scope.results[i].station_items[j].item.item_type.name==="radome")
    				{
					if(precradomedate!='')
					{
						var d1 = new Date(precradomedate.replace(/-/g,'/'));
						var d2 = new Date($scope.results[i].station_items[j].item.item_attributes[0].date_from.replace(/-/g,'/'));
						if(d1.getTime()>d2.getTime())
						{
							var temp = $scope.results[i].station_items[j];
							$scope.results[i].station_items[j] = $scope.results[i].station_items[j-1];
							$scope.results[i].station_items[j-1] = temp;
						}
					}
					precradomedate = $scope.results[i].station_items[j].item.item_attributes[0].date_from;
				}
			}
		}
		return $scope.results;
	};

	

	/*****************************************************************************
	* equipment function allows to get receivers, antenna and radome to display
	******************************************************************************/
	$scope.equipment = function() {
		var arrtemp = [];
		$scope.antennalist = [];
		$scope.receiverlist = [];
		$scope.radomelist = [];
		$scope.humiditysensorlist = [];
		$scope.pressuresensorlist = [];
		$scope.temperaturesensorlist = [];
		$scope.watervaporsensorlist = [];
                $scope.frequencystandardsensorlist = [];
		var precdate = '';
                var count = 0;
		$scope.results = $scope.sortequipment();
		for(var i=0;i<$scope.results.length;i++){
			for(var j=0;j<$scope.results[i].station_items.length;j++)
			{
				if($scope.results[i].station_items[j].item.item_type.name==="antenna")
    				{
					if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null)
					{
						$scope.antenna_dateto = $scope.currentdate;
					}else{
						$scope.antenna_dateto = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
					}
					$scope.antenna_comment = $scope.results[i].station_items[j].item.comment;
					for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
					{
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_type"){
							$scope.antenna_name = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							$scope.antenna_datefrom = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
							$scope.antenna_igs = $scope.results[i].station_items[j].item.item_attributes[k].igs_defined;
							$scope.antenna_model = $scope.results[i].station_items[j].item.item_attributes[k].model;
							if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null)
							{
								$scope.antenna_dateto = $scope.currentdate;
							}else{
								$scope.antenna_dateto = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number"){
							$scope.antenna_serial_number = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.antenna_height = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.antenna_height = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.antenna_height = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							} 
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_reference_point"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.antenna_reference_point = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.antenna_reference_point = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.antenna_reference_point = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "marker_arp_up_ecc"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.marker_arp_up_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.marker_arp_up_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.marker_arp_up_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "marker_arp_north_ecc"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.marker_arp_north_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.marker_arp_north_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.marker_arp_north_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "marker_arp_east_ecc"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.marker_arp_east_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.marker_arp_east_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.marker_arp_east_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "alignment_from_true_n"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.alignment_from_true_n = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.alignment_from_true_n = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.alignment_from_true_n = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_cable_type"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.antenna_cable_type = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.antenna_cable_type = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.antenna_cable_type = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
						if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_cable_length"){
							if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
								$scope.antenna_cable_length = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
								$scope.antenna_cable_length = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
								$scope.antenna_cable_length = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
						}
					}
					$scope.antennalist.push({datefrom:$scope.antenna_datefrom,dateto:$scope.antenna_dateto,antenna:$scope.antenna_name,antenna_serial_number:$scope.antenna_serial_number,height:$scope.antenna_height,igs:$scope.antenna_igs,model:$scope.antenna_model,antenna_reference_point:$scope.antenna_reference_point,marker_arp_up_ecc:$scope.marker_arp_up_ecc,marker_arp_north_ecc:$scope.marker_arp_north_ecc,marker_arp_east_ecc:$scope.marker_arp_east_ecc,alignment_from_true_n:$scope.alignment_from_true_n,antenna_cable_type:$scope.antenna_cable_type,antenna_cable_length:$scope.antenna_cable_length,comment:$scope.antenna_comment});
					for(var l=0;l<$scope.results[i].station_items.length;l++)
					{
						
						if($scope.results[i].station_items[l].item.item_type.name==="radome")
    						{
							$scope.radome_comment = $scope.results[i].station_items[l].item.comment;
							for(var k=0;k<$scope.results[i].station_items[l].item.item_attributes.length;k++)
							{
								if ($scope.results[i].station_items[l].item.item_attributes[k].attribute.name === "radome_type"){
									if ($scope.results[i].station_items[l].item.item_attributes[k].date_to === null)
									{
										$scope.radome_dateto = $scope.currentdate;
									}else{
										$scope.radome_dateto = $scope.results[i].station_items[l].item.item_attributes[k].date_to;
									}
									if(($scope.results[i].station_items[l].item.item_attributes[k].date_from===$scope.antenna_datefrom)&&($scope.radome_dateto===$scope.antenna_dateto))
									{
										$scope.radome_name = $scope.results[i].station_items[l].item.item_attributes[k].value_varchar;
										$scope.radomelist.push({datefrom:$scope.results[i].station_items[l].item.item_attributes[k].date_from,dateto:$scope.radome_dateto,radome:$scope.radome_name,igs:$scope.results[i].station_items[l].item.item_attributes[k].igs_defined,description:$scope.results[i].station_items[l].item.item_attributes[k].description,comment:$scope.radome_comment}); 
									}
								}
							}
							  	
						}
					}
					for(var l=0;l<$scope.results[i].station_items.length;l++)
					{	
						if($scope.results[i].station_items[l].item.item_type.name==="receiver")
    						{
							$scope.receiver_comment = $scope.results[i].station_items[l].item.comment;
							for(var m=0;m<$scope.results[i].station_items[l].item.item_attributes.length;m++)
							{
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "receiver_type")
								{
									$scope.receiver_name = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									$scope.receiver_datefrom = $scope.results[i].station_items[l].item.item_attributes[m].date_from;
									$scope.receiver_igs = $scope.results[i].station_items[l].item.item_attributes[m].igs_defined;
									$scope.receiver_model= $scope.results[i].station_items[l].item.item_attributes[m].model;
									if ($scope.results[i].station_items[l].item.item_attributes[m].date_to === null)
									{
										$scope.receiver_dateto = $scope.currentdate;
									}else{
										$scope.receiver_dateto = $scope.results[i].station_items[l].item.item_attributes[m].date_to;
									}
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "serial_number")
								{
									$scope.receiver_serial_number = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "firmware_version"){
									$scope.receiver_firmware_version = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "satellite_system"){
									$scope.receiver_satellites = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "elevation_cutoff_setting"){
									if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
										$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
										$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
										$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
									}
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "temperature_stabilization"){
									if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
										$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
										$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
										$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
									}
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "input_frequency"){
									if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
										$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
										$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
										$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
									}
								}
								if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "data_sampling_interval"){
									if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
										$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
										$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
									}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
										$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
									}
								}
							}
							if(count===0){//to avoid to display several time the receivers
									$scope.receiverlist.push({datefrom:$scope.receiver_datefrom,dateto:$scope.receiver_dateto,receiver:$scope.receiver_name,receiver_serial_number:$scope.receiver_serial_number,firmware:$scope.receiver_firmware_version,satellites_system:$scope.receiver_satellites,igs:$scope.receiver_igs,model:$scope.receiver_model,receiver_elevation_cutoff_setting:$scope.receiver_elevation_cutoff_setting,receiver_temperature_stabilization:$scope.receiver_temperature_stabilization,receiver_input_frequency:$scope.receiver_input_frequency,receiver_data_sampling_interval:$scope.receiver_data_sampling_interval,comment:$scope.receiver_comment});
							}
							// Part of session view
							var d1 = new Date($scope.results[i].station_items[l].item.item_attributes[0].date_from.replace(/-/g,'/'));
							var d2 = new Date($scope.antenna_dateto.replace(/-/g,'/'));
							var d3 = new Date($scope.receiver_dateto.replace(/-/g,'/'));
							var d4 = new Date($scope.antenna_datefrom.replace(/-/g,'/'));
							if((d1.getTime()<=d2.getTime())&&(d3.getTime()>=d4.getTime()))
							{
								for(var m=0;m<$scope.results[i].station_items[l].item.item_attributes.length;m++)
								{
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "receiver_type"){
										$scope.receiver_name = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
										$scope.receiver_datefrom = $scope.results[i].station_items[l].item.item_attributes[m].date_from;
										if ($scope.results[i].station_items[l].item.item_attributes[m].date_to === null)
										{
											$scope.receiver_dateto = $scope.currentdate;
										}else{
											$scope.receiver_dateto = $scope.results[i].station_items[l].item.item_attributes[m].date_to;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "serial_number"){
										$scope.receiver_serial_number = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "firmware_version"){
										$scope.receiver_firmware_version = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "satellite_system"){
										$scope.receiver_satellites = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
									}
                                                                        if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "elevation_cutoff_setting"){
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
											$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
											$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
											$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "temperature_stabilization"){
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
											$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
											$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
											$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "input_frequency"){
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
											$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
											$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
											$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "data_sampling_interval"){
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar!==null){
											$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar; 
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric!==null){
											$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										}else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date!==null){
											$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
								}
								if(precdate==='')
								{
									var d5 = new Date($scope.receiver_dateto.replace(/-/g,'/'));
									var d6 = new Date($scope.antenna_dateto.replace(/-/g,'/'));
									if(d5.getTime()<=d6.getTime())
									{
										var dateto = $scope.receiver_dateto;
									}else
									{
										var dateto = $scope.antenna_dateto;
									}
									arrtemp.push({datefrom:$scope.receiver_datefrom,dateto:dateto,antenna:$scope.antenna_name,antenna_serial_number:$scope.antenna_serial_number,antenna_height:$scope.antenna_height,antenna_reference_point:$scope.antenna_reference_point,marker_arp_up_ecc:$scope.marker_arp_up_ecc,marker_arp_north_ecc:$scope.marker_arp_north_ecc,marker_arp_east_ecc:$scope.marker_arp_east_ecc,alignment_from_true_n:$scope.alignment_from_true_n,antenna_cable_type:$scope.antenna_cable_type,antenna_cable_length:$scope.antenna_cable_length,radome:$scope.radome_name,receiver:$scope.receiver_name,receiver_serial_number:$scope.receiver_serial_number,firmware:$scope.receiver_firmware_version,satellites_system:$scope.receiver_satellites,receiver_elevation_cutoff_setting:$scope.receiver_elevation_cutoff_setting,receiver_temperature_stabilization:$scope.receiver_temperature_stabilization,receiver_input_frequency:$scope.receiver_input_frequency,receiver_data_sampling_interval:$scope.receiver_data_sampling_interval});
									precdate = dateto; 
								}else{
									var d7 = new Date($scope.receiver_datefrom.replace(/-/g,'/'));
									var d8 = new Date(precdate.replace(/-/g,'/'));
									if(d7.getTime()<d8.getTime())
									{
										var datefrom = $scope.antenna_datefrom;
									}else
									{
										var datefrom = $scope.receiver_datefrom;
									}
									var d9 = new Date($scope.receiver_dateto.replace(/-/g,'/'));
									var d10 = new Date($scope.antenna_dateto.replace(/-/g,'/'));
									if(d9.getTime()<=d10.getTime())
									{
										var dateto = $scope.receiver_dateto;
									}else
									{
										var dateto = $scope.antenna_dateto;
									}
									arrtemp.push({datefrom:datefrom,dateto:dateto,antenna:$scope.antenna_name,antenna_serial_number:$scope.antenna_serial_number,antenna_height:$scope.antenna_height,antenna_reference_point:$scope.antenna_reference_point,marker_arp_up_ecc:$scope.marker_arp_up_ecc,marker_arp_north_ecc:$scope.marker_arp_north_ecc,marker_arp_east_ecc:$scope.marker_arp_east_ecc,alignment_from_true_n:$scope.alignment_from_true_n,antenna_cable_type:$scope.antenna_cable_type,antenna_cable_length:$scope.antenna_cable_length,radome:$scope.radome_name,receiver:$scope.receiver_name,receiver_serial_number:$scope.receiver_serial_number,firmware:$scope.receiver_firmware_version,satellites_system:$scope.receiver_satellites,receiver_elevation_cutoff_setting:$scope.receiver_elevation_cutoff_setting,receiver_temperature_stabilization:$scope.receiver_temperature_stabilization,receiver_input_frequency:$scope.receiver_input_frequency,receiver_data_sampling_interval:$scope.receiver_data_sampling_interval});
									precdate = dateto;
								}
								
							}
							
						}
					}

				count+=1;					
				}
				// End of part of session view
				//other instrument
					if($scope.results[i].station_items[j].item.item_type.name==="humidity_sensor")
    					{
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null)
						{
							$scope.sensor_dateto_humidity = $scope.currentdate;
						}else{
							$scope.sensor_dateto_humidity = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.sensor_comment_humidity = $scope.results[i].station_items[j].item.comment;
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "sensor_model"){
								$scope.sensor_model_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								$scope.sensor_datefrom_humidity = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null)
								{
									$scope.sensor_dateto_humidity = $scope.currentdate;
								}else{
									$scope.sensor_dateto_humidity = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer"){
								$scope.sensor_manufacturer_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number"){
								$scope.sensor_serial_number_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "data_sampling_interval"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_data_sampling_interval_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_data_sampling_interval_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_data_sampling_interval_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "accuracy"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_accuracy_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_accuracy_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_accuracy_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_height_diff_to_ant_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_height_diff_to_ant_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_height_diff_to_ant_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date"){
								$scope.sensor_calibration_date_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "aspiration"){
								$scope.sensor_aspiration_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}               	
						}
						$scope.humiditysensorlist.push({datefrom:$scope.sensor_datefrom_humidity,dateto:$scope.sensor_dateto_humidity,sensor_model:$scope.sensor_model_humidity,sensor_manufacturer:$scope.sensor_manufacturer_humidity,sensor_serial_number:$scope.sensor_serial_number_humidity,sensor_data_sampling_interval:$scope.sensor_data_sampling_interval_humidity,sensor_accuracy:$scope.sensor_accuracy_humidity,sensor_height_diff_to_ant:$scope.sensor_height_diff_to_ant_humidity,sensor_calibration_date:$scope.sensor_calibration_date_humidity,sensor_aspiration:$scope.sensor_aspiration_humidity,comment:$scope.sensor_comment_humidity});	
					}
					if($scope.results[i].station_items[j].item.item_type.name==="pressure_sensor")
    					{
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null)
						{
							$scope.sensor_dateto_pressure = $scope.currentdate;
						}else{
							$scope.sensor_dateto_pressure = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
					        $scope.sensor_comment_pressure = $scope.results[i].station_items[j].item.comment;
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "sensor_model"){
								$scope.sensor_model_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								$scope.sensor_datefrom_pressure = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null)
								{
									$scope.sensor_dateto_pressure = $scope.currentdate;
								}else{
									$scope.sensor_dateto_pressure = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer"){
								$scope.sensor_manufacturer_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number"){
								$scope.sensor_serial_number_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "data_sampling_interval"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_data_sampling_interval_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_data_sampling_interval_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_data_sampling_interval_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								} 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "accuracy"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_accuracy_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_accuracy_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_accuracy_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_height_diff_to_ant_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_height_diff_to_ant_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_height_diff_to_ant_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								} 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date"){
								$scope.sensor_calibration_date_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "aspiration"){
								$scope.sensor_aspiration_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}	
						}
						$scope.pressuresensorlist.push({datefrom:$scope.sensor_datefrom_pressure,dateto:$scope.sensor_dateto_pressure,sensor_model:$scope.sensor_model_pressure,sensor_manufacturer:$scope.sensor_manufacturer_pressure,sensor_serial_number:$scope.sensor_serial_number_pressure,sensor_data_sampling_interval:$scope.sensor_data_sampling_interval_pressure,sensor_accuracy:$scope.sensor_accuracy_pressure,sensor_height_diff_to_ant:$scope.sensor_height_diff_to_ant_pressure,sensor_calibration_date:$scope.sensor_calibration_date_pressure,sensor_aspiration:$scope.sensor_aspiration_pressure,comment:$scope.sensor_comment_pressure});	
					}
					if($scope.results[i].station_items[j].item.item_type.name==="temperature_sensor")
    					{
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null)
						{
							$scope.sensor_dateto_temperature = $scope.currentdate;
						}else{
							$scope.sensor_dateto_temperature = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
					        $scope.sensor_comment_temperature = $scope.results[i].station_items[j].item.comment;
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "sensor_model"){
								$scope.sensor_model_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								$scope.sensor_datefrom_temperature = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null)
								{
									$scope.sensor_dateto_temperature = $scope.currentdate;
								}else{
									$scope.sensor_dateto_temperature = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer"){
								$scope.sensor_manufacturer_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number"){
								$scope.sensor_serial_number_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "data_sampling_interval"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_data_sampling_interval_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_data_sampling_interval_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_data_sampling_interval_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "accuracy"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_accuracy_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_accuracy_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_accuracy_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_height_diff_to_ant_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_height_diff_to_ant_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_height_diff_to_ant_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								} 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date"){
								$scope.sensor_calibration_date_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "aspiration"){
								$scope.sensor_aspiration_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "distance_to_antenna"){
								$scope.sensor_distance_to_antenna_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric; 
							}   
						}
						$scope.temperaturesensorlist.push({datefrom:$scope.sensor_datefrom_temperature,dateto:$scope.sensor_dateto_temperature,sensor_model:$scope.sensor_model_temperature,sensor_manufacturer:$scope.sensor_manufacturer_temperature,sensor_serial_number:$scope.sensor_serial_number_temperature,sensor_data_sampling_interval:$scope.sensor_data_sampling_interval_temperature,sensor_accuracy:$scope.sensor_accuracy_temperature,sensor_height_diff_to_ant:$scope.sensor_height_diff_to_ant_temperature,sensor_calibration_date:$scope.sensor_calibration_date_temperature,sensor_aspiration:$scope.sensor_aspiration_temperature,comment:$scope.sensor_comment_temperature});	
					}
					if($scope.results[i].station_items[j].item.item_type.name==="water_vapor_radiometer")
    					{
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null)
						{
							$scope.sensor_dateto_radiometer = $scope.currentdate;
						}else{
							$scope.sensor_dateto_radiometer = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
					        $scope.sensor_comment_radiometer = $scope.results[i].station_items[j].item.comment;
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "water_vapor_radiometer"){
								$scope.sensor_water_vapor_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								$scope.sensor_datefrom_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null)
								{
									$scope.sensor_dateto_radiometer = $scope.currentdate;
								}else{
									$scope.sensor_dateto_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer"){
								$scope.sensor_manufacturer_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number"){
								$scope.sensor_serial_number_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_height_diff_to_ant_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_height_diff_to_ant_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_height_diff_to_ant_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date"){
								$scope.sensor_calibration_date_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_date; 
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "distance_to_antenna"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_distance_to_antenna_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_distance_to_antenna_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_distance_to_antenna_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
						}
						$scope.watervaporsensorlist.push({datefrom:$scope.sensor_datefrom_radiometer,dateto:$scope.sensor_dateto_radiometer,sensor_manufacturer:$scope.sensor_manufacturer_radiometer,sensor_serial_number:$scope.sensor_serial_number_radiometer,sensor_height_diff_to_ant:$scope.sensor_height_diff_to_ant_radiometer,sensor_calibration_date:$scope.sensor_calibration_date_radiometer,sensor_distance_to_antenna:$scope.sensor_distance_to_antenna_radiometer,sensor_water_vapor_radiometer:$scope.sensor_water_vapor_radiometer,comment:$scope.sensor_comment_radiometer});	
					}
					if($scope.results[i].station_items[j].item.item_type.name==="frequency_standard")
    					{
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null)
						{
							$scope.sensor_dateto_frequency = $scope.currentdate;
						}else{
							$scope.sensor_dateto_frequency = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
					        $scope.sensor_comment_frequency = $scope.results[i].station_items[j].item.comment;
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "standard_type"){
								$scope.sensor_standard_type_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								$scope.sensor_datefrom_frequency = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null)
								{
									$scope.sensor_dateto_frequency = $scope.currentdate;
								}else{
									$scope.sensor_dateto_frequency = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "input_frequency"){
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar!==null){
									$scope.sensor_input_frequency_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar; 
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric!==null){
									$scope.sensor_input_frequency_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								}else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date!==null){
									$scope.sensor_input_frequency_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							
						}
						$scope.frequencystandardsensorlist.push({datefrom:$scope.sensor_datefrom_frequency,dateto:$scope.sensor_dateto_frequency,sensor_standard_type:$scope.sensor_standard_type_frequency,sensor_input_frequency:$scope.sensor_input_frequency_frequency,comment:$scope.sensor_comment_frequency});	
					}
			}
		}
		return arrtemp;
	};
	
	$scope.markers = new L.FeatureGroup();

        /**************************************************************
	* findUnique function allows to return array with unique values
	***************************************************************/
	$scope.findUnique = function (arr) {
	    var distinct=[];

	    for(var i=0;i<arr.length;i++)
		{
		var str=arr[i];
		if(distinct.indexOf(str)===-1)
		    {
		    distinct.push(str);
		    }
		}
             return distinct;
             
	};

	/********************************************************************************************************************
	* createColorNetworksDict function allows to have a dictionary of color for each network and get an array of networks
	********************************************************************************************************************/
        $scope.createColorNetworksDict = function (data){
		var dicttemp = {};
		var arrtemp = [];
		var networkStr = ''; 
            	var arrcolor = ["#E00DEF","#0000FF","#FF0000","#ED7F10","#FD6C9E","#FF00FF","#00FE7E","#79F8F8","#660099",
"#FCDC12","#EDFF0C","#AD4F09","#007FFF","#F4661B","#DE3163","#F9429E","#16B84E","#54F98D","#6600FF",
"#D2CAEC","#2E006C","#FEBFD2","#FF007F","#F88E55","#318CE7","#73C2FB","#FFFF6B","#F7230C","#D473D4",
"#E0115F","#01796F","#6C0277","#9E0E40","#9EFD38","#22780F","#FF4901","#B3B191","#83A697","#DF6D14",
"#00CCCB","#2C75FF","#FEFEE2","#E7F00D","#DC143C","#997A8D","#25FDE9","#0131B4","#F0C300","#00561B","#FFFF00"];
			
		var k = 0;
		angular.forEach(data, function(result) {
			for(var j=0;j<result.station_networks.length;j++)
			{
				networkStr = networkStr + " " + result.station_networks[j].network.name;
			}
                        if (networkStr.substr(0,1)===" ")
			{
				networkStr = networkStr.substr(1);
			}
			arrtemp.push(networkStr);
			if (!(networkStr in dicttemp)){
				dicttemp[networkStr] = {"color":arrcolor[k]}
				k = k +1;
			}
			networkStr = '';
                        
		});
                $scope.colorNetworks = dicttemp;
		$scope.networkStr = $scope.findUnique(arrtemp);		
	};

         
	/********************************************************************
	* markersHandler function handles the display of markers in the map
	********************************************************************/
        $scope.markersHandler = function(data) {
		var marker = '';
		var networkStr = '';
                var date_end = '';
		angular.forEach(data, function(result) {
			for(var j=0;j<result.station_networks.length;j++)
			{
			  networkStr = networkStr + " " + result.station_networks[j].network.name;  
			}
			if (networkStr.substr(0,1)===" ")
			{
				networkStr = networkStr.substr(1);
			}
			
                   	if (result.date_to === $scope.currentdate){
	   			var optionMarker = {
						radius: 7,
						fillColor: "#FF0000",
						color: "#000",
						weight: 1,
						opacity: 1,
						fillOpacity: 0.8
                                                
					   };
                       		marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon],optionMarker);
				date_end = "";
                   	}else{
				var LeafIcon = L.Icon.extend({
				    options: {
                                        iconSize:     [15, 15],
					iconAnchor:   [15, 15]
				    }
				});
				
				var mypathicon  = "images/black.png";
                                marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon],{icon: new LeafIcon({iconUrl: mypathicon})});
				date_end = result.date_to;
		   	}
		    	networkStr = '';
           	    	marker.result = result;
		    	marker.bindPopup('<span><b>Marker:</b> ' + result.marker + 
                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) + 
                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) + 
                                 '</span><br style="line-height:0px;"><span><b>Elev:</b> ' + result.location.coordinates.altitude.toFixed(3) + 
                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from + 
                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end + 
                                 '</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name +
                                 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result)
                                 );
			
		    	$scope.markers.addLayer(marker);
		});	
		$scope.mymap.addLayer($scope.markers);
		$scope.mymap.fitBounds($scope.markers.getBounds(),{maxZoom:3});		
	};

	/*****************************************************************************************************************
	* markersHandler_multi function handles the display of markers in the map when the result contains several markers
	******************************************************************************************************************/
        $scope.markersHandler_multi = function(data) {
		var marker = '';
		var networkStr = '';
		var markerarray = [];
		angular.forEach(data, function(result) {
			for(var j=0;j<result.station_networks.length;j++)
			{
			  networkStr = networkStr + " " + result.station_networks[j].network.name;  
			}
			if (networkStr.substr(0,1)===" ")
			{
				networkStr = networkStr.substr(1);
			}
                        //active station 
                        if (result.date_to === $scope.currentdate){
				var optionMarker = {
							radius: 7,
							fillColor: $scope.colorNetworks[networkStr].color,
							color: "#000",
							weight: 1,
							opacity: 1,
							fillOpacity: 0.8							
						};
				
                                marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon],optionMarker);
                            
			}else{//desactive station
                                var LeafIcon = L.Icon.extend({
					options: {
						 iconSize:     [15, 15],
						iconAnchor:   [15, 15]
					}
				});
                                var mycolor = $scope.colorNetworks[networkStr].color;
				var mypathicon  = "images/black.png";
                               	marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon],{icon: new LeafIcon({iconUrl: mypathicon})});
			}
			
			
		        marker.on('click', function(e) {
				var markerselectedflag = false;
				for (var key in $scope.markerselecteddict) {
					if($scope.markerselecteddict[key]===result.marker){
						markerselectedflag = true;	
					}
				}
				if(markerselectedflag === true){//if we click on marker always selected, we unselected this marker
					$scope.latitude = '';
					$scope.longitude = '';
					angular.forEach($scope.gridOptions.data, function (data, index) {
						if(data.marker===result.marker){
							$scope.gridApi.selection.unSelectRow($scope.gridOptions.data[index]);
						}
					});
					//$scope.mymap.closePopup();
				}else{//if we click on marker no selected, we selected also in the grid
			                $scope.latitude = result.location.coordinates.lat.toFixed(3);
					$scope.longitude = result.location.coordinates.lon.toFixed(3);
					angular.forEach($scope.gridOptions.data, function (data, index) {
						if(data.marker===result.marker){
							$scope.gridApi.selection.selectRow($scope.gridOptions.data[index]);//select the row in the grid
                					$scope.gridApi.grid.element[0].getElementsByClassName("ui-grid-viewport")[0].scrollTop = index * $scope.gridApi.grid.options.rowHeight;//the row selected in the grid is showed on the top of the grid
						}
					});
				}
				$scope.$apply();//allow to make an update
			});
			
			
			networkStr = '';
			$scope.markers.addLayer(marker);
		});
                
		$scope.mymap.addLayer($scope.markers);
		$scope.mymap.fitBounds($scope.markers.getBounds(),{maxZoom:3});	
		
	};

        

	/************************************************************
	* getNetworkForPopup function handles the display of networks
	* in tooltip when the users click on marker in the map
	************************************************************/
	$scope.getNetworkForPopup = function(result) {	
		var networkStr = ''; 
	    		
		for(var j=0;j<result.station_networks.length;j++)
		{
		  networkStr = networkStr + " " + result.station_networks[j].network.name;
		}
		if (networkStr.substr(0,1)===" ")
                {
			networkStr = networkStr.substr(1);
		}
		return networkStr;		
			  
	};

	
	
	/*******************************************************************
	* expandfunc function handles expand contact with the sign + and -
	*******************************************************************/
	$scope.expandfunc = function(elem){
	    if ($scope.isElemShown(elem)) 
             {	
			if (elem==='doc'){
				$scope.expanddoc = 'minus';
                                $scope.shownElemdoc = null;
			}
			if (elem==='log'){
				$scope.expandlog = 'minus';
                                $scope.shownElemlog = null;
			}
			if (elem==='net'){
				$scope.expandnet = 'minus';
                                $scope.shownElemnet = null;
			}
			if (elem==='receiver'){
				$scope.expandreceiver = 'minus';
                                $scope.shownElemreceiver = null;
			}
			if (elem==='antenna'){
				$scope.expandantenna = 'minus';
                                $scope.shownElemantenna = null;
			}
			if (elem==='radome'){
				$scope.expandradome = 'minus';
                                $scope.shownElemradome = null;
			}
			if (elem==='contact'){
				$scope.expandcontact = 'minus';
                                $scope.shownElemcontact = null;
			}
	    } else {  
			  if (elem==='doc'){
				$scope.expanddoc = 'plus';
                                $scope.shownElemdoc = elem;
			  }
			  if (elem==='log'){
				$scope.expandlog = 'plus';
                                $scope.shownElemlog = elem;
			  }
			  if (elem==='net'){
				$scope.expandnet = 'plus';
                                $scope.shownElemnet = elem;
			  }
			  if (elem==='receiver'){
				$scope.expandreceiver = 'plus';
                                $scope.shownElemreceiver = elem;
			  }
			  if (elem==='antenna'){
				$scope.expandantenna = 'plus';
                                $scope.shownElemantenna = elem;
			  }
			  if (elem==='radome'){
				$scope.expandradome = 'plus';
                                $scope.shownElemradome = elem;
			  }
			  if (elem==='contact'){
				$scope.expandcontact = 'plus';
                                $scope.shownElemcontact = elem;
			  }
		}
	};
	
	/*******************************************************************
	* isElemShown function handles expand contact with the sign + and -
	*******************************************************************/
	$scope.isElemShown = function(elem) {
                  if (elem==='doc'){
			return $scope.shownElemdoc === elem;
		  }
		  if (elem==='log'){
			return $scope.shownElemlog === elem;
		  }
		  if (elem==='net'){
			return $scope.shownElemnet === elem;
		  }
		  if (elem==='receiver'){
			return $scope.shownElemreceiver === elem;
		  }
		  if (elem==='antenna'){
			return $scope.shownElemantenna === elem;
		  }
		  if (elem==='radome'){
			return $scope.shownElemradome === elem;
		  }
		  if (elem==='contact'){
                        return $scope.shownElemcontact === elem;
		  }
	};

	/****************************************************************
	* download function handles downloaded files in different format  
	****************************************************************/
	$scope.download = function(filename,format) {
			if(format === 'Json')
			{
				var blob = new Blob([JSON.stringify($scope.results)], {type: 'text/json'});	
				if(window.navigator.msSaveOrOpenBlob) {//IE
					window.navigator.msSaveBlob(blob, filename);
				}else{
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;        
					document.body.appendChild(elem);
					elem.click();        
					document.body.removeChild(elem);
				}
			}else if((format==='CSV')||(format==='XML')||(format==='GeodesyML')||(format==='SINEX')||(format==='GAMIT')){
				if(format==='CSV'){
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/csv?';
				}else if(format==='XML'){
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/xml?';
				}
                                  
                                var req = $resource(adress +':myquery' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: false}});
				//execute asynchronous query
				req.query({myquery:$routeParams.querystring}).$promise.then(function(data) 
				{
					if (data!== '')
					{
						window.location = adress+$routeParams.querystring;
					}else{
						$scope.openmessage($scope.messageinfonodata);
					}
				});
			}
	};

	/****************************************************************
	* downloadgeodesymlfile function allow to download geodesyml file
	*****************************************************************/
	$scope.downloadgeodesymlfile = function (marker) {
                var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/geodesyml/' + marker.toUpperCase();
		window.open(adress);			
	};

	/*********************************************************************************************
	* downloadselectedfilegeodesyml function allow to download geodesyml file selected in the grid
	*********************************************************************************************/
	$scope.downloadselectedfilegeodesyml = function () {
		var selectedRows = $scope.gridApi.selection.getSelectedRows();
                var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/geodesyml/';
		if(selectedRows.length > 0){
			var i = 0;
			while(i<selectedRows.length){
				var elem = window.document.createElement('a');
				elem.href = adress+selectedRows[i]['marker'];
				document.body.appendChild(elem);
				elem.click();        
				document.body.removeChild(elem);
	
				i++;			
			}
		}else{
			$scope.openmessage($scope.messageinfogrid);
		}			
	};

	/*****************************************************************************************
	* downloadselectedfilesitelog function allow to download sitelog file selected in the grid
	******************************************************************************************/
	$scope.downloadselectedfilesitelog = function () {
		var selectedRows = $scope.gridApi.selection.getSelectedRows();
                var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/';
		if(selectedRows.length > 0){
                        var i = 0;
			while(i<selectedRows.length){
				var elem = window.document.createElement('a');
				elem.href = adress+selectedRows[i]['marker'];
				document.body.appendChild(elem);
				elem.click();        
				document.body.removeChild(elem);
				i++;			
			}
		}else{
			$scope.openmessage($scope.messageinfogrid);
		}			
	};

	/***************************************************************************
	* exportquery function allows to users to get the query for command request
	***************************************************************************/
	$scope.exportquery = function(format) {
                var pyglass_request = "pyglass stations -u " + CONFIG.server;
                if($routeParams.querystring.indexOf("site=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("site=");
			var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 5, endpos);
			pyglass_request = pyglass_request + " -sn " + param.replace(/,/g, " ");	
		}
                if($routeParams.querystring.indexOf("marker=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("marker=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -m " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("minLat=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("minLat=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsminlat " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("maxLat=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("maxLat=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsmaxlat " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("minLon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("minLon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsminlon " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("maxLon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("maxLon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsmaxlon " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("centerLat=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("centerLat=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -rcclat " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("centerLon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("centerLon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -rcclon " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("radius=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("radius=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rcr " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("polygon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("polygon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -ps " + param.replace(/[!,]/g, " ");
		}
                if($routeParams.querystring.indexOf("altitude=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("altitude=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			var posseparatedparam = param.indexOf(",");
                        if(posseparatedparam !== -1)
			{
				if(param.indexOf(",") !== 0)
				{
					pyglass_request = pyglass_request + " -htmin " + param.substring(0, posseparatedparam) + " -htmax " + param.substring(posseparatedparam + 1, param.length);
				}else{
					pyglass_request = pyglass_request + " -htmax " + param.substring(1, param.length);
				}
			}else{
				pyglass_request = pyglass_request + " -htmin " + param;
			}
		}
                if($routeParams.querystring.indexOf("longitude=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("longitude=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			var posseparatedparam = param.indexOf(",");
                        if(posseparatedparam !== -1)
			{
				if(param.indexOf(",") !== 0)
				{
					pyglass_request = pyglass_request + " -lgmin " + param.substring(0, posseparatedparam) + " -lgmax " + param.substring(posseparatedparam + 1, param.length);
				}else{
					pyglass_request = pyglass_request + " -lgmax " + param.substring(1, param.length);
				}
			}else{
				pyglass_request = pyglass_request + " -lgmin " + param;
			}
		}
                if($routeParams.querystring.indexOf("latitude=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("latitude=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			var posseparatedparam = param.indexOf(",");
                        if(posseparatedparam !== -1)
			{
				if(param.indexOf(",") !== 0)
				{
					pyglass_request = pyglass_request + " -ltmin " + param.substring(0, posseparatedparam) + " -ltmax " + param.substring(posseparatedparam + 1, param.length);
				}else{
					pyglass_request = pyglass_request + " -ltmax " + param.substring(1, param.length);
				}
			}else{
				pyglass_request = pyglass_request + " -ltmin " + param;
			}
		}
		if($routeParams.querystring.indexOf("installedDateMin=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("installedDateMin=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -idmin " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("installedDateMax=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("installedDateMax=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -idmax " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("removedDateMin=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("removedDateMin=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -rdmin " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("removedDateMax=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("removedDateMax=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -rdmax " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("network=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("network=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -nw " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("invertedNetworks=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("invertedNetworks=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -inw " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("agency=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("agency=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -ag " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("receiver=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("receiver=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			pyglass_request = pyglass_request + " -rt " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("antenna=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("antenna=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -at " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("radome=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("radome=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -radt " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("country=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("country=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -co " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("state=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("state=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 6, endpos);
			pyglass_request = pyglass_request + " -pv " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("city=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("city=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 5, endpos);
			pyglass_request = pyglass_request + " -ct " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("satelliteSystem=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("satelliteSystem=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 16, endpos);
			pyglass_request = pyglass_request + " -ss " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("dateRange=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("dateRange=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -dr " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("dataAvailability=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("dataAvailability=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -da " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("fileType=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("fileType=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			pyglass_request = pyglass_request + " -ft " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("samplingFrequency=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("samplingFrequency=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 18, endpos);
			pyglass_request = pyglass_request + " -sf " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("samplingWindow=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("samplingWindow=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -sw " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("minimumObservationYears=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("minimumObservationYears=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 24, endpos);
			pyglass_request = pyglass_request + " -mo " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("statusfile=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("statusfile=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 11, endpos);
			pyglass_request = pyglass_request + " -stf " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("observationtype=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("observationtype=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 16, endpos);
			pyglass_request = pyglass_request + " -obst " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("frequency=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("frequency=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -freq " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("channel=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("channel=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -chan " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("constellation=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("constellation=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 14, endpos);
			pyglass_request = pyglass_request + " -constel " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("ratioepoch=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("ratioepoch=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 11, endpos);
			pyglass_request = pyglass_request + " -epoch " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("elevangle=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("elevangle=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -elang " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("multipathvalue=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("multipathvalue=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -multpath " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("nbcycleslips=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("nbcycleslips=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 13, endpos);
			pyglass_request = pyglass_request + " -cycslps " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("spprms=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("spprms=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -ssprms " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("nbclockjumps=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("nbclockjumps=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 13, endpos);
			pyglass_request = pyglass_request + " -clkjmps " + param.replace(/,/g, " ");
		}         
		if(format==='Json'){
			$scope.currentrequest = $scope.request_json;
                        $scope.pyglassrequest = pyglass_request + " -o full-json";
		}else if(format==='Json + files'){
			$scope.currentrequest = $scope.request_json_files;
			$scope.pyglassrequest = pyglass_request + " -o full-json+files";
		}else if(format==='GeodesyML'){
			$scope.currentrequest = $scope.request_geodesyml;
			$scope.pyglassrequest = pyglass_request + " -o full-geodesyml";
		}else if(format==='GeodesyML + files'){
			$scope.currentrequest = $scope.request_geodesyml_files;
			$scope.pyglassrequest = pyglass_request + " -o full-geodesyml+files";
		}else if(format==='File list'){
			$scope.currentrequest = $scope.request_files_list;
			$scope.pyglassrequest = pyglass_request + " -o full-filelist";
		}else if(format==='SINEX'){
			$scope.currentrequest = $scope.request_sinex;
			$scope.pyglassrequest = pyglass_request + " -o full-sinex";
		}else if(format==='GAMIT'){
			$scope.currentrequest = $scope.request_gamit;
			$scope.pyglassrequest = pyglass_request + " -o full-gamit";
		}else if(format==='CSV'){
			$scope.currentrequest = $scope.request_csv;
			$scope.pyglassrequest = pyglass_request + " -o full-csv";
		}else if(format==='KML'){
			$scope.currentrequest = $scope.request_kml;
			$scope.pyglassrequest = pyglass_request + " -o full-kml";
		}
        	$scope.opendialog();
	};

	/******************************************************************************************
	* opendialog function allows to open a modal when the users click on "request query" button 
	*******************************************************************************************/
	$scope.opendialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalRequest.html',
		size: 'lg',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/***************************************
	* cancel function allows to cancel modal
	****************************************/
	$scope.cancel = function () {
    		$scope.modalInstance.dismiss("cancel");
  	};

	/*********************************************************************************
	* getlastequipment function allows to get the last receivers, antennas and radomes
	*********************************************************************************/
	$scope.getlastequipment = function() {
                var lastreceiver = '';
		var lastantenna = '';
		var lastradome = '';
		for(var i=0;i<$scope.results.length;i++)
		{
			for(var j=0;j<$scope.results[i].station_items.length;j++)
			{
				if($scope.results[i].station_items[j].item.item_type.name==="antenna")
    				{
					//if($scope.results[i].station_items[j].item.item_attributes[0].date_to==='9999-12-30 00:00:00')
					if($scope.results[i].station_items[j].item.item_attributes[0].date_to===null)
					{
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_type"){
								lastantenna = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.results[i]["lastantenna"]=lastantenna;
							}
						}
					}
				}
				if($scope.results[i].station_items[j].item.item_type.name==="receiver")
    				{
					//if($scope.results[i].station_items[j].item.item_attributes[0].date_to==='9999-12-30 00:00:00')
					if($scope.results[i].station_items[j].item.item_attributes[0].date_to===null)
					{
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "receiver_type"){
								lastreceiver = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.results[i]["lastreceiver"]=lastreceiver;
							}
						}
					}	
				}
				if($scope.results[i].station_items[j].item.item_type.name==="radome")
    				{
					//if($scope.results[i].station_items[j].item.item_attributes[0].date_to==='9999-12-30 00:00:00')
					if($scope.results[i].station_items[j].item.item_attributes[0].date_to===null)
					{
						for(var k=0;k<$scope.results[i].station_items[j].item.item_attributes.length;k++)
						{
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "radome_type"){
								lastradome = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.results[i]["lastradome"]=lastradome;
							}
						}
					}	
				}
			}
		}
		return $scope.results;
	};
		
	/***************************************************************
	* getNetwork function handles the display of networks
	* in rows of table summary station
	***************************************************************/
	$scope.getNetwork = function(data){
		var networkStr = '';
		var dicttemp = {}; 
		for(var i=0;i<data.length;i++)
		{
			for(var j=0;j<data[i].station_networks.length;j++)
			{
			  networkStr = networkStr + " " + data[i].station_networks[j].network.name; 
			}
			if (networkStr.substr(0,1)===" ")
			{
				networkStr = networkStr.substr(1);
			}
			dicttemp[data[i].marker] = networkStr;
			var networkStr = '';
		}
		return dicttemp;		
	};

	/*********************************************************************
	* metadataDetail function handles on click links to access metadata
	**********************************************************************/
	$scope.metadataDetail = function (code) {
		var adress = CONFIG.server + CONFIG.glassapi + "/webresources/stations/v2/combination/full/json?";
		var param = "marker=";
		var request_json = adress + param + code;
                /*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/" + $routeParams.querystring;
		$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/" + $routeParams.querystring;
		$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/" + $routeParams.querystring;
		$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/" + $routeParams.querystring;
		$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/" + $routeParams.querystring;
		$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/" + $routeParams.querystring;
		$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/" + $routeParams.querystring;
		$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/" + $routeParams.querystring;*/
		var mypass = $window.open('#/metadata/' + param + code,'_blank');	
	};

	/************************************************
	* messageinfogrid variable to grid error message
	************************************************/
	$scope.messageinfogrid = {	
    		textAlert : "You have to select at least one thing in grid or in lisboxes",
    		mode : 'info'
	  };
	/*******************************************************
	* messageinfonodata variable to advanced error message
	********************************************************/
	$scope.messageinfonodata = {
	    textAlert : "No data available",
	    mode : 'info'
	  };
  	/*******************************************************************
	* openmessage function allows to open a modal when there's an error
	*******************************************************************/
	$scope.openmessage = function(message) {
		return $scope.modalInstance = $uibModal.open({
	      	      templateUrl: 'views/messageBox.html',
                      scope: $scope,
		      size: 'lg',
		      resolve: {
			data: function () {
				$scope.messageinfo = message;
			  	return $scope.messageinfo;
			}
		      }
		 });
	};
	/**************************************
	* close function allows to close modal
	***************************************/
	$scope.close = function(){
	    $scope.modalInstance.close();
	};

	/***************************************************
	* selectText function allows to select text in modal
	***************************************************/
	$scope.selectText = function(containerid){
		if (document.selection) {
		    var range = document.body.createTextRange();
		    range.moveToElementText(document.getElementById(containerid));
		    range.select();
                    document.execCommand("Copy"); 
		} else if (window.getSelection) {
		    var range = document.createRange();
		    range.selectNode(document.getElementById(containerid));
                    window.getSelection().removeAllRanges();
		    window.getSelection().addRange(range);
                    document.execCommand("Copy"); 
		}
		
    	};

	/*****************************************
	* gridHandler function handles grid part
	******************************************/
	$scope.gridHandler = function(data) {
		var defaultcolor='';
		var screenwidth = screen.width;
		//custom selection on grid with checkbox
		$templateCache.put('ui-grid/selectionRowHeaderButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-row-selected': row.isSelected}\" ><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"row.isSelected\" ng-click=\"row.isSelected=!row.isSelected;selectButtonClick(row, $event)\">&nbsp;</div>"
  );

  		$templateCache.put('ui-grid/selectionSelectAllButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-all-selected': grid.selection.selectAll}\" ng-if=\"grid.options.enableSelectAll\"><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"grid.selection.selectAll\" ng-click=\"grid.selection.selectAll=!grid.selection.selectAll;headerButtonClick($event)\"></div>"
  ); 
		//define all grid options and their columns
		$scope.gridOptions = {
                        enableGridMenu: false,
		    	enableFiltering: false,
			enableSorting: true,
			enableRowSelection: true,
			enableSelectAll: true,
			multiSelect: true,
                        exporterMenuCsv: false,
			exporterMenuPdf: false,
                        enableColumnMoving: true,
                        enableColumnResizing: true,
                        showGridFooter: true,
			//all function regarding the ui-grid
                        onRegisterApi: function(gridApi){ 
      				$scope.gridApi = gridApi;
				
				//function allows to get the row selected in the grid and display the corresponding marker into the map with a circle bigger
				$scope.gridApi.selection.on.rowSelectionChanged($scope, function(row){ 
					if(row.entity.date_to===$scope.currentdate)
                                        {
						$scope.mymap.eachLayer(function(layer) {
							if(layer.hasOwnProperty("_latlng")){
								if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
								{
							
									var defaultoption = {
										    		radius: 7,
		           									fillColor: $scope.colorNetworks[row.entity.getNetwork()].color,
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
										};
									var mylayerid = layer._leaflet_id;
									if (row.isSelected){ 
										var highlight = {
										    		radius: 12,
												fillColor: "#2CDBF6",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
										};
										$scope.mymap._layers[mylayerid].setStyle(highlight);
										$scope.mymap._layers[mylayerid].bringToFront();
										$scope.markerselecteddict[mylayerid] = row.entity.marker;//store the marker in the dictionnary to use later
										
										var y = $scope.searchkey(row.entity.marker,markerchartdict);
											mainchart.yAxis[0].addPlotBand({                      
											    from: parseInt(y) - 0.25,
											    to: parseInt(y) + 0.25,
											    id: row.entity.marker
							                                    
											});
									}else{
										$scope.mymap._layers[mylayerid].setStyle(defaultoption);
										delete $scope.markerselecteddict[mylayerid];
                                                                                mainchart.yAxis[0].removePlotBand(row.entity.marker);
									}
							
								}
							}	
						});
                                                
                                               
                                               
					}
					
				});
				//function allows to get several row selected in the grid and display them into the map with a circle bigger
				 $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
					angular.forEach(rows, function(row, key) {
					 	if(row.entity.date_to===$scope.currentdate)
		                                {
							$scope.mymap.eachLayer(function(layer) {
								if(layer.hasOwnProperty("_latlng")){
									if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
									{
							
										var defaultoption = {
											    		radius: 7,
													fillColor: $scope.colorNetworks[row.entity.getNetwork()].color,
													color: "#000",
													weight: 1,
													opacity: 1,
													fillOpacity: 0.8
											};
										var mylayerid = layer._leaflet_id;
										if (row.isSelected){ 
											var highlight = {
											    		radius: 12,
													fillColor: "#2CDBF6",
													color: "#000",
													weight: 1,
													opacity: 1,
													fillOpacity: 0.8
											};
											$scope.mymap._layers[mylayerid].setStyle(highlight);
											$scope.mymap._layers[mylayerid].bringToFront();
											$scope.markerselecteddict[mylayerid] = row.entity.marker;
                                                                                        
											var y = $scope.searchkey(row.entity.marker,markerchartdict);
											mainchart.yAxis[0].addPlotBand({                      
											    from: parseInt(y) - 0.25,
											    to: parseInt(y) + 0.25,
											    id: row.entity.marker
							                                    
											});
										}else{
											$scope.mymap._layers[mylayerid].setStyle(defaultoption);
											delete $scope.markerselecteddict[mylayerid];
                                                                                        mainchart.yAxis[0].removePlotBand(row.entity.marker);
											
										}
							
									}
								}	
							});
							
						}
					});	
				});

    			},
			//definition of each column in the grid
			columnDefs: [
				  { name:'Marker', field: 'marker', enableColumnMenu: false,
				    cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.metadataDetail(row.entity.marker)">{{ COL_FIELD }}</a></div>'},
				  { name:'Site name', field: 'name',enableColumnMenu: false},
                                  { name:'Install Date', field: 'date_from',enableColumnMenu: false},
		                  { name:'End Date', field: 'date_to',enableColumnMenu: false,cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) 
										{
											if (grid.getCellValue(row ,col) == $scope.currentdate) 
											 {
												return 'white';
											 }
			        						}},
				  { name:'Lat', field: 'location.coordinates.lat', type: 'number', cellFilter: 'number: 3',enableColumnMenu: false},
		                  { name:'Lon', field: 'location.coordinates.lon', type: 'number', cellFilter: 'number: 3',enableColumnMenu: false},
		                  { name:'Elev', field: 'location.coordinates.altitude', type: 'number', cellFilter: 'number: 3',enableColumnMenu: false},
                                  { name:'Network', field: 'getNetwork()',enableColumnMenu: false},
				  { name:'Country', field: 'location.city.state.country.name',enableColumnMenu: false},
				  { name:'Last receiver', field: 'lastreceiver',enableColumnMenu: false},
                                  { name:'Last antenna', field: 'lastantenna',enableColumnMenu: false},
                                  { name:'Last radome', field: 'lastradome',enableColumnMenu: false}
				],
			data : data,
			rowTemplate: '<div ng-mouseover="grid.appScope.onRowHover(row);" ng-mouseleave="grid.appScope.onRowLeave(row);"><div ng-repeat="col in colContainer.renderedColumns track by col.colDef.name"  class="ui-grid-cell" ui-grid-cell></div></div>'
        	};
	};

        /****************************************************************
	* onRowHover function handles the mouse over the row of grid
	****************************************************************/
	/*$scope.onRowHover = function(row) {
		if (row.entity.marker.toUpperCase() in $scope.timeseries_dict)
                {
			$scope.station_timeseries = $scope.timeseries_dict[row.entity.marker.toUpperCase()];
		}else{
			$scope.station_timeseries = 'images/no_timeseries.png';
		}

		if (row.entity.marker.toUpperCase() in $scope.pictures_dict)
                {
			$scope.station_pictures = $scope.pictures_dict[row.entity.marker.toUpperCase()];
		}else{
			$scope.station_pictures = 'images/no_timeseries.png';
		}		
	};*/
	$scope.onRowHover = function(row) {
       
                $scope.station_timeseries_ingv = CONFIG.server + CONFIG.glassapi + '/images/timeseries/INGV/timeseries_' + row.entity.marker.toUpperCase() + '.png';
		$scope.station_timeseries_uga = CONFIG.server + CONFIG.glassapi + '/images/timeseries/UGA/timeseries_' + row.entity.marker.toUpperCase() + '.png';
		if (row.entity.marker.toUpperCase() in $scope.pictures_dict)
                {
			$scope.station_pictures = $scope.pictures_dict[row.entity.marker.toUpperCase()];
		}else{
			$scope.station_pictures = 'images/no_timeseries.png';
		}		
	};

		
	/***********************************************************************************
	* fillmarkerdict function allows to create a dictionary markers to use in highcharts
	************************************************************************************/
	$scope.fillmarkerdict = function(data){
		var dicttemp = {};
                var cpt = 1; 
		angular.forEach(data, function(result) {
			dicttemp[cpt] = result.marker;
                        markerchartarray.push(cpt);
			cpt=cpt+1;
		});
		
		return dicttemp;		
	};
        /**************************************************************
	* searchkey function allows to find the key of dictionary value
	**************************************************************/
	$scope.searchkey = function (val,array){
		    for (var key in array) {
			var this_val = array[key];
			if(this_val == val){
			    return key;
			    break;
			}
		    }
        };
	/*************************************************************************
	* convertDateFormat function allows to convert a date in format yyyy-mm-dd
	**************************************************************************/
	$scope.convertDateFormat = function(date) {
	    var mydate = new Date(date);
	    var dd = mydate.getDate();
	    var mm = mydate.getMonth()+1;
	    var yyyy = mydate.getFullYear();
	    if(dd<10){
		dd='0'+dd
	    } 
	    if(mm<10){
		mm='0'+mm
	    }
	       
	    return mydate = yyyy+'-'+mm+'-'+dd;
	};
	/**************************************************************************************
	* dateToNum function allows to convert a date in format yyyy-mm-dd to a number yyyymmdd
	**************************************************************************************/
	$scope.dateToNum = function (d) {
                d = d.substring(0, 10);
	  	d = d.split("-"); 
		return Number(d[0]+d[1]+d[2]);
	};
        /********************************************************************************
	* temporalcharts function allow to display the temporal chart for several station
	********************************************************************************/
	$scope.temporalcharts = function() {
                var defaultcolor='';
                var globalmarker='';
                var hasPlotBand = false;
                var series_stations = [];
                var data_availability_percent = {};
                var firstDateRange = '';
		var lastDateRange = '';
		var titleRange  = 'Data availability between station install date and current date';
		var fromdateofrange = "";
		var endateofrange = "";
                //prepare the data for the timeseries
                for(var i=0;i<$scope.results.length;i++)
                {
			var firstDate=$scope.results[i].date_from;
			firstDate = firstDate.substring(0,10);
			firstDateRange = firstDate;
			firstDate= Date.UTC(firstDate.substring(0, 4),firstDate.substring(5, 7)-1,firstDate.substring(8, 10));
		        var lastDate="";
			if ($scope.results[i].date_to === null)
			{
				lastDate = $scope.currentDateFormat();
				lastDate = lastDate.substring(0,10);
				lastDateRange = lastDate;
				lastDate= Date.UTC(lastDate.substring(0, 4),lastDate.substring(5, 7)-1,lastDate.substring(8, 10));
			}else{
				lastDate=$scope.results[i].date_to;
				lastDate = lastDate.substring(0,10);
				lastDateRange = lastDate;
				lastDate= Date.UTC(lastDate.substring(0, 4),lastDate.substring(5, 7)-1,lastDate.substring(8, 10));
			}
			if($scope.results[i].rinex_files_dates.length>0)
			{
				var arraydate = $scope.results[i].rinex_files_dates[0].split(",");
				var reference_dates = [];
		                var new_station_object = {};
				var new_ref_date = [];
		                new_station_object['name'] = markerchartdict[i+1];
		                new_station_object['keys'] = ['x', 'y', 'name'];
				for(var j=0;j<arraydate.length;j++)
		        	{
					if (j%2==0){
						new_ref_date = [];
						new_ref_date.push(Date.UTC(arraydate[j].slice(0, 4),arraydate[j].slice(5, 7)-1,arraydate[j].slice(8, 10)));//month start 0-11
			                        new_ref_date.push(i+1);
						new_ref_date.push('begindate');
						reference_dates.push(new_ref_date);			
				        }else{
						new_ref_date = [];
						new_ref_date.push(Date.UTC(arraydate[j].slice(0, 4),arraydate[j].slice(5, 7)-1,arraydate[j].slice(8, 10),11,59,59));//Date.UTC(year,month-1,day) start at 12h00
					        new_ref_date.push(i+1);
						new_ref_date.push('enddate');
						reference_dates.push(new_ref_date);
						new_ref_date = [];
						new_ref_date.push(Date.UTC(arraydate[j].slice(0, 4),arraydate[j].slice(5, 7)-1,arraydate[j].slice(8, 10),12));
					        new_ref_date.push(null);
						reference_dates.push(new_ref_date);
					}
				}
		                new_station_object['data'] = reference_dates;                      
				series_stations.push(new_station_object);
				//calcul data percent
				var dateDiff =  lastDate-firstDate;
				var totalDays =dateDiff / (1000 * 3600 * 24);
				data_availability_percent[i+1] = (($scope.results[i].nb_rinex_files*100)/totalDays).toFixed(2) + ' % ';
			}	
		}
		
		//Plot the chart
		Highcharts.chart('temporalcontainer', {
		    
			    chart: {
					type: 'line',
		                        zoomType: 'xy',
					events: {
						load : function () {
						  mainchart = this; // `this` is the reference to the chart
						}
               
					      }
				    },
			    title: {
				text: titleRange
			    },
			    legend: {
				enabled: false
			    },
			    xAxis: {
                                      
                                      	type: 'datetime',
                                        labels: {
                                    		format: '{value:%e %b %Y}'
                                        }
			    },
			    yAxis: [
				    {
				      min: 0,
                                      max: 7,
                                      tickPositions : markerchartarray,
                                      scrollbar: {
						    enabled: true
						},
                                      title: {
		                        text: ''
		                      },
				      labels: {
						formatter: function() {
						    var value = markerchartdict[this.value];
						    return  value !== 'undefined' ? value: this.value;
						} 
						
					    }
				    },
				    {
                                      opposite:true,
				      linkedTo: 0,
                                      tickPositions : markerchartarray,
				      title: {
		                        text: ''
		                      },
				      labels: {
						enabled: true,
						formatter: function() {
						    var value = data_availability_percent[this.value];
						    if (value===undefined){ value = "";}
						    var tooltiptext = "Percent of data between install date and current date";
						    return  '<span title="' + tooltiptext + '">' + value + '</span>';
						},
						useHTML : true
					    }	
				    }
				  ],
			    
			    plotOptions: {
				series: {
					    cursor: 'pointer',
					    marker: {
						    enabled: true,
                                                    symbol: "square"
						},
					    events: {
						click: function (event) 
                                                {
						   	
                                                        globalmarker = this.name;
							var y = $scope.searchkey(globalmarker,markerchartdict);
							if (!hasPlotBand) {
								mainchart.yAxis[0].addPlotBand({                      
								    from: parseInt(y) - 0.25,
								    to: parseInt(y) + 0.25,
								    id: globalmarker
								});
								angular.forEach($scope.gridOptions.data, function (data, index) {
									if(data.marker===globalmarker){
										$scope.gridApi.selection.selectRow($scope.gridOptions.data[index]);
										$scope.gridApi.grid.element[0].getElementsByClassName("ui-grid-viewport")[0].scrollTop = index * $scope.gridApi.grid.options.rowHeight;
									}
								});
								$scope.$apply();
							} else {
								mainchart.yAxis[0].removePlotBand(globalmarker);
								angular.forEach($scope.gridOptions.data, function (data, index) {
									if(data.marker===globalmarker){
										$scope.gridApi.selection.unSelectRow($scope.gridOptions.data[index]);
									}
								});
                                                                $scope.$apply();
							}
							hasPlotBand = !hasPlotBand;
						}
					    }
					}
			    },
                            tooltip : {
				    formatter: function() {
					var tooltip;
					if (this.key == 'begindate') {
					    tooltip = '<b>Start date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
					}
					else if (this.key == 'enddate'){
					    tooltip = '<b>End date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
					}
					return tooltip;
				    }
				},
			      series: series_stations
			  
		});
		$scope.modalInstance.opened.then(function () {
			$scope.loading = false;
                	$scope.modalInstance.close();//stop the spinner loading
		});
	};

	/****************************************************************************************
	* initdataavailable function allow to init the temporal chart for one station and heatmap
	****************************************************************************************/
	$scope.initdataavailable = function(data) {
		var arraydate = [];
		var results = [];
		var new_station_object = {};
		var listDate = data[0].rinex_files_dates[0].split(",");
		for(var i=0;i<listDate.length;i+=2)
        	{
                        var dt = new Date(listDate[i].slice(0,10));
			var end = new Date(listDate[i+1].slice(0,10));
			while (dt <= end) {
				arraydate.push(dt.toISOString().slice(0,10));
				dt.setUTCDate(dt.getUTCDate() + 1);
		    	}
		}
		new_station_object['marker'] = data[0].marker;
		new_station_object['date_from'] = data[0].date_from;
		new_station_object['date_to'] = data[0].date_to;
		new_station_object['rinex_files_dates'] = arraydate;
		results.push(new_station_object);
		$scope.temporalchartforone(results,'');
		$scope.heatmap(results); 
	};
	/*********************************************************************************
	* temporalchartforone function allow to display the temporal chart for one station
	*********************************************************************************/
	$scope.temporalchartforone = function(data,params) {
			var reference_dates = [];
			var fromdateofrange = "";
			var endateofrange = "";		
			if(data[0].rinex_files_dates.length>0)
			{
				var arraydate = data[0].rinex_files_dates;
                                $scope.filefromdateselected = data[0].date_from.substring(0,10);
				var firstDate = data[0].date_from.substring(0,10);
				firstDate= Date.UTC(firstDate.substring(0, 4),firstDate.substring(5, 7)-1,firstDate.substring(8, 10));
				var lastDate = "";
				if (data[0].date_to === null)
				{
					lastDate = $scope.currentDateFormat();
					lastDate = lastDate.substring(0,10);
					$scope.filetodateselected = lastDate;
					lastDate= Date.UTC(lastDate.substring(0, 4),lastDate.substring(5, 7)-1,lastDate.substring(8, 10));
				}else{
					lastDate=data[0].date_to;
					lastDate = lastDate.substring(0,10);
					$scope.filetodateselected = lastDate;
					lastDate= Date.UTC(lastDate.substring(0, 4),lastDate.substring(5, 7)-1,lastDate.substring(8, 10));
				}
				
		                var data_availability_percent = {};
		               
				for(var j=0;j<arraydate.length;j++)
		        	{
					var year = arraydate[j].substring(0, 4);
					var month = arraydate[j].substring(5, 7);
					var day = arraydate[j].substring(8, 10);
					if(j==0){
						var new_ref_date = [];
		                                new_ref_date.push(Date.UTC(year,month-1,day));//month start 0-11
		                                new_ref_date.push(1);
						new_ref_date.push('begindate');
						reference_dates.push(new_ref_date);
		                               
					}else{
						var myday = Date.UTC(year,month-1,day);
		                                
						if((myday-precday)>(24 * 60 * 60 * 1000))
		                                {
							var date_end = precday + (12 * 60 * 60 * 1000);//12 because Date.UTC(year,month-1,day) start at 12h00
							var new_ref_date = [];
							new_ref_date.push(date_end);
							new_ref_date.push(1);
							new_ref_date.push('enddate');
							reference_dates.push(new_ref_date);
							new_ref_date = [];
							new_ref_date.push(date_end + (60 * 1000));
					        	new_ref_date.push(null);
							reference_dates.push(new_ref_date);
							var date_start = myday;
							new_ref_date = [];
							new_ref_date.push(myday);
							new_ref_date.push(1);
							new_ref_date.push('begindate');
							reference_dates.push(new_ref_date);
						}
					}
		                        var precday = Date.UTC(year,month-1,day);
					if(j==arraydate.length-1)//case end of list
		                        {
						var new_ref_date = [];
						var year = arraydate[j].substring(0, 4);
						var month = arraydate[j].substring(5, 7);
						var day = arraydate[j].substring(8, 10);
						var date_end = Date.UTC(year,month-1,day);
						new_ref_date.push(date_end);
						new_ref_date.push(1);
						new_ref_date.push('enddate');
						reference_dates.push(new_ref_date);      
					}
       
				}
				//calcul data percent
		                if(params.indexOf("date_range=") !== -1)
				{
					var startpos = params.indexOf("date_range=");
				        var endpos = "";
					if(params.indexOf("&", startpos) !== -1)
					{
						endpos = params.indexOf("&", startpos);
					}else{
				        	endpos = params.length;
					}
					var param = params.substring(startpos + 11, endpos);
					if((param.substring(0,1)!=",")&&(param.length!=21))//case only from date
					{
						fromdateofrange = param.substring(0, 10);
						endateofrange = "";
					}else if(param.substring(0,1)==",")//case only end date
					{
						fromdateofrange = "";
						endateofrange = param.substring(1, 11);
					}else if(param.length==21)// case from and end dates
					{
						fromdateofrange = param.substring(0, 10);
						endateofrange = param.substring(11, 21);
					}
					if(fromdateofrange!=""){
		                        	firstDate = Date.UTC(fromdateofrange.substring(0, 4),fromdateofrange.substring(5, 7)-1,fromdateofrange.substring(8, 10));
						$scope.filefromdateselected = fromdateofrange;
					}
					if(endateofrange!=""){
		                        	lastDate = Date.UTC(endateofrange.substring(0, 4),endateofrange.substring(5, 7)-1,endateofrange.substring(8, 10));
						$scope.filetodateselected = endateofrange;
					}
				}
				
				var dateDiff =  lastDate-firstDate;
				var totalDays =dateDiff / (1000 * 3600 * 24) + 1;
				data_availability_percent[1] = ((arraydate.length*100)/totalDays).toFixed(2) + ' % ';
		
			//Plot the chart
			Highcharts.chart('temporalplaceholder', {
			    
				    chart: {
						type: 'line',
				                zoomType: 'xy'
					    },
				    title: {
					text: ''
				    },
				    legend: {
					enabled: false
				    },
				    xAxis: {
		                              
		                              	type: 'datetime',
		                                labels: {
		                            		format: '{value:%e %b %Y}'
		                                }
				    },
				    yAxis: [
					    {
		                              scrollbar: {
							    enabled: false
							},
		                              title: {
				                text: ''
				              },
					      labels: {
							formatter: function() {
							    var value =data[0].marker;
							    return  value !== 'undefined' ? value: this.value;
							} 
						
						    }
					    },
					    {
		                              opposite:true,
					      linkedTo: 0,
					      title: {
				                text: ''
				              },
					      labels: {
							enabled: true,
							formatter: function() {
							    var value = data_availability_percent[this.value];
							    if (value===undefined){ value = "";}
							    var tooltiptext = "Percent of data between " + $scope.filefromdateselected + " and " + $scope.filetodateselected;
							    return  '<span title="' + tooltiptext + '">' + value + '</span>';
							},
							 useHTML: true
						    }	
					    }
					  ],
				    
				    plotOptions: {
					series: {
						   marker: {
							    enabled: true,
		                                            symbol: "square"
							} 
						}
				    },
		                   tooltip : {
					    formatter: function() {
						var tooltip;
						if (this.key == 'begindate') {
						    tooltip = '<b>Start date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
						}
						else if (this.key == 'enddate'){
						    tooltip = '<b>End date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
						}
						return tooltip;
					    }
					},
				    series: [{
					name: data[0].marker,
		                        keys: ['x', 'y', 'name'],
					data: reference_dates
				    }]
		
			});

		}
	};

	/************************************************************************************
	* heatmap function allow to display the heatmap of file availability for one station
	************************************************************************************/
	$scope.heatmap = function(data) {
		var percent_of_data = [];
	        var list_of_years = []; 
		var year_indice = 0;
	        var posnextmonth = 0;
	        var cpt_day = 0;
		var lastdayofmonth = '';
	        var startpos = 0;
		var pos = 0;
		if(data[0].rinex_files_dates.length>0)
		{
			//prepare the data for the timeseries
			var arraydate = data[0].rinex_files_dates;
			while(posnextmonth<arraydate.length)
			{
				var percent_of_month = 0;
				var total_day = 0;
				var month_number = 0;
				var year = arraydate[posnextmonth].substring(0, 4);
				var month = arraydate[posnextmonth].substring(5, 7);
				var day = arraydate[posnextmonth].substring(8, 10);
				//calcul data percent
				switch(month) {
				    case '01':
					total_day=31;
			                month_number=0;
					lastdayofmonth=year+"/"+month+"/"+total_day; 
					break;
				    case '02':
					if (((parseInt(year)%4==0) && (parseInt(year)%100!=0)) || (parseInt(year)%400==0))
					{
	 					total_day=29;
					}else{
						total_day=28;
					}
			                month_number=1;
			                lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '03':
					total_day=31;
			                month_number=2;
			                lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '04':
					total_day=30;
			                month_number=3;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '05':
					total_day=31;
			                month_number=4;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '06':
					total_day=30;
			                month_number=5;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '07':
					total_day=31;
			                month_number=6;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '08':
					total_day=31;
			                month_number=7;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '09':
					total_day=30;
			                month_number=8;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '10':
					total_day=31;
			                month_number=9;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '11':
					total_day=30;
			                month_number=10;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				    case '12':
					total_day=31;
			                month_number=11;
					lastdayofmonth=year+"/"+month+"/"+total_day;
					break;
				}
				while(pos<arraydate.length)
				{	
					var iteryear = arraydate[pos].substring(0, 4);
					var itermonth = arraydate[pos].substring(5, 7);
					if(month==itermonth && year==iteryear)
			                {
						cpt_day = cpt_day + 1;
					}else{
						break;
					}    	             
				        pos = pos + 1;                       
				}
				
				percent_of_month = (((cpt_day)*100)/total_day).toFixed(2);
				if(list_of_years.length==0)
			        {
					list_of_years.push(year);
				}else if(list_of_years[list_of_years.length-1]!=year)
			        {
					list_of_years.push(year);
					year_indice = year_indice + 1;
				}
				var new_ref_date = [];
			        new_ref_date.push(month_number);
			        new_ref_date.push(year_indice);
				new_ref_date.push(percent_of_month);
				percent_of_data.push(new_ref_date);
			        cpt_day = 0;
			        total_day = 0;
				percent_of_month = 0;

				posnextmonth = pos;                       
			}
			//Plot the heatmap
			Highcharts.chart('heatmapplaceholder', {
				chart: {
					type: 'heatmap',
					marginTop: 50,
					marginBottom: 80,
					plotBorderWidth: 1
				    },


				    title: {
					text: 'Percent of data availability in the month'
				    },

				    xAxis: {
				        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
				    },

				    yAxis: {
					categories: list_of_years,
					title: {
					        text: ''
					      }
				    },

				    colorAxis: {
				        min: 0,
				        max: 100,
				    	stops: [
						    [0, '#FFFFFF'],
						    [0.2, '#F30000'],
						    [0.5, '#ffff00'],
						    [1, '#5DBA76']
						],
				    	minColor: '#ffff00',
				    	maxColor: '#ff0000'
				    },

				    legend: {
				        align: 'center',
					    verticalAlign: 'bottom',
					    floating: true,
					    x: 0,
					    y: 0
				    },

				    tooltip: {
					formatter: function () {
					    return '<b>Date : ' + this.series.xAxis.categories[this.point.x] + ' ' + this.series.yAxis.categories[this.point.y] + '</b><br><b>' +
						this.point.value + '</b> %';
					}
				    },

				    plotOptions: {
					    series: {
						events: {
						    click: function (event) {
				                        $scope.detailflag = true;
				                        $timeout(function(){
				                        	$scope.calendarheatmap(event.point.series.xAxis.categories[event.point.x],event.point.series.yAxis.categories[event.point.y]);
				                        },100);
					
						    }
						}
					    }
					},
		
				    series: [{
					name: data[0].marker,
					borderWidth: 1,
		                        data:percent_of_data
				        
				    }]
			       });
		}

	};

	
	/*****************************************************************************
	* calendarheatmap function allow to display the calendar of the month selected
	*****************************************************************************/
	$scope.calendarheatmap = function(month,year) {
                var month_number;
                var month_str;
                var total_day;
                var data_date = {};
		switch(month) {
		    case 'Jan':
                        month_number=0;
			month_str='01';
                        total_day=31; 
			break;
		    case 'Feb':
                        month_number=1;
                        month_str='02';
			if (((parseInt(year)%4==0) && (parseInt(year)%100!=0)) || (parseInt(year)%400==0))
			{
				total_day=29;
			}else{
				total_day=28;
			}
			break;
		    case 'Mar':
                        month_number=2;
                        month_str='03';
			total_day=31;
			break;
		    case 'Apr':
                        month_number=3;
                        month_str='04';
			total_day=30;
			break;
		    case 'May':
                        month_number=4;
                        month_str='05';
			total_day=31;
			break;
		    case 'Jun':
                        month_number=5;
                        month_str='06';
			total_day=30;
			break;
		    case 'Jul':
                        month_number=6;
                        month_str='07';
			total_day=31;
			break;
		    case 'Aug':
                        month_number=7;
                        month_str='08';
			total_day=31;
			break;
		    case 'Sep':
                        month_number=8;
                        month_str='09';
			total_day=30;
			break;
		    case 'Oct':
                        month_number=9;
                        month_str='10';
			total_day=31;
			break;
		    case 'Nov':
                        month_number=10;
                        month_str='11';
			total_day=30;
			break;
		    case 'Dec':
                        month_number=11;
                        month_str='12';
                        total_day=31;
			break;
		}
                //prepare the data
		var arraydate = '';
		var myparams = 'dateRange=' + year + "-" + month_str + "-01|" + year + "-" + month_str + "-" + total_day + "&marker=" + $scope.results[0].marker;
		var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combination/';
		var req = $resource(adress +':params'+'/json' ,{params:'@params'}, {query: {method: 'get', isArray: true}});
		req.query({params:myparams}).$promise.then(function(data) 
		{
			if (data.length>0)
			{
				arraydate = data;
				$scope.datesonmonth = data;
				for(var i=0;i<arraydate.length;i++)
				{	
				        if(year + "-" + month_str==arraydate[i].reference_date.substring(0, 7))
				        {
						var year_iter = arraydate[i].reference_date.substring(0, 4);
						var month_iter = arraydate[i].reference_date.substring(5, 7);
						var day_iter = arraydate[i].reference_date.substring(8, 10);
						data_date[Date.UTC(year_iter,month_iter-1,day_iter)/1000] = 1;
					}			
				}
				$scope.dateselectedarray = [];
				//plot the calendar
				$("#cal-heatmap").html("");
				var cal = new CalHeatMap();
				cal.init({
						itemSelector: "#cal-heatmap",
						start: new Date(year, month_number),
				                data:data_date,
						domain : "month",
				                range : 1,
						subDomain : "x_day",
				                cellSize: 30,
				                subDomainTextFormat: "%d",
						legend: [1],
				                legendColors:["#F30000","#5DBA76"],
				                displayLegend: false,
						itemName: [''],
				                domainLabelFormat: "%B %Y",
						onClick: function(date,nb) {
							if ($scope.shiftkeypressed) {
								$scope.dateselectedarray.push(date);
								if($scope.dateselectedarray.length==2){
									if($scope.dateselectedarray[1].getDate()<$scope.dateselectedarray[0].getDate())
									{
										var tempdate = $scope.dateselectedarray[0];
										$scope.dateselectedarray[0] = $scope.dateselectedarray[1];
										$scope.dateselectedarray[1] = tempdate;	
									}
									var nextdate = $scope.dateselectedarray[0].getDate();
									var i = 1;
									while(nextdate<$scope.dateselectedarray[1].getDate())
									{     
										var newdate = new Date($scope.dateselectedarray[0]);
										newdate.setDate(newdate.getDate()+i);
										var dateCalformat = new Date(newdate.getFullYear(),newdate.getMonth(),newdate.getDate());
										if(dateCalformat.getDate()<$scope.dateselectedarray[1].getDate()){
									      		$scope.dateselectedarray.push(dateCalformat);
										}
									      	nextdate = dateCalformat.getDate();
										i++;
									}
								}
								cal.options.highlight = $scope.dateselectedarray;
								cal.highlight($scope.dateselectedarray);
								$scope.shiftkeypressed = false;  
							}else{
								var index = -1;
								for (var i = 0; i < $scope.dateselectedarray.length; ++i) {
								    if ($scope.dateselectedarray[i].valueOf() == date.valueOf()) {
									index = i;
									break;
								    }
								}
								if(index>-1)
								{		
									$scope.dateselectedarray.splice(index,1);	
								}else{
									$scope.dateselectedarray.push(date);
								}
								if($scope.dateselectedarray.length!=0){
									cal.options.highlight = $scope.dateselectedarray;
									cal.highlight($scope.dateselectedarray);
								}else{
									cal.options.highlight = $scope.dateselectedarray;
									cal.highlight(new Date(1000,0,1));
								}
							}
						}
				               
					});
			}
		});

	};
	
	/*************************************************************************
	* downloadselectedonmonth function to handle downloading files in calendar  
	*************************************************************************/
	$scope.downloadselectedonmonth = function() {
		var clickdate = "";
		while($scope.dateselectedarray.length>0)
		{
			clickdate = $scope.convertDateFormat($scope.dateselectedarray[0]);
			for(var i=0;i<$scope.datesonmonth.length;i++)
			{
				var mydate = $scope.datesonmonth[i].reference_date.substring(0, 10);
				if(clickdate==mydate)
				{
					if ($scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming']!="")
					{
						var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
					}else{
						var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/"  + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
					}
					var windowvalue = window.open(link,$scope.datesonmonth[i]['name']);
					
					if(windowvalue.name!=""){
						$scope.dateselectedarray.shift();
					}
				}
			}
		}
	};

	/**********************************************************************************************
	* changedReferenceFromDate function allows to know what's users selected in "file date install"  
	**********************************************************************************************/
	$scope.changedReferenceFromDate = function(date,date_from) {
		if(date==null){
			$scope.filefromdateselected = date_from.substring(0,10);
		}else{
			$scope.filefromdateselected = date.toISOString().slice(0,10);
		}
		var params = 'marker='+ $scope.results[0].marker + '&date_range=' + $scope.filefromdateselected + ',' + $scope.filetodateselected;
		var arraydate = [];
		var arraydatetemp = [];
		var data = [];
		var new_station_object = {};
		var listDate = $scope.results[0].rinex_files_dates[0].split(",");
		for(var i=0;i<listDate.length;i+=2)
        	{
                        var dt = new Date(listDate[i].slice(0,10));
			var end = new Date(listDate[i+1].slice(0,10));
			while (dt <= end) {
				arraydatetemp.push(dt.toISOString().slice(0,10));
				dt.setUTCDate(dt.getUTCDate() + 1);
		    	}
		}
		var startindex = arraydatetemp.indexOf($scope.filefromdateselected);
                if(startindex==-1){
			var afterdatesarray = arraydatetemp.filter(function(d) {
			    return new Date(d) - new Date($scope.filefromdateselected) > 0;
			});	
			startindex = arraydatetemp.indexOf(afterdatesarray[0]);
		} 
		var endindex = arraydatetemp.indexOf($scope.filetodateselected);
		if(endindex==-1){
			var beforedatesarray = arraydatetemp.filter(function(d) {
			    return new Date(d) - new Date($scope.filetodateselected) < 0;
			});
			endindex = arraydatetemp.indexOf(beforedatesarray[beforedatesarray.length-1]);
		}
		arraydate = arraydatetemp.slice(startindex,endindex+1);
		new_station_object['marker'] = $scope.results[0].marker;
		new_station_object['date_from'] = $scope.results[0].date_from;
		new_station_object['date_to'] = $scope.results[0].date_to;
		new_station_object['rinex_files_dates'] = arraydate;
		data.push(new_station_object);		
		$scope.temporalchartforone(data,params);
		$scope.heatmap(data);
		$("#cal-heatmap").html("");
	};
        /*******************************************************************************************
	* changedReferenceToDate function allows to know what's users selected in "file date remove"  
	*******************************************************************************************/
	$scope.changedReferenceToDate = function(date) {
		if(date==null){
			var lastDate = $scope.currentDateFormat();
			$scope.filetodateselected = lastDate.substring(0,10);	
		}else{
	    		$scope.filetodateselected = date.toISOString().slice(0,10);
		}
		var params = 'marker='+ $scope.results[0].marker + '&date_range=' + $scope.filefromdateselected + ',' + $scope.filetodateselected;
		var arraydate = [];
		var arraydatetemp = [];
		var data = [];
		var new_station_object = {};
		var listDate = $scope.results[0].rinex_files_dates[0].split(",");
		for(var i=0;i<listDate.length;i+=2)
        	{
                        var dt = new Date(listDate[i].slice(0,10));
			var end = new Date(listDate[i+1].slice(0,10));
			while (dt <= end) {
				arraydatetemp.push(dt.toISOString().slice(0,10));
				dt.setUTCDate(dt.getUTCDate() + 1);
		    	}
		}
		var startindex = arraydatetemp.indexOf($scope.filefromdateselected);
		if(startindex==-1){
			var afterdatesarray = arraydatetemp.filter(function(d) {
			    return new Date(d) - new Date($scope.filefromdateselected) > 0;
			});	
			startindex = arraydatetemp.indexOf(afterdatesarray[0]);
		} 
		var endindex = arraydatetemp.indexOf($scope.filetodateselected);
		if(endindex==-1){
			var beforedatesarray = arraydatetemp.filter(function(d) {
			    return new Date(d) - new Date($scope.filetodateselected) < 0;
			});
			endindex = arraydatetemp.indexOf(beforedatesarray[beforedatesarray.length-1]);
		}		
		arraydate = arraydatetemp.slice(startindex,endindex+1);
		new_station_object['marker'] = $scope.results[0].marker;
		new_station_object['date_from'] = $scope.results[0].date_from;
		new_station_object['date_to'] = $scope.results[0].date_to;
		new_station_object['rinex_files_dates'] = arraydate;
		data.push(new_station_object);
		$scope.temporalchartforone(data,params);
		$scope.heatmap(data);
		$("#cal-heatmap").html("");
	};

	/*******************************************************************
	* switchmap function allow to switch between satellite or simple map
	*******************************************************************/
	$scope.switchmap = function () {
		var MyControl = L.Control.extend({
		    options: {
			position: 'bottomleft'
		    },
		    onAdd: function (map) {
			var container = L.DomUtil.create('div', 'switchmap-control');
			container.innerHTML += '<button id="mapbuttonid" style="background-image:url(images/satellitemap.png);width:70px;height:70px;"></button>';
			$scope.imagemap = "satellitemap";       	
			$compile( container )($scope);
			return container;
		    }
		});
		$scope.mymap.addControl(new MyControl());
		$('.switchmap-control').click(function(event) {
			if($scope.imagemap == "satellitemap")
			{
				L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
				document.getElementById("mapbuttonid").style.backgroundImage= "url(images/simplemap.png)";
				$scope.imagemap = "simplemap";
			}else{
				L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
				document.getElementById("mapbuttonid").style.backgroundImage= "url(images/satellitemap.png)";
				$scope.imagemap = "satellitemap";
			}
		});
	};

	//T3

	/*************************************************************
	* requestQualityCheck function allows to request quality check
	*************************************************************/
	$scope.requestQualityCheck = function () {
		var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combinationT3/';
		var params = 'marker='+ $scope.results[0].marker + '&dateRange=' + $scope.fileT3fromdateselected + '|' + $scope.fileT3todateselected;
		var req = $resource(adress +':myquery'+'/json' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: true}});
		req.query({myquery:params}).$promise.then(function(data) 
		{
			if (data.length>0)
			{	
				$scope.qualitycheckplot(data);
			}
		});
	};

	/*****************************************************************************
	* initQualityCheck function allows to request quality check for initialization
	******************************************************************************/
	$scope.initQualityCheck = function (data) {
		if(data[0].rinex_files_dates.length>0)
		{
			var arraydate = data[0].rinex_files_dates[0].split(",");
			$scope.fileT3todateselected = arraydate[arraydate.length-1].substring(0, 10);
			var datetemp= Date.UTC($scope.fileT3todateselected.substring(0, 4),$scope.fileT3todateselected.substring(5, 7)-1,$scope.fileT3todateselected.substring(8, 10))-(2*24*60*60*1000);
			$scope.fileT3fromdateselected = new Date(datetemp).toISOString().slice(0,10);
			console.log($scope.fileT3fromdateselected);
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combinationT3/';
			var params = 'marker='+ $scope.results[0].marker + '&dateRange=' + $scope.fileT3fromdateselected + '|' + $scope.fileT3todateselected;
			var req = $resource(adress +':myquery'+'/json' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: true}});
			req.query({myquery:params}).$promise.then(function(data) 
			{
				if (data.length>0)
				{	
					$scope.qualitycheckplot(data);
				}
			});
		}
	};

	/*********************************************************************************
	* qualitycheckplot function allow to display the quality check plot for one station
	*********************************************************************************/
	$scope.qualitycheckplot = function(data) {
			var obs_have = [];
			var epoch_compl = [];
			var cyc_slips = [];
			var mp1 = [];
			var mp2 = [];
			var firstDate = '';
			var mp1_temp = [];
			var mp2_temp = [];
			var obs_have_L2 = [];
			
			firstDate = data[0].reference_date.substring(0,10);
			firstDate= Date.UTC(firstDate.substring(0, 4),firstDate.substring(5, 7)-1,firstDate.substring(8, 10));
			for(var i=0;i<data.length;i++)
		        {
				obs_have.push(data[i]["qc_report_summary"].obs_have * 100/data[i]["qc_report_summary"].obs_expt);
				cyc_slips.push(data[i]["qc_report_summary"].cyc_slps);
				for(var j=0;j<data[i]["qc_report_summary"]["qc_constellation_summary"].length;j++)
		        	{
					for(var k=0;k<data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"].length;k++)
		        		{
						if((data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"][k]["obsnames"].name == "C1")&&(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["constellation"].name == "GPS"))
						{
							mp1_temp.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"][k].cod_mpth);
						}
						if((data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"][k]["obsnames"].name == "P2")&&(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["constellation"].name == "GPS"))
						{
							mp2_temp.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"][k].cod_mpth);
						}		
					}
					if(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["constellation"].name == "GPS")
					{
						epoch_compl.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j].epo_have * 100/data[i]["qc_report_summary"]["qc_constellation_summary"][j].epo_expt);
						for(var k=0;k<data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"].length;k++)
		        			{
							if(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].obsnames.name=="L2")
							{
								obs_have_L2.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_have * 100/data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_expt);
							}	
						}
					}
				}
				var sum = mp1_temp.reduce(function(a, b) { return a + b; });
				var avg = sum / mp1_temp.length;
				mp1.push(avg);
				var sum = mp2_temp.reduce(function(a, b) { return a + b; });
				var avg = sum / mp2_temp.length;
				mp2.push(avg);	
			}
			
			//Plot the chart
			Highcharts.chart('qcplaceholder', {
			    
				    chart: {
						type: 'column',
				                zoomType: 'x'
					    },
				    title: {
					text: ""
				    },
				    legend: {
					enabled: true
				    },
				    xAxis: {
		                              
		                              	type: 'datetime',
		                                labels: {
		                            		format: '{value:%e %b %Y}'
		                                }
				    },
				    yAxis: [
					    {
		                              scrollbar: {
							    enabled: false
							},
		                              title: {
				                text: ''
				              }
					    }
					    
					  ],
				    
				    plotOptions: {
					
				    },
		                   tooltip : {

					},
				    series: [{
						name: 'Epoch completeness (%)',
						data: epoch_compl,
						pointStart: firstDate,
                                                pointInterval: 24 * 3600 * 1000,// one day
						dataLabels: {
						    enabled: true,
						    color: '#FFFFFF',
						    y: 25, // 10 pixels down from the top
						    style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						    }
						}
					    }, {
						name: 'Obs have L2 GPS (%)',
						data: obs_have_L2,
						pointStart: firstDate,
                                                pointInterval: 24 * 3600 * 1000, // one day
						dataLabels: {
						    enabled: true,
						    color: '#FFFFFF',
						    format: '{point.y:.1f}', // one decimal
						    y: 25, // 10 pixels down from the top
						    style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						    }
						}
					    }, {
						name: 'Obs have all GNSS (%)',
						data: obs_have,
						pointStart: firstDate,
                                                pointInterval: 24 * 3600 * 1000, // one day
						dataLabels: {
						    enabled: true,
						    color: '#FFFFFF',
						    format: '{point.y:.1f}', // one decimal
						    y: 25, // 10 pixels down from the top
						    style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						    }
						}
					    }, {
						name: 'Cyc slips (#)',
						data: cyc_slips,
						pointStart: firstDate,
                                                pointInterval: 24 * 3600 * 1000, // one day
						dataLabels: {
						    enabled: true,
						    color: '#FFFFFF',
						    y: 25, // 10 pixels down from the top
						    style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						    }
						}
					    }, {
						name: 'Mp1 (cm)',
						data: mp1,
						pointStart: firstDate,
                                                pointInterval: 24 * 3600 * 1000, // one day
						dataLabels: {
						    enabled: true,
						    color: '#FFFFFF',
						    format: '{point.y:.1f}', // one decimal
						    y: 25, // 10 pixels down from the top
						    style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						    }
						}
					    }, {
						name: 'Mp2 (cm)',
						data: mp2,
						pointStart: firstDate,
                                                pointInterval: 24 * 3600 * 1000, // one day
						dataLabels: {
						    enabled: true,
						    color: '#FFFFFF',
						    format: '{point.y:.1f}', // one decimal
						    y: 25, // 10 pixels down from the top
						    style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						    }
						}
					    }]
		
			});

	};

	
	/**********************************************************************************************
	* changedT3ReferenceFromDate function allows to know what's users selected in "file date install"  
	**********************************************************************************************/
	$scope.changedT3ReferenceFromDate = function(item) {
		if(item==undefined){
			$scope.fileT3fromdateselected=undefined;
		}else{
	    		$scope.fileT3fromdateselected = $scope.convertDateFormat(item);
		}
	};
        /*******************************************************************************************
	* changedT3ReferenceToDate function allows to know what's users selected in "file date remove"  
	*******************************************************************************************/
	$scope.changedT3ReferenceToDate = function(item) {
		if(item==undefined){
			$scope.fileT3todateselected=undefined;
		}else{
	    		$scope.fileT3todateselected = $scope.convertDateFormat(item);
		}
	};
	
}]);

'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:FileCtrl
 * @description
 * # FileCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
  .controller('FileCtrl', ["$scope", "serviceHistory", "serviceResource", "$uibModal", "$window", "$resource", "$routeParams", "$templateCache", "CONFIG", "$http", "$location", "$q", "$cookieStore", "$interval", "$timeout", function ($scope, serviceHistory,serviceResource,$uibModal,$window,$resource,$routeParams,$templateCache,CONFIG,$http,$location,$q, $cookieStore,$interval, $timeout) {
	$scope.results = '';
  
	/*********************************************
	* addhistory function allows to add in history
	*********************************************/
	$scope.addhistory = function() {
	    serviceHistory.addhistory(CONFIG.server + CONFIG.clientname + "/#/file/" + $routeParams.querystring);
	};

	/**************************************************************************************
	* currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
	**************************************************************************************/
	$scope.currentDateFormat = function() {
	    var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1;
	    var HH = today.getHours();
	    var MM = today.getMinutes();
	    var SS = today.getSeconds();
	    var yyyy = today.getFullYear();
	    if(dd<10){
		dd='0'+dd
	    } 
	    if(mm<10){
		mm='0'+mm
	    }
	    if (HH < 10) {
		HH = "0" + HH;
	    }
	    if (MM < 10) {
		MM = "0" + MM;
	    }
	    if (SS < 10) {
		SS = "0" + SS;
	    }   
	    return today = yyyy+'-'+mm+'-'+dd+' '+HH+':'+MM+':'+SS;
	};
       
        /*************************************************************
	* loadingdialog function allows to open a modal with a spinner 
	**************************************************************/
	$scope.loadingdialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalLoading.html',
		size: 'm',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/*****************************************************
	* requestFiles function allows to request metadata
	******************************************************/
	$scope.requestFiles = function () {
		var adress = CONFIG.server  + CONFIG.glassapi + '/webresources/files/combination/';
		var req = $resource(adress +':myquery'+'/json' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: true}});
		$scope.request_json = adress + $routeParams.querystring + '/json';
	        
		$scope.loading = true;
                $scope.loadingdialog();
		req.query({myquery:$routeParams.querystring}).$promise.then(function(data) 
		{
			if (data.length>0)
			{	
				$scope.nodata = false;
				$scope.modalInstance.opened.then(function () {
					$scope.loading = false;//stop the spinner loading
                                	$scope.modalInstance.close();//stop the spinner loading
				});
				$scope.results = data;
				$scope.currentdate = $scope.currentDateFormat();
				
                                $scope.gridOptions.data = $scope.results;
			}else{
                                $scope.modalInstance.close();//stop the spinner loading
				$scope.openmessage($scope.messageinfonodata);
			}
		});
	};
        $scope.requestFiles();

	/*****************************************
	* gridHandler function handles grid part
	******************************************/
	$scope.gridHandler = function() {
		var screenwidth = screen.width;
		//custom selection on grid with checkbox
		$templateCache.put('ui-grid/selectionRowHeaderButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-row-selected': row.isSelected}\" ><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"row.isSelected\" ng-click=\"row.isSelected=!row.isSelected;selectButtonClick(row, $event)\">&nbsp;</div>"
  );

  		$templateCache.put('ui-grid/selectionSelectAllButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-all-selected': grid.selection.selectAll}\" ng-if=\"grid.options.enableSelectAll\"><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"grid.selection.selectAll\" ng-click=\"grid.selection.selectAll=!grid.selection.selectAll;headerButtonClick($event)\"></div>"
  ); 
		//define all grid options and their columns
		$scope.gridOptions = {
                        enableGridMenu: false,
		    	enableFiltering: false,
			enableSorting: true,
			enableRowSelection: true,
			enableSelectAll: true,
			multiSelect: true,
                        exporterMenuCsv: false,
			exporterMenuPdf: false,
                        enableColumnMoving: true,
                        enableColumnResizing: true,
                        showGridFooter: true,
			//all function regarding the ui-grid
                        onRegisterApi: function(gridApi){ 
      				$scope.gridApi = gridApi;
    			},
			//definition of each column in the grid
			columnDefs: [
				  { name:'File Name', field: 'name', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.downloadrinexfile(row.entity.name,row.entity.data_center.hostname,row.entity.data_center.data_center_structure.directory_naming)">{{ COL_FIELD }}</a></div>'},
				  { name:'File Type', field: 'file_type.format',enableColumnMenu: false},
                                  { name:'Reference Date', field: 'reference_date',enableColumnMenu: false, headerTooltip: 'date of the observation'},
                                  { name:'Published Date', field: 'published_date',enableColumnMenu: false, headerTooltip: 'first publication date'},
                                  { name:'Revision Date', field: 'revision_date',enableColumnMenu: false, headerTooltip: 'last date when the file has been revised'},
                                  { name:'Sampling Window', field: 'file_type.sampling_window',enableColumnMenu: false, headerTooltip: 'time window of the acquisitions'},
		                  { name:'Sampling Frequency', field: 'file_type.sampling_frequency',enableColumnMenu: false, headerTooltip: 'frequency of the acquisitions'},
				  { name:'MD5 Check Sum', field: 'md5_checksum',enableColumnMenu: false, headerTooltip: 'MD5 for the compressed file'},
		                  { name:'File Size', field: 'file_size',enableColumnMenu: false},
                                  { name:'Data Center Acronym', field: 'data_center.acronym',enableColumnMenu: false, headerTooltip: 'Data Center'},
				  { name:'Status', field: 'status',enableColumnMenu: false, headerTooltip: 'Current status of the file\n -3 : metadata incompliant with T1 metadata\n-2: QC running not successful\n-1 QC currently running\n0 File not checked\n1 File checked – OK according to the T1/T2/T3 metadata\n2 File checked – small inconsistency in RINEX header\n3 File checked – warning flag based on hardwired QC metrics'},
                                  { name:'Data center Protocol', field: 'data_center.protocol',enableColumnMenu: false, visible: false},
                                  { name:'Data center Hostname', field: 'data_center.hostname',enableColumnMenu: false, visible: false},
                                  { name:'Directory Naming', field: 'data_center.data_center_structure.directory_naming',enableColumnMenu: false, visible: false},
				  { name:'Relative Path', field: 'relative_path',enableColumnMenu: false, visible: false},     
				],
			data : []
        	};
	};
        
        
        $scope.gridHandler();
      
        /*************************************************************************
	* downloadrinexfile function handles on click links to download rinex file
	*************************************************************************/
	$scope.downloadrinexfile = function (filename,hostname,directory) {
		for(var i=0;i<$scope.results.length;i++)
                {
			if ($scope.results[i].name === filename && $scope.results[i].data_center.hostname === hostname && $scope.results[i].data_center.data_center_structure.directory_naming === directory)
			{
				if ($scope.results[i]['data_center']['data_center_structure']['directory_naming']!="")
				{
					var link = $scope.results[i]['data_center']['protocol'] + "://" + $scope.results[i]['data_center']['hostname'] + "/" + $scope.results[i]['data_center']['data_center_structure']['directory_naming'] + "/" + $scope.results[i]['relative_path'] + "/" + $scope.results[i]['name'];
				}else{
					var link = $scope.results[i]['data_center']['protocol'] + "://" + $scope.results[i]['data_center']['hostname'] + "/" + $scope.results[i]['relative_path'] + "/" + $scope.results[i]['name'];
				}
                                window.location = link; // start the download
			}
		}		
	};
	
	
	/*************************************************************************************
	* downloadselectedfile function allow to download rinex file selected in the grid
	*************************************************************************************/
	$scope.downloadselectedfile = function () {
		var selectedRows = $scope.gridApi.selection.getSelectedRows();
		if(selectedRows.length > 0){
			var confirmUser = $window.confirm('You need to configure your browser to avoid the download popup to appear for each file and allow popup for this site. If you do not want, you could download files by Wget Unix Script or File list. Do you want to continue ?');
		        if(confirmUser){
				var url = '';
				for(var i=0;i<selectedRows.length;i++){
					if (selectedRows[i]['data_center']['data_center_structure']['directory_naming']!="")
					{
						url = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['data_center']['data_center_structure']['directory_naming'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
					}else{
						url = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
					}
					window.open(url);
				}
		        }
		}else{
			$scope.openmessage($scope.messageinfogrid);
		}			
	};
	

	
	/****************************************************************
	* download function to handle downloading files in different format  
	****************************************************************/
	$scope.download = function(filename,format) {
		var selectedRows = $scope.gridApi.selection.getSelectedRows();
		if(selectedRows.length > 0)
		{
			if(format === 'Json')
			{
                                var jsonarray = [];
				for(var i=0;i<selectedRows.length;i++){
					for(var j=0;j<$scope.results.length;j++)
					{
						if(selectedRows[i].name==$scope.results[j].name)
						{
							jsonarray.push($scope.results[j]);
						}
					}
				}
			
				var blob = new Blob([JSON.stringify(jsonarray)], {type: 'text/json'});	
				if(window.navigator.msSaveOrOpenBlob) {//IE
					window.navigator.msSaveBlob(blob, filename);
				}else{
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;        
					document.body.appendChild(elem);
					elem.click();        
					document.body.removeChild(elem);
				}
			}else if(format === 'sh')
			{
				var script_text = "";
                                
				for(var i=0;i<selectedRows.length;i++){
					if (selectedRows[i]['data_center']['data_center_structure']['directory_naming']!="")
					{
						var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['data_center']['data_center_structure']['directory_naming'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
					}else{
						var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
					}
					script_text = script_text + "wget " + link + "\n";
                                }
				
				var blob = new Blob([script_text], {type: 'text/plain'});	
				if(window.navigator.msSaveOrOpenBlob) {//IE
					window.navigator.msSaveBlob(blob, filename);
				}else{
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;        
					document.body.appendChild(elem);
					elem.click();        
					document.body.removeChild(elem);
				}
			}else if(format === 'txt')
			{
				var script_text = "";
                               
				for(var i=0;i<selectedRows.length;i++){	
					if (selectedRows[i]['data_center']['data_center_structure']['directory_naming']!="")
					{
						var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['data_center']['data_center_structure']['directory_naming'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
					}else{
						var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
					}
					script_text = script_text + link + "\r\n";
				}
				
				var blob = new Blob([script_text], {type: 'text/plain'});	
				if(window.navigator.msSaveOrOpenBlob) {//IE
					window.navigator.msSaveBlob(blob, filename);
				}else{
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;        
					document.body.appendChild(elem);
					elem.click();        
					document.body.removeChild(elem);
				}
			}else if((format==='CSV')||(format==='GeodesyML')||(format==='SINEX')||(format==='GAMIT')){
				if(format==='CSV'){
					var adress = CONFIG.server  + CONFIG.glassapi + '/webresources/files/combination/';
				}
                                var req = $resource(adress +':myquery'+'/csv' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: false}});
				//execute asynchronous query
				req.query({myquery:$routeParams.querystring}).$promise.then(function(data) 
				{
					if (data!== '')
					{
						window.location = adress+$routeParams.querystring;
					}else{
						$scope.openmessage($scope.messageinfonodata);
					}
				});
			}
               }else{
			$scope.openmessage($scope.messageinfogrid);
	       }	
	};
	
	/***************************************************************************
	* exportquery function allows to users to get the query for command request
	***************************************************************************/
	$scope.exportquery = function(format) {
		var pyglass_request = "pyglass files -u " + CONFIG.server;
                if($routeParams.querystring.indexOf("site=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("site:");
			var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 5, endpos);
			pyglass_request = pyglass_request + " -sn " + param.replace(/,/g, " ");	
		}
                if($routeParams.querystring.indexOf("marker=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("marker=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -m " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("minLat=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("minLat=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsminlat " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("maxLat=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("maxLat=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsmaxlat " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("minLon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("minLon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsminlon " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("maxLon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("maxLon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rsmaxlon " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("centerLat=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("centerLat=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -rcclat " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("centerLon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("centerLon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -rcclon " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("radius=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("radius=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -rcr " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("polygon=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("polygon=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -ps " + param.replace(/[!,]/g, " ");
		}
                if($routeParams.querystring.indexOf("altitude=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("altitude=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			var posseparatedparam = param.indexOf(",");
                        if(posseparatedparam !== -1)
			{
				if(param.indexOf(",") !== 0)
				{
					pyglass_request = pyglass_request + " -htmin " + param.substring(0, posseparatedparam) + " -htmax " + param.substring(posseparatedparam + 1, param.length);
				}else{
					pyglass_request = pyglass_request + " -htmax " + param.substring(1, param.length);
				}
			}else{
				pyglass_request = pyglass_request + " -htmin " + param;
			}
		}
                if($routeParams.querystring.indexOf("longitude=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("longitude=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			var posseparatedparam = param.indexOf(",");
                        if(posseparatedparam !== -1)
			{
				if(param.indexOf(",") !== 0)
				{
					pyglass_request = pyglass_request + " -lgmin " + param.substring(0, posseparatedparam) + " -lgmax " + param.substring(posseparatedparam + 1, param.length);
				}else{
					pyglass_request = pyglass_request + " -lgmax " + param.substring(1, param.length);
				}
			}else{
				pyglass_request = pyglass_request + " -lgmin " + param;
			}
		}
                if($routeParams.querystring.indexOf("latitude=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("latitude=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			var posseparatedparam = param.indexOf(",");
                        if(posseparatedparam !== -1)
			{
				if(param.indexOf(",") !== 0)
				{
					pyglass_request = pyglass_request + " -ltmin " + param.substring(0, posseparatedparam) + " -ltmax " + param.substring(posseparatedparam + 1, param.length);
				}else{
					pyglass_request = pyglass_request + " -ltmax " + param.substring(1, param.length);
				}
			}else{
				pyglass_request = pyglass_request + " -ltmin " + param;
			}
		}
		if($routeParams.querystring.indexOf("installedDateMin=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("installedDateMin=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -idmin " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("installedDateMax=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("installedDateMax=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -idmax " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("removedDateMin=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("removedDateMin=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -rdmin " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("removedDateMax=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("removedDateMax=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -rdmax " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("network=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("network=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -nw " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("invertedNetworks=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("invertedNetworks=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -inw " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("agency=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("agency=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -ag " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("receiver=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("receiver=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			pyglass_request = pyglass_request + " -rt " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("antenna=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("antenna=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -at " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("radome=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("radome=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -radt " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("country=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("country=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -co " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("state=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("state=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 6, endpos);
			pyglass_request = pyglass_request + " -pv " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("city=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("city=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 5, endpos);
			pyglass_request = pyglass_request + " -ct " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("satelliteSystem=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("satelliteSystem=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 16, endpos);
			pyglass_request = pyglass_request + " -ss " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("dateRange=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("dateRange=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -dr " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("dataAvailability=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("dataAvailability=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 17, endpos);
			pyglass_request = pyglass_request + " -da " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("fileType=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("fileType=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 9, endpos);
			pyglass_request = pyglass_request + " -ft " + param.replace(/,/g, " ");
		}
                if($routeParams.querystring.indexOf("samplingFrequency=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("samplingFrequency=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 18, endpos);
			pyglass_request = pyglass_request + " -sf " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("samplingWindow=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("samplingWindow=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -sw " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("minimumObservationYears=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("minimumObservationYears=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 24, endpos);
			pyglass_request = pyglass_request + " -mo " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("statusfile=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("statusfile=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 11, endpos);
			pyglass_request = pyglass_request + " -stf " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("observationtype=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("observationtype=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 16, endpos);
			pyglass_request = pyglass_request + " -obst " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("frequency=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("frequency=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -freq " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("channel=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("channel=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 8, endpos);
			pyglass_request = pyglass_request + " -chan " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("constellation=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("constellation=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 14, endpos);
			pyglass_request = pyglass_request + " -constel " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("ratioepoch=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("ratioepoch=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 11, endpos);
			pyglass_request = pyglass_request + " -epoch " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("elevangle=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("elevangle=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 10, endpos);
			pyglass_request = pyglass_request + " -elang " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("multipathvalue=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("multipathvalue=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 15, endpos);
			pyglass_request = pyglass_request + " -multpath " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("nbcycleslips=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("nbcycleslips=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 13, endpos);
			pyglass_request = pyglass_request + " -cycslps " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("spprms=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("spprms=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 7, endpos);
			pyglass_request = pyglass_request + " -ssprms " + param.replace(/,/g, " ");
		}
		if($routeParams.querystring.indexOf("nbclockjumps=") !== -1)
		{
			var startpos = $routeParams.querystring.indexOf("nbclockjumps=");
                        var endpos = $routeParams.querystring.length;
			if($routeParams.querystring.indexOf("&", startpos) !== -1)
			{
				endpos = $routeParams.querystring.indexOf("&", startpos);
			}
                        var param = $routeParams.querystring.substring(startpos + 13, endpos);
			pyglass_request = pyglass_request + " -clkjmps " + param.replace(/,/g, " ");
		}         
		if(format==='Json'){
			$scope.currentrequest = $scope.request_json;
                        $scope.pyglassrequest = pyglass_request + " -o full-json";
		}else if(format==='Json + files'){
			$scope.currentrequest = $scope.request_json_files;
			$scope.pyglassrequest = pyglass_request + " -o full-json+files";
		}else if(format==='GeodesyML'){
			$scope.currentrequest = $scope.request_geodesyml;
			$scope.pyglassrequest = pyglass_request + " -o full-geodesyml";
		}else if(format==='GeodesyML + files'){
			$scope.currentrequest = $scope.request_geodesyml_files;
			$scope.pyglassrequest = pyglass_request + " -o full-geodesyml+files";
		}else if(format==='File list'){
			$scope.currentrequest = $scope.request_files_list;
			$scope.pyglassrequest = pyglass_request + " -o full-filelist";
		}else if(format==='SINEX'){
			$scope.currentrequest = $scope.request_sinex;
			$scope.pyglassrequest = pyglass_request + " -o full-sinex";
		}else if(format==='GAMIT'){
			$scope.currentrequest = $scope.request_gamit;
			$scope.pyglassrequest = pyglass_request + " -o full-gamit";
		}else if(format==='CSV'){
			$scope.currentrequest = $scope.request_csv;
			$scope.pyglassrequest = pyglass_request + " -o full-csv";
		}else if(format==='KML'){
			$scope.currentrequest = $scope.request_kml;
			$scope.pyglassrequest = pyglass_request + " -o full-kml";
		}
        	$scope.opendialog();
	};

	/******************************************************************************************
	* opendialog function allows to open a modal when the users click on "request query" button 
	*******************************************************************************************/
	$scope.opendialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalRequest.html',
		size: 'lg',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/***************************************
	* cancel function allows to cancel modal
	****************************************/
	$scope.cancel = function () {
    		$scope.modalInstance.dismiss("cancel");
  	};

	/*******************************
	* messageinfogrid alert message
	********************************/
	$scope.messageinfogrid = {	
    		textAlert : "You have to select the files in the grid before to download",
    		mode : 'info'
	  };
	/*******************************************************
	* messageinfonodata variable to advanced error message
	********************************************************/
	$scope.messageinfonodata = {
	    textAlert : "No data available",
	    mode : 'info'
	  };
	
  	/*******************************************************************
	* openmessage function allows to open a modal when there's an error
	*******************************************************************/
	$scope.openmessage = function(message) {
		return $scope.modalInstance = $uibModal.open({
	      	      templateUrl: 'views/messageBox.html',
                      scope: $scope,
		      size: 'lg',
		      resolve: {
			data: function () {
				$scope.messageinfo = message;
			  	return $scope.messageinfo;
			}
		      }
		 });
	};
	/**************************************
	* close function allows to close modal
	***************************************/
	$scope.close = function(){
	    $scope.modalInstance.close();
	};
	
}]);

'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc service
 * @name glasswebuiApp.serviceHistory
 * @description
 * # serviceHistory
 * Factory in the glasswebuiApp.
 */
angular.module('glasswebuiApp')
  .factory('serviceHistory', ["$cookies", function serviceHistory($cookies) {
    return{
	addhistory : function(data) {
            var cookies = $cookies.getAll();
            var exist = false;
            angular.forEach(cookies, function (v, k) {
                if (v===data){
			exist = true;
		}
	    });
            
	    if(exist===false){
		$cookies.put(new Date(), data);
	    }
	},
	clearhistory : function() {
	    var cookies = $cookies.getAll();
	    angular.forEach(cookies, function (v, k) {
                    $cookies.remove(k);	    
	    });
	}	
    };
    
  }]);

'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:NetworkCtrl
 * @description
 * # NetworkCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
  .controller('NetworkCtrl', ["$scope", "serviceHistory", "serviceResource", "$uibModal", "$window", "$timeout", "$resource", "$routeParams", "$compile", "$templateCache", "CONFIG", function ($scope, serviceHistory, serviceResource, $uibModal,$window,$timeout,$resource,$routeParams,$compile,$templateCache,CONFIG) {
	$scope.results = '';
	$scope.markers = new L.FeatureGroup(); // Layer group for the map
     

        /*********************************************
	* addhistory function allows to add in history
	*********************************************/
	$scope.addhistory = function() {
	    serviceHistory.addhistory(CONFIG.server + CONFIG.clientname + "/#/network/" + $routeParams.querystring);
	};	
		  
	/**************************************************************************************
	* currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
	**************************************************************************************/
	$scope.currentDateFormat = function() {
	    var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1;
	    var HH = today.getHours();
	    var MM = today.getMinutes();
	    var SS = today.getSeconds();
	    var yyyy = today.getFullYear();
	    if(dd<10){
		dd='0'+dd
	    } 
	    if(mm<10){
		mm='0'+mm
	    }
	    if (HH < 10) {
		HH = "0" + HH;
	    }
	    if (MM < 10) {
		MM = "0" + MM;
	    }
	    if (SS < 10) {
		SS = "0" + SS;
	    }   
	    return today = yyyy+'-'+mm+'-'+dd+' '+HH+':'+MM+':'+SS;
	};
       
	/*************************************************************
	* loadingdialog function allows to open a modal with a spinner 
	**************************************************************/
	$scope.loadingdialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalLoading.html',
		size: 'm',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/*****************************************************
	* requestNetwork function allows to request network
	******************************************************/
	$scope.requestNetwork = function () {
		var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/json?network=';
		var req = $resource(adress +':myquery' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: true}}); // how to use: see angular doc about the service $resource
		$scope.request_json = adress + $routeParams.querystring;//$routeParams.querystring is the parameter defined in app.js for routing
		/*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/network:" + $routeParams.querystring;
		$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/network:" + $routeParams.querystring;
		$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/network:" + $routeParams.querystring;
		$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/network:" + $routeParams.querystring;
		$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/network:" + $routeParams.querystring;
		$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/network:" + $routeParams.querystring;
		$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/network:" + $routeParams.querystring;
		$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/network:" + $routeParams.querystring;*/
		$scope.loading = true;//variable checked for loading element in network.html
                $scope.loadingdialog();
                $scope.param_network_name = $routeParams.querystring;
		req.query({myquery:$routeParams.querystring}).$promise.then(function(data) 
		{
			if (data.length>0)
			{	
				$scope.nodata = false;
				$scope.modalInstance.opened.then(function () {
					$scope.loading = false;
                                	$scope.modalInstance.close();//stop the spinner loading
				});	
				$scope.results = data;
				$scope.currentdate = $scope.currentDateFormat();
				for(var i=0;i<$scope.results.length;i++){
					if ($scope.results[i].date_to === null)
					{
						$scope.results[i]['date_to'] = $scope.currentdate;
						//$scope.status = "Active";
					}/*else{
						//$scope.status = "Deactivated";
					}*/
				}
				
                                $timeout(function(){
					$scope.mymap = L.map('networkmapid');
					L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					$scope.switchmap();
					$scope.markersHandler($scope.results);
                                        
				},100);//set timeout 100ms to wait the DOM is loading
				
                                $scope.gridOptions.data = $scope.results;//Update the grid data with the result from the request
                                        
			}else{
				$scope.modalInstance.close();//stop the spinner loading
				$scope.openmessage($scope.messageinfonodata);
			}
		});
		
	};

        /***************************************
	* gridHandler: function handling the grid
	***************************************/
	$scope.gridHandler = function() {
		//custom checkbox in the grid  
		$templateCache.put('ui-grid/selectionRowHeaderButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-row-selected': row.isSelected}\" ><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"row.isSelected\" ng-click=\"row.isSelected=!row.isSelected;selectButtonClick(row, $event)\">&nbsp;</div>"
  );

  		$templateCache.put('ui-grid/selectionSelectAllButtons',
    "<div class=\"ui-grid-selection-row-header-buttons \" ng-class=\"{'ui-grid-all-selected': grid.selection.selectAll}\" ng-if=\"grid.options.enableSelectAll\"><input style=\"margin: 0; vertical-align: middle\" type=\"checkbox\" ng-model=\"grid.selection.selectAll\" ng-click=\"grid.selection.selectAll=!grid.selection.selectAll;headerButtonClick($event)\"></div>"
  ); 
		//define all grid options and their columns
		$scope.gridOptions = {
                        enableGridMenu: false,
		    	enableFiltering: false,
			enableSorting: true,
			enableRowSelection: true,
			enableSelectAll: true,
			multiSelect: true,
                        exporterMenuCsv: false,
			exporterMenuPdf: false,
                        enableColumnMoving: true,
                        enableColumnResizing: true, 
                        showGridFooter: true,
			//all function regarding the ui-grid
                        onRegisterApi: function(gridApi){ 
      				$scope.gridApi = gridApi;
				
				//function allows to get the row selected in the grid and display the corresponding marker into the map with a  bigger circle
				$scope.gridApi.selection.on.rowSelectionChanged($scope, function(row){ 
					if(row.entity.date_to===$scope.currentdate) // if not current date (station desactivated), the station marker is a png and not changeable.
                                        {
						$scope.mymap.eachLayer(function(layer) {
						if(layer.hasOwnProperty("_latlng")){
							if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
							{
							
								var defaultoption = {
									    		radius: 7,
                   									fillColor: "#FF0000",
											color: "#000",
											weight: 1,
											opacity: 1,
											fillOpacity: 0.8
									};
								var mylayerid = layer._leaflet_id;
								if (row.isSelected){ 
									var highlight = {
									    		radius: 12,
											fillColor: "#2CDBF6",
											color: "#000",
											weight: 1,
											opacity: 1,
											fillOpacity: 0.8
									};
									$scope.mymap._layers[mylayerid].setStyle(highlight);
									$scope.mymap._layers[mylayerid].bringToFront();
								}else{
									$scope.mymap._layers[mylayerid].setStyle(defaultoption);
									
								}
							
							}
						}	
						});
					}
					
				});
				//function allows to get several row selected in the grid and display them into the map with a circle bigger
				//(global checkbox or shift+click)
				 $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
					angular.forEach(rows, function(row, key) {
					 	if(row.entity.date_to===$scope.currentdate)
		                                {
							$scope.mymap.eachLayer(function(layer) {
							if(layer.hasOwnProperty("_latlng")){
								if((row.entity.location.coordinates.lat===layer._latlng.lat)&&(row.entity.location.coordinates.lon===layer._latlng.lng))
								{
							
									var defaultoption = {
										    		radius: 7,
												fillColor: "#FF0000",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
										};
									var mylayerid = layer._leaflet_id;
									if (row.isSelected){ 
										var highlight = {
										    		radius: 12,
												fillColor: "#2CDBF6",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
										};
										$scope.mymap._layers[mylayerid].setStyle(highlight);
										$scope.mymap._layers[mylayerid].bringToFront();
									}else{
										$scope.mymap._layers[mylayerid].setStyle(defaultoption);
										
									}
							
								}
							}	
							});
						}
					});	
				});

    			},
			//definition of each column in the grid
			columnDefs: [
				  { name:'Marker', field: 'marker', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.metadataDetail(row.entity.marker)">{{ COL_FIELD }}</a></div>'},
				  { name:'Site name', field: 'name',enableColumnMenu: false},
                                  { name:'Install Date', field: 'date_from',enableColumnMenu: false},
		                  { name:'End Date', field: 'date_to',enableColumnMenu: false,cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) 
										{
											if (grid.getCellValue(row ,col) == $scope.currentdate) 
											 {
												return 'white'; //for ui-grid the class '.white' is transparent and not only white
											 }
			        						}},
				  { name:'Lat', field: 'location.coordinates.lat', type: 'number', cellFilter: 'number: 3',enableColumnMenu: false},
		                  { name:'Lon', field: 'location.coordinates.lon', type: 'number', cellFilter: 'number: 3',enableColumnMenu: false},
		                  { name:'Elev', field: 'location.coordinates.altitude', type: 'number', cellFilter: 'number: 3',enableColumnMenu: false},
				  { name:'Country', field: 'location.city.state.country.name' ,enableColumnMenu: false}       
				],
			data : []
        	};
	};
        



	// call the functions        
        $scope.gridHandler();
        $scope.requestNetwork();
	


         
	/********************************************************************
	* markersHandler function handles the display of markers in the map
	********************************************************************/
        $scope.markersHandler = function(data) {
		var marker = '';
		var networkStr = '';
                var date_end = '';
		angular.forEach(data, function(result) {
			for(var j=0;j<result.station_networks.length;j++)
			{
			  networkStr = networkStr + " " + result.station_networks[j].network.name;  
			}
			if (networkStr.substr(0,1)===" ")
			{	
				networkStr = networkStr.substr(1);
			}
			
                   	if (result.date_to === $scope.currentdate){
	   			var optionMarker = {
						radius: 7,
						fillColor: "#FF0000",
						color: "#000",
						weight: 1,
						opacity: 1,
						fillOpacity: 0.8
                                                
					   };
                       		marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon],optionMarker);
                                date_end = "";
                   	}else{
				var LeafIcon = L.Icon.extend({
				    options: {
                                        iconSize:     [15, 15],
					iconAnchor:   [15, 15]
				    }
				});
				
				var mypathicon  = "images/black.png";
                                marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon],{icon: new LeafIcon({iconUrl: mypathicon})});
                                date_end = result.date_to;
		   	}
		    	networkStr = '';
           	    	
                        var container = $('<div />');//create a container for popup
                        marker.result = result;//allow to store in the marker all the informations
			
			// when there is two stations with the exactely the same coordinates
			var markerdouble = '';
			var datefromdouble = '';
			var datetodouble = '';
			var altitudedouble = '';
			
			for (var i = 0; i < data.length; i++) {
			  if ((data[i].location.coordinates.lat === result.location.coordinates.lat)&&(data[i].location.coordinates.lon === result.location.coordinates.lon))
				// Strict comparison: if there is a even a very slight difference in coordinates, it should be visible in the map
				{
					if (data[i].marker!=result.marker)
					{
						markerdouble = data[i].marker;
						datefromdouble = data[i].date_from;
						if (data[i].date_to === $scope.currentdate){
							datetodouble = "";
						}else{
							datetodouble = data[i].date_to;
						}
						altitudedouble = data[i].location.coordinates.altitude;
					}
				}
			}
			if (markerdouble!='') {//case two stations at a same coordinates
				
				container.on('click', '.popupLink', function() {
		                        $scope.metadataDetail(result.marker);
				});
				container.on('click', '.popupLink2', function() {
		                        $scope.metadataDetail(markerdouble);
				});

				container.html('<div id="popupcontainer"><div><span><b>Marker:</b> ' + result.marker +  
		                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from + 
		                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end + 
		                                 '</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name + 	                         
                                                 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result) +  
		                                 '</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a></div>' + 
						 '<div><span><b>Marker:</b> ' + markerdouble + 
		                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Alt:</b> ' + altitudedouble.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + datefromdouble + 
		                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + datetodouble +
		                                 '</span><br style="line-height:0px;"><span><b>Country:</b> '  + result.location.city.state.country.name +		                                 
						 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result) +
		                                 '</span><br style="line-height:0px;"><a class="popupLink2">Metadata details</a></div></div>'); 

			   	
			}else{
				
				container.on('click', '.popupLink', function() {
		                        $scope.metadataDetail(result.marker);
				});

				container.html('<span><b>Marker:</b> ' + result.marker + 
		                                 '</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) + 
		                                 '</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from + 
		                                 '</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end + 
		                                 '</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name + 		                                 
						 '</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result) +  
		                                 '</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a>');
				
			}
			
			marker.bindPopup(container[0]);//bind the marker to the popup
			
		    	$scope.markers.addLayer(marker);
		});	
		$scope.mymap.addLayer($scope.markers);
		$scope.mymap.fitBounds($scope.markers.getBounds(),{maxZoom:3});		
	};

	/************************************************************
	* getNetworkForPopup function handles the display of networks
	* in tooltip when the users click on marker in the map
	************************************************************/
	$scope.getNetworkForPopup = function(result) {	
		var networkStr = ''; 
	    		
		for(var j=0;j<result.station_networks.length;j++)
		{
		  networkStr = networkStr + " " + result.station_networks[j].network.name;
		}
		if (networkStr.substr(0,1)===" ")
                {
			networkStr = networkStr.substr(1);
		}
		return networkStr;		
			  
	};

	
	/****************************************************************
	* download function handles downloaded files in different format  
	****************************************************************/
	$scope.download = function(filename,format) {
			if(format === 'Json')
			{
				var blob = new Blob([JSON.stringify($scope.results)], {type: 'text/json'});	
				if(window.navigator.msSaveOrOpenBlob) {//IE
					window.navigator.msSaveBlob(blob, filename);
				}else{
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;        
					document.body.appendChild(elem);
					elem.click();        
					document.body.removeChild(elem);
				}
			}else if((format==='CSV')||(format==='GeodesyML')||(format==='SINEX')||(format==='GAMIT')){
				if(format==='CSV'){
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/csv?network=';
				}
                                var req = $resource(adress +':myquery' ,{myquery:'@myquery'}, {query: {method: 'get', isArray: false}});
				//execute asynchronous query
				req.query({myquery:$routeParams.querystring}).$promise.then(function(data) 
				{
					if (data!== '')
					{
						window.location = adress+$routeParams.querystring;
					}else{
						$scope.openmessage($scope.messageinfonodata);
					}
				});
			}
	};

	/***************************************************************************
	* exportquery function allows to users to get the query for command request
	***************************************************************************/
	$scope.exportquery = function(format) {
		var pyglass_request = "pyglass stations -u " + CONFIG.server;
	        pyglass_request = pyglass_request + " -nw " + $routeParams.querystring;
                         
		if(format==='Json'){
			$scope.currentrequest = $scope.request_json;
                        $scope.pyglassrequest = pyglass_request + " -o full-json";
		}else if(format==='Json + files'){
			$scope.currentrequest = $scope.request_json_files;
			$scope.pyglassrequest = pyglass_request + " -o full-json+files";
		}else if(format==='GeodesyML'){
			$scope.currentrequest = $scope.request_geodesyml;
			$scope.pyglassrequest = pyglass_request + " -o full-geodesyml";
		}else if(format==='GeodesyML + files'){
			$scope.currentrequest = $scope.request_geodesyml_files;
			$scope.pyglassrequest = pyglass_request + " -o full-geodesyml+files";
		}else if(format==='File list'){
			$scope.currentrequest = $scope.request_files_list;
			$scope.pyglassrequest = pyglass_request + " -o full-filelist";
		}else if(format==='SINEX'){
			$scope.currentrequest = $scope.request_sinex;
			$scope.pyglassrequest = pyglass_request + " -o full-sinex";
		}else if(format==='GAMIT'){
			$scope.currentrequest = $scope.request_gamit;
			$scope.pyglassrequest = pyglass_request + " -o full-gamit";
		}else if(format==='CSV'){
			$scope.currentrequest = $scope.request_csv;
			$scope.pyglassrequest = pyglass_request + " -o full-csv";
		}else if(format==='KML'){
			$scope.currentrequest = $scope.request_kml;
			$scope.pyglassrequest = pyglass_request + " -o full-kml";
		}
        	$scope.opendialog();
	};

	/******************************************************************************************
	* opendialog function allows to open a modal when the users click on "request query" button 
	*******************************************************************************************/
	$scope.opendialog = function() {
	    return $scope.modalInstance = $uibModal.open({
		templateUrl: 'views/modalRequest.html',
		size: 'lg',
		scope: $scope,
                windowClass: 'center-modal'
	    });	
	};

	/***************************************
	* cancel function allows to cancel modal
	****************************************/
	$scope.cancel = function () {
    		$scope.modalInstance.dismiss("cancel");
  	};


	/*********************************************************************
	* metadataDetail function handles on click links to access metadata
	**********************************************************************/
	$scope.metadataDetail = function (code) {
		var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/json?';
		var param = "marker=";
		var request_json = adress + param + code;
                /*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/" + $routeParams.querystring;
		$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/" + $routeParams.querystring;
		$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/" + $routeParams.querystring;
		$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/" + $routeParams.querystring;
		$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/" + $routeParams.querystring;
		$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/" + $routeParams.querystring;
		$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/" + $routeParams.querystring;
		$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/" + $routeParams.querystring;*/
		var mypass = $window.open('#/metadata/' + param + code,'_blank');	
	};

	
	/*******************************************************
	* messageinfonodata variable to advanced error message
	********************************************************/
	$scope.messageinfonodata = {
	    textAlert : "No data available",
	    mode : 'info'
	  };
  	/*******************************************************************
	* openmessage function allows to open a modal when there's an error
	*******************************************************************/
	$scope.openmessage = function(message) {
		return $scope.modalInstance = $uibModal.open({
	      	      templateUrl: 'views/messageBox.html',
                      scope: $scope,
		      size: 'lg',
		      resolve: {
			data: function () {
				$scope.messageinfo = message;
			  	return $scope.messageinfo;
			}
		      }
		 });
	};
	/**************************************
	* close function allows to close modal
	***************************************/
	$scope.close = function(){
	    $scope.modalInstance.close();
	};

	/***************************************************
	* selectText function allows to select text in modal
	***************************************************/
	$scope.selectText = function(containerid){
		if (document.selection) {
		    var range = document.body.createTextRange();
		    range.moveToElementText(document.getElementById(containerid));
		    range.select();
                    document.execCommand("Copy"); 
		} else if (window.getSelection) {//IE
		    var range = document.createRange();
		    range.selectNode(document.getElementById(containerid));
                    window.getSelection().removeAllRanges();
		    window.getSelection().addRange(range);
                    document.execCommand("Copy"); 
		}
		
    	};

	/*******************************************************************
	* switchmap function allow to switch between satellite or simple map
	*******************************************************************/
	$scope.switchmap = function () {
		var MyControl = L.Control.extend({
		    options: {
			position: 'bottomleft'
		    },
		    onAdd: function (map) {
			var container = L.DomUtil.create('div', 'switchmap-control');
			container.innerHTML += '<button id="mapbuttonid" style="background-image:url(images/satellitemap.png);width:70px;height:70px;"></button>';
			$scope.imagemap = "satellitemap";       	
			$compile( container )($scope);
			return container;
		    }
		});
		$scope.mymap.addControl(new MyControl());
		$('.switchmap-control').click(function(event) {
			if($scope.imagemap == "satellitemap")
			{
				L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
				document.getElementById("mapbuttonid").style.backgroundImage= "url(images/simplemap.png)";
				$scope.imagemap = "simplemap";
			}else{
				L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
				document.getElementById("mapbuttonid").style.backgroundImage= "url(images/satellitemap.png)";
				$scope.imagemap = "satellitemap";
			}
		});
	};
	
}]);
