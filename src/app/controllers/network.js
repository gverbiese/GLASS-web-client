'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:NetworkCtrl
 * @description
 * # NetworkCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
	.controller('NetworkCtrl', function ($scope, serviceHistory, serviceResource, $uibModal, $window, $timeout, $resource, $routeParams, $compile, $templateCache, CONFIG, tourConfig) {
		/***** AngularJS Tour *****/
		tourConfig.animation = false;
		
		
		$scope.results = '';
		$scope.markers = new L.FeatureGroup(); // Layer group for the map


        /*********************************************
	* addhistory function allows to add in history
	*********************************************/
		$scope.addhistory = function () {
			serviceHistory.addhistory(CONFIG.server + CONFIG.clientname + "/#/network/" + $routeParams.querystring);
		};

		/**************************************************************************************
		* currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
		**************************************************************************************/
		$scope.currentDateFormat = function () {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var HH = today.getHours();
			var MM = today.getMinutes();
			var SS = today.getSeconds();
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}
			if (HH < 10) {
				HH = "0" + HH;
			}
			if (MM < 10) {
				MM = "0" + MM;
			}
			if (SS < 10) {
				SS = "0" + SS;
			}
			return today = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM + ':' + SS;
		};

		/*************************************************************
		* loadingdialog function allows to open a modal with a spinner 
		**************************************************************/
		$scope.loadingdialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalLoading.html',
				size: 'm',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/*****************************************************
		* requestNetwork function allows to request network
		******************************************************/
		$scope.requestNetwork = function () {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/json?network=';
			var req = $resource(adress + ':myquery', { myquery: '@myquery' }, { query: { method: 'get', isArray: true } }); // how to use: see angular doc about the service $resource
			$scope.request_json = adress + $routeParams.querystring;//$routeParams.querystring is the parameter defined in app.js for routing
			/*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/network:" + $routeParams.querystring;
			$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/network:" + $routeParams.querystring;
			$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/network:" + $routeParams.querystring;
			$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/network:" + $routeParams.querystring;
			$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/network:" + $routeParams.querystring;
			$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/network:" + $routeParams.querystring;
			$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/network:" + $routeParams.querystring;
			$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/network:" + $routeParams.querystring;*/
			$scope.loading = true;//variable checked for loading element in network.html
			$scope.loadingdialog();
			$scope.param_network_name = $routeParams.querystring;
			req.query({ myquery: $routeParams.querystring }).$promise.then(function (data) {
				if (data.length > 0) {
					$scope.nodata = false;
					$scope.modalInstance.opened.then(function () {
						$scope.loading = false;
						$scope.modalInstance.close();//stop the spinner loading
					});
					$scope.results = data;
					$scope.currentdate = $scope.currentDateFormat();
					for (var i = 0; i < $scope.results.length; i++) {
						if ($scope.results[i].date_to === null) {
							$scope.results[i]['date_to'] = $scope.currentdate;
							//$scope.status = "Active";
						}/*else{
						//$scope.status = "Deactivated";
					}*/
					}

					$timeout(function () {
						$scope.mymap = L.map('networkmapid', {
							minZoom: 2,
							gestureHandling: true,
							zoomDelta: 0.5
						});
						L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
						$scope.switchmap();
						$scope.showZoomMessage();
						$scope.markersHandler($scope.results);
						$scope.mymap.scrollWheelZoom.disable();
						//Scroll  with CTRL+Mouse method Map
						$('#networkmapid').bind('mousewheel DOMMouseScroll', function (event) {
							event.stopPropagation();
							if (event.ctrlKey == true) {
								event.preventDefault();
								$scope.mymap.scrollWheelZoom.enable();
								setTimeout(function () {
									$scope.mymap.scrollWheelZoom.disable();
								}, 2000);
							} else {
								$scope.mymap.scrollWheelZoom.disable();
							}
						});

					}, 100);//set timeout 100ms to wait the DOM is loading

					$scope.gridOptions.data = $scope.results;//Update the grid data with the result from the request

				} else {
					$scope.modalInstance.close();//stop the spinner loading
					$scope.openmessage($scope.messageinfonodata);
				}
			});

		};

		/**
		 * Show a message on the map explaining how to zoom
		 */
		$scope.showZoomMessage = function () {
			L.Control.zoomText = L.Control.extend({
				onAdd: function (map) {
					var text = L.DomUtil.create('div');
					text.id = "zoomText";
					text.innerHTML = "<strong>Zoom : CTRL+SCROLL</strong>";
					return text;
				},
				onRemove: function (map) { }
			});
			L.control.zoomText = function (opts) { return new L.Control.zoomText(opts); }
			L.control.zoomText({ position: 'bottomright' }).addTo($scope.mymap);
		}
        /***************************************
		 * gridHandler: function handling the grid
		 ***************************************/
		$scope.gridHandler = function () {
			//define all grid options and their columns
			$scope.gridOptions = {
				enableGridMenu: false,
				enableFiltering: false,
				enableSorting: true,
				enableRowSelection: true,
				enableSelectAll: true,
				multiSelect: true,
				exporterMenuCsv: false,
				exporterMenuPdf: false,
				enableColumnMoving: true,
				enableColumnResizing: true,
				showGridFooter: true,
				//all function regarding the ui-grid
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;

					//function allows to get the row selected in the grid and display the corresponding marker into the map with a  bigger circle
					$scope.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
						if (row.entity.date_to === $scope.currentdate) // if not current date (station desactivated), the station marker is a png and not changeable.
						{
							$scope.mymap.eachLayer(function (layer) {
								if (layer.hasOwnProperty("_latlng")) {
									if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {

										var defaultoption = {
											radius: 7,
											fillColor: "#FF0000",
											color: "#000",
											weight: 1,
											opacity: 1,
											fillOpacity: 0.8
										};
										var mylayerid = layer._leaflet_id;
										if (row.isSelected) {
											var highlight = {
												radius: 12,
												fillColor: "#2CDBF6",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
											};
											$scope.mymap._layers[mylayerid].setStyle(highlight);
											$scope.mymap._layers[mylayerid].bringToFront();
										} else {
											$scope.mymap._layers[mylayerid].setStyle(defaultoption);

										}

									}
								}
							});
						}

					});
					//function allows to get several row selected in the grid and display them into the map with a circle bigger
					//(global checkbox or shift+click)
					$scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
						angular.forEach(rows, function (row, key) {
							if (row.entity.date_to === $scope.currentdate) {
								$scope.mymap.eachLayer(function (layer) {
									if (layer.hasOwnProperty("_latlng")) {
										if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {

											var defaultoption = {
												radius: 7,
												fillColor: "#FF0000",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
											};
											var mylayerid = layer._leaflet_id;
											if (row.isSelected) {
												var highlight = {
													radius: 12,
													fillColor: "#2CDBF6",
													color: "#000",
													weight: 1,
													opacity: 1,
													fillOpacity: 0.8
												};
												$scope.mymap._layers[mylayerid].setStyle(highlight);
												$scope.mymap._layers[mylayerid].bringToFront();
											} else {
												$scope.mymap._layers[mylayerid].setStyle(defaultoption);

											}

										}
									}
								});
							}
						});
					});

				},
				//definition of each column in the grid
				columnDefs: [
					{ name: 'Marker', field: 'marker_long_name', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.metadataDetail(row.entity.marker)">{{ COL_FIELD }}</a></div>' },
					{ name: 'Site name', field: 'name', enableColumnMenu: false },
					{ name: 'Install Date', field: 'date_from', enableColumnMenu: false },
					{
						name: 'End Date', field: 'date_to', enableColumnMenu: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							if (grid.getCellValue(row, col) == $scope.currentdate) {
								return 'white'; //for ui-grid the class '.white' is transparent and not only white
							}
						}
					},
					{ name: 'Lat', field: 'location.coordinates.lat', type: 'number', cellFilter: 'number: 3', enableColumnMenu: false },
					{ name: 'Lon', field: 'location.coordinates.lon', type: 'number', cellFilter: 'number: 3', enableColumnMenu: false },
					{ name: 'Elev', field: 'location.coordinates.altitude', type: 'number', cellFilter: 'number: 3', enableColumnMenu: false },
					{ name: 'Country', field: 'location.city.state.country.name', enableColumnMenu: false }
				],
				data: []
			};
		};




		// call the functions        
		$scope.gridHandler();
		$scope.requestNetwork();




		/********************************************************************
		* markersHandler function handles the display of markers in the map
		********************************************************************/
		$scope.markersHandler = function (data) {
			var marker = '';
			var networkStr = '';
			var date_end = '';
			angular.forEach(data, function (result) {
				var internationalStation = false;
				for (var j = 0; j < result.station_networks.length; j++) {
					networkStr = networkStr + " " + result.station_networks[j].network.name;
					if(["EPN","IGS","EPOS"].indexOf(result.station_networks[j].network.name) >= 0){
						internationalStation = true;
					}
				}
				if (networkStr.substr(0, 1) === " ") {
					networkStr = networkStr.substr(1);
				}

				if (result.date_to === $scope.currentdate) {
					var optionMarker = {
						radius: 7,
						fillColor: "#FF0000",
						color: "#000",
						weight: 1,
						opacity: 1,
						fillOpacity: 0.8
					};
					if(internationalStation === true){
						optionMarker = {
							radius: 7,
							fillColor: "#FF0000",
							color: "#000",
							weight: 3,
							color:"#FFFFFF",
							opacity: 1,
							fillOpacity: 0.8
						};
					}

					marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon], optionMarker);
					date_end = "";
				} else {
					var LeafIcon = L.Icon.extend({
						options: {
							iconSize: [15, 15],
							iconAnchor: [15, 15]
						}
					});

					var mypathicon = "images/black.png";
					marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon], { icon: new LeafIcon({ iconUrl: mypathicon }) });
					date_end = result.date_to;
				}
				networkStr = '';

				var container = $('<div />');//create a container for popup
				marker.result = result;//allow to store in the marker all the informations

				// when there is two stations with the exactely the same coordinates
				var markerdouble = '';
				var datefromdouble = '';
				var datetodouble = '';
				var altitudedouble = '';

				for (var i = 0; i < data.length; i++) {
					if ((data[i].location.coordinates.lat === result.location.coordinates.lat) && (data[i].location.coordinates.lon === result.location.coordinates.lon))
					// Strict comparison: if there is a even a very slight difference in coordinates, it should be visible in the map
					{
						if (data[i].marker != result.marker) {
							markerdouble = data[i].marker;
							datefromdouble = data[i].date_from;
							if (data[i].date_to === $scope.currentdate) {
								datetodouble = "";
							} else {
								datetodouble = data[i].date_to;
							}
							altitudedouble = data[i].location.coordinates.altitude;
						}
					}
				}
				if (markerdouble != '') {//case two stations at a same coordinates

					container.on('click', '.popupLink', function () {
						$scope.metadataDetail(result.marker);
					});
					container.on('click', '.popupLink2', function () {
						$scope.metadataDetail(markerdouble);
					});

					container.html('<div id="popupcontainer"><div><span><b>Marker:</b> ' + result.marker_long_name +
						'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from +
						'</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end +
						'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name +
						'</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result) +
						'</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a></div>' +
						'<div><span><b>Marker:</b> ' + markerdouble +
						'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Alt:</b> ' + altitudedouble.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + datefromdouble +
						'</span><br style="line-height:0px;"><span><b>End date:</b> ' + datetodouble +
						'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name +
						'</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result) +
						'</span><br style="line-height:0px;"><a class="popupLink2">Metadata details</a></div></div>');


				} else {

					container.on('click', '.popupLink', function () {
						$scope.metadataDetail(result.marker);
					});

					container.html('<span><b>Marker:</b> ' + result.marker_long_name +
						'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Alt:</b> ' + result.location.coordinates.altitude.toFixed(3) +
						'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from +
						'</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end +
						'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name +
						'</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result) +
						'</span><br style="line-height:0px;"><a class="popupLink">Metadata details</a>');

				}

				marker.bindPopup(container[0]);//bind the marker to the popup

				$scope.markers.addLayer(marker);
			});
			$scope.mymap.addLayer($scope.markers);
			$scope.mymap.fitBounds($scope.markers.getBounds(), { maxZoom: 3 });
		};

		/************************************************************
		* getNetworkForPopup function handles the display of networks
		* in tooltip when the users click on marker in the map
		************************************************************/
		$scope.getNetworkForPopup = function (result) {
			var networkStr = '';

			for (var j = 0; j < result.station_networks.length; j++) {
				networkStr = networkStr + " " + result.station_networks[j].network.name;
			}
			if (networkStr.substr(0, 1) === " ") {
				networkStr = networkStr.substr(1);
			}
			return networkStr;

		};


		/****************************************************************
		* download function handles downloaded files in different format  
		****************************************************************/
		$scope.download = function (filename, format) {
			if (format === 'Json') {
				var blob = new Blob([JSON.stringify($scope.results)], { type: 'text/json' });
				if (window.navigator.msSaveOrOpenBlob) {//IE
					window.navigator.msSaveBlob(blob, filename);
				} else {
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;
					document.body.appendChild(elem);
					elem.click();
					document.body.removeChild(elem);
				}
			} else if ((format === 'CSV') || (format === 'GeodesyML') || (format === 'SINEX') || (format === 'GAMIT')) {
				if (format === 'CSV') {
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/csv?network=';
				}
				var req = $resource(adress + ':myquery', { myquery: '@myquery' }, { query: { method: 'get', isArray: false } });
				//execute asynchronous query
				req.query({ myquery: $routeParams.querystring }).$promise.then(function (data) {
					if (data !== '') {
						window.location = adress + $routeParams.querystring;
					} else {
						$scope.openmessage($scope.messageinfonodata);
					}
				});
			}
		};

		/***************************************************************************
		* exportquery function allows to users to get the query for command request
		***************************************************************************/
		$scope.exportquery = function (format) {
			var pyglass_request = "pyglass stations -u " + CONFIG.server;
			pyglass_request = pyglass_request + " -nw " + $routeParams.querystring;

			if (format === 'Json') {
				$scope.currentrequest = $scope.request_json;
				$scope.pyglassrequest = pyglass_request + " -o full-json";
			} else if (format === 'Json + files') {
				$scope.currentrequest = $scope.request_json_files;
				$scope.pyglassrequest = pyglass_request + " -o full-json+files";
			} else if (format === 'GeodesyML') {
				$scope.currentrequest = $scope.request_geodesyml;
				$scope.pyglassrequest = pyglass_request + " -o full-geodesyml";
			} else if (format === 'GeodesyML + files') {
				$scope.currentrequest = $scope.request_geodesyml_files;
				$scope.pyglassrequest = pyglass_request + " -o full-geodesyml+files";
			} else if (format === 'File list') {
				$scope.currentrequest = $scope.request_files_list;
				$scope.pyglassrequest = pyglass_request + " -o full-filelist";
			} else if (format === 'SINEX') {
				$scope.currentrequest = $scope.request_sinex;
				$scope.pyglassrequest = pyglass_request + " -o full-sinex";
			} else if (format === 'GAMIT') {
				$scope.currentrequest = $scope.request_gamit;
				$scope.pyglassrequest = pyglass_request + " -o full-gamit";
			} else if (format === 'CSV') {
				$scope.currentrequest = $scope.request_csv;
				$scope.pyglassrequest = pyglass_request + " -o full-csv";
			} else if (format === 'KML') {
				$scope.currentrequest = $scope.request_kml;
				$scope.pyglassrequest = pyglass_request + " -o full-kml";
			}
			$scope.opendialog();
		};

		/******************************************************************************************
		* opendialog function allows to open a modal when the users click on "request query" button 
		*******************************************************************************************/
		$scope.opendialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalRequest.html',
				size: 'lg',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/***************************************
		* cancel function allows to cancel modal
		****************************************/
		$scope.cancel = function () {
			$scope.modalInstance.dismiss("cancel");
		};


		/*********************************************************************
		* metadataDetail function handles on click links to access metadata
		**********************************************************************/
		$scope.metadataDetail = function (code) {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/json?';
			var param = "marker=";
			var request_json = adress + param + code;
			/*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/" + $routeParams.querystring;
	$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/" + $routeParams.querystring;
	$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/" + $routeParams.querystring;
	$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/" + $routeParams.querystring;
	$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/" + $routeParams.querystring;
	$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/" + $routeParams.querystring;
	$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/" + $routeParams.querystring;
	$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/" + $routeParams.querystring;*/
			var mypass = $window.open('#/metadata/' + param + code, '_blank');
		};


		/*******************************************************
		* messageinfonodata variable to advanced error message
		********************************************************/
		$scope.messageinfonodata = {
			textAlert: "No data available",
			mode: 'info'
		};
  	/*******************************************************************
	* openmessage function allows to open a modal when there's an error
	*******************************************************************/
		$scope.openmessage = function (message) {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/messageBox.html',
				scope: $scope,
				size: 'lg',
				resolve: {
					data: function () {
						$scope.messageinfo = message;
						return $scope.messageinfo;
					}
				}
			});
		};
		/**************************************
		* close function allows to close modal
		***************************************/
		$scope.close = function () {
			$scope.modalInstance.close();
		};

		/***************************************************
		* selectText function allows to select text in modal
		***************************************************/
		$scope.selectText = function (containerid) {
			if (document.selection) {
				var range = document.body.createTextRange();
				range.moveToElementText(document.getElementById(containerid));
				range.select();
				document.execCommand("Copy");
			} else if (window.getSelection) {//IE
				var range = document.createRange();
				range.selectNode(document.getElementById(containerid));
				window.getSelection().removeAllRanges();
				window.getSelection().addRange(range);
				document.execCommand("Copy");
			}

		};

		/*******************************************************************
		* switchmap function allow to switch between satellite or simple map
		*******************************************************************/
		$scope.switchmap = function () {
			var MyControl = L.Control.extend({
				options: {
					position: 'bottomleft'
				},
				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'switchmap-control');
					container.innerHTML += '<button id="mapbuttonid" style="background-image:url(images/satellitemap.png);width:70px;height:70px;"></button>';
					$scope.imagemap = "satellitemap";
					$compile(container)($scope);
					return container;
				}
			});
			$scope.mymap.addControl(new MyControl());
			$('.switchmap-control').click(function (event) {
				if ($scope.imagemap == "satellitemap") {
					L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					document.getElementById("mapbuttonid").style.backgroundImage = "url(images/simplemap.png)";
					$scope.imagemap = "simplemap";
				} else {
					L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					document.getElementById("mapbuttonid").style.backgroundImage = "url(images/satellitemap.png)";
					$scope.imagemap = "satellitemap";
				}
			});
		};

	});
