'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:FileCtrl
 * @description
 * # FileCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
	.controller('FileCtrl', function ($scope, serviceHistory, uiGridConstants, serviceResource, $uibModal, $window, $resource, $routeParams, $templateCache, CONFIG, $http, $location, $q,/*$cookie,*/$interval, $timeout, tourConfig) {
		/***** AngularJS Tour *****/
		tourConfig.animation = false;		
		
		
		$scope.results = '';

		/*********************************************
		* addhistory function allows to add in history
		*********************************************/
		$scope.addhistory = function () {
			serviceHistory.addhistory(CONFIG.server + CONFIG.clientname + "/#/file/" + $routeParams.querystring);
		};

		/**************************************************************************************
		* currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
		**************************************************************************************/
		$scope.currentDateFormat = function () {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var HH = today.getHours();
			var MM = today.getMinutes();
			var SS = today.getSeconds();
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}
			if (HH < 10) {
				HH = "0" + HH;
			}
			if (MM < 10) {
				MM = "0" + MM;
			}
			if (SS < 10) {
				SS = "0" + SS;
			}
			return today = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM + ':' + SS;
		};

        /*************************************************************
	* loadingdialog function allows to open a modal with a spinner 
	**************************************************************/
		$scope.loadingdialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalLoading.html',
				size: 'm',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/*****************************************************
		* requestFiles function allows to request metadata
		******************************************************/
		$scope.requestFiles = function () {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combination/';
			var req = $resource(adress + ':myquery' + '/json', { myquery: '@myquery' }, { query: { method: 'get', isArray: true } });
			$scope.request_json = adress + $routeParams.querystring + '/json';

			$scope.loading = true;
			$scope.loadingdialog();
			req.query({ myquery: $routeParams.querystring }).$promise.then(function (data) {
				if (data.length > 0) {
					$scope.nodata = false;
					$scope.modalInstance.opened.then(function () {
						$scope.loading = false;//stop the spinner loading
						$scope.modalInstance.close();//stop the spinner loading
					});
					$scope.results = data;
					$scope.currentdate = $scope.currentDateFormat();

					$scope.gridOptions.data = $scope.results;
				} else {
					$scope.modalInstance.close();//stop the spinner loading
					$scope.openmessage($scope.messageinfonodata);
				}
			});
		};
		$scope.requestFiles();

		/*****************************************
		* gridHandler function handles grid part
		******************************************/
		$scope.gridHandler = function () {
			var screenwidth = screen.width;
			//define all grid options and their columns
			$scope.gridOptions = {
				enableGridMenu: false,
				enableFiltering: false,
				enableSorting: true,
				enableRowSelection: true,
				enableSelectAll: true,
				multiSelect: true,
				exporterMenuCsv: false,
				exporterMenuPdf: false,
				enableColumnMoving: true,
				enableColumnResizing: true,
				showGridFooter: true,
				//all function regarding the ui-grid
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
				},
				//definition of each column in the grid
				columnDefs: [
					{ name: 'File Name', field: 'name', sort: { direction: uiGridConstants.ASC, priority: 1 }, enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="row.isSelected=true; grid.appScope.opennodownload()">{{ COL_FIELD }}</a></div>' },
					{ name: 'File Type', field: 'file_type.format', enableColumnMenu: false },
					{ name: 'Reference Date', field: 'reference_date', enableColumnMenu: false, headerTooltip: 'date of the observation' },
					{ name: 'Published Date', field: 'published_date', enableColumnMenu: false, headerTooltip: 'first publication date' },
					{ name: 'Revision Date', field: 'revision_date', enableColumnMenu: false, headerTooltip: 'last date when the file has been revised' },
					{ name: 'Sampling Window', field: 'file_type.sampling_window', enableColumnMenu: false, headerTooltip: 'time window of the acquisitions' },
					{ name: 'Sampling Frequency', field: 'file_type.sampling_frequency', enableColumnMenu: false, headerTooltip: 'frequency of the acquisitions' },
					{ name: 'MD5 Check Sum', field: 'md5_checksum', enableColumnMenu: false, headerTooltip: 'MD5 for the compressed file' },
					{ name: 'File Size', field: 'file_size', enableColumnMenu: false },
					{ name: 'Data Center Acronym', field: 'data_center.acronym', enableColumnMenu: false, headerTooltip: 'Data Center' },
					{ name: 'Status', field: 'status', enableColumnMenu: false, headerTooltip: 'Current status of the file\n -3 : metadata incompliant with T1 metadata\n-2: QC running not successful\n-1 QC currently running\n0 File not checked\n1 File checked – OK according to the T1/T2/T3 metadata\n2 File checked – small inconsistency in RINEX header\n3 File checked – warning flag based on hardwired QC metrics' },
					{ name: 'Data center Protocol', field: 'data_center.protocol', enableColumnMenu: false, visible: false },
					{ name: 'Data center Hostname', field: 'data_center.hostname', enableColumnMenu: false, visible: false },
					{ name: 'Directory Naming', field: 'data_center.data_center_structure.directory_naming', enableColumnMenu: false, visible: false },
					{ name: 'Relative Path', field: 'relative_path', enableColumnMenu: false, visible: false },
				],
				data: []
			};
		};


		$scope.gridHandler();

        /*************************************************************************
	* downloadrinexfile function handles on click links to download rinex file
	*************************************************************************/
		$scope.downloadrinexfile = function (filename, hostname, directory) {
			for (var i = 0; i < $scope.results.length; i++) {
				if ($scope.results[i].name === filename && $scope.results[i].data_center.hostname === hostname && $scope.results[i].data_center.data_center_structure.directory_naming === directory) {
					if ($scope.results[i]['data_center']['data_center_structure']['directory_naming'] != "") {
						var link = $scope.results[i]['data_center']['protocol'] + "://" + $scope.results[i]['data_center']['hostname'] + "/" + $scope.results[i]['data_center']['data_center_structure']['directory_naming'] + "/" + $scope.results[i]['relative_path'] + "/" + $scope.results[i]['name'];
					} else {
						var link = $scope.results[i]['data_center']['protocol'] + "://" + $scope.results[i]['data_center']['hostname'] + "/" + $scope.results[i]['relative_path'] + "/" + $scope.results[i]['name'];
					}
					window.location = link; // start the download
				}
			}
		};


		/*************************************************************************************
		* downloadselectedfile function allow to download rinex file selected in the grid
		*************************************************************************************/
		$scope.downloadselectedfile = function () {
			var selectedRows = $scope.gridApi.selection.getSelectedRows();
			if (selectedRows.length > 0) {
				var confirmUser = $window.confirm('You need to configure your browser to avoid the download popup to appear for each file and allow popup for this site. If you do not want, you could download files by Wget Unix Script or File list. Do you want to continue ?');
				if (confirmUser) {
					var url = '';
					for (var i = 0; i < selectedRows.length; i++) {
						if (selectedRows[i]['data_center']['data_center_structure']['directory_naming'] != "") {
							url = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['data_center']['data_center_structure']['directory_naming'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
						} else {
							url = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
						}
						window.open(url);
					}
				}
			} else {
				$scope.openmessage($scope.messageinfogrid);
			}
		};



		/****************************************************************
		* download function to handle downloading files in different format  
		****************************************************************/
		$scope.download = function (filename, format) {
			var selectedRows = $scope.gridApi.selection.getSelectedRows();
			if (selectedRows.length > 0) {
				if (format === 'Json') {
					var jsonarray = [];
					for (var i = 0; i < selectedRows.length; i++) {
						for (var j = 0; j < $scope.results.length; j++) {
							if (selectedRows[i].name == $scope.results[j].name) {
								jsonarray.push($scope.results[j]);
							}
						}
					}

					var blob = new Blob([JSON.stringify(jsonarray)], { type: 'text/json' });
					if (window.navigator.msSaveOrOpenBlob) {//IE
						window.navigator.msSaveBlob(blob, filename);
					} else {
						var elem = window.document.createElement('a');
						elem.href = window.URL.createObjectURL(blob);
						elem.download = filename;
						document.body.appendChild(elem);
						elem.click();
						document.body.removeChild(elem);
					}
				} else if (format === 'sh') {
					var script_text = "";

					for (var i = 0; i < selectedRows.length; i++) {
						if (selectedRows[i]['data_center']['data_center_structure']['directory_naming'] != "") {
							var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['data_center']['data_center_structure']['directory_naming'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
						} else {
							var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
						}
						script_text = script_text + "wget " + link + "\n";
					}

					var blob = new Blob([script_text], { type: 'text/plain' });
					if (window.navigator.msSaveOrOpenBlob) {//IE
						window.navigator.msSaveBlob(blob, filename);
					} else {
						var elem = window.document.createElement('a');
						elem.href = window.URL.createObjectURL(blob);
						elem.download = filename;
						document.body.appendChild(elem);
						elem.click();
						document.body.removeChild(elem);
					}
				} else if (format === 'txt') {
					var script_text = "";

					for (var i = 0; i < selectedRows.length; i++) {
						if (selectedRows[i]['data_center']['data_center_structure']['directory_naming'] != "") {
							var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['data_center']['data_center_structure']['directory_naming'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
						} else {
							var link = selectedRows[i]['data_center']['protocol'] + "://" + selectedRows[i]['data_center']['hostname'] + "/" + selectedRows[i]['relative_path'] + "/" + selectedRows[i]['name'];
						}
						script_text = script_text + link + "\r\n";
					}

					var blob = new Blob([script_text], { type: 'text/plain' });
					if (window.navigator.msSaveOrOpenBlob) {//IE
						window.navigator.msSaveBlob(blob, filename);
					} else {
						var elem = window.document.createElement('a');
						elem.href = window.URL.createObjectURL(blob);
						elem.download = filename;
						document.body.appendChild(elem);
						elem.click();
						document.body.removeChild(elem);
					}
				} else if ((format === 'CSV') || (format === 'GeodesyML') || (format === 'SINEX') || (format === 'GAMIT')) {
					if (format === 'CSV') {
						var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combination/';
					}
					var req = $resource(adress + ':myquery' + '/csv', { myquery: '@myquery' }, { query: { method: 'get', isArray: false } });
					//execute asynchronous query
					req.query({ myquery: $routeParams.querystring }).$promise.then(function (data) {
						if (data !== '') {
							window.location = adress + $routeParams.querystring;
						} else {
							$scope.openmessage($scope.messageinfonodata);
						}
					});
				}
			} else {
				$scope.openmessage($scope.messageinfogrid);
			}
		};

		/***************************************************************************
		* exportquery function allows to users to get the query for command request
		***************************************************************************/
		$scope.exportquery = function (format) {
			var pyglass_request = "pyglass files -u " + CONFIG.server;
			if ($routeParams.querystring.indexOf("site=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("site:");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 5, endpos);
				pyglass_request = pyglass_request + " -sn " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("marker=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("marker=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -m " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("minLat=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("minLat=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsminlat " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("maxLat=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("maxLat=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsmaxlat " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("minLon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("minLon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsminlon " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("maxLon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("maxLon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsmaxlon " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("centerLat=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("centerLat=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -rcclat " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("centerLon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("centerLon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -rcclon " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("radius=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("radius=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rcr " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("polygon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("polygon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -ps " + param.replace(/[!,]/g, " ");
			}
			if ($routeParams.querystring.indexOf("altitude=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("altitude=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				var posseparatedparam = param.indexOf(",");
				if (posseparatedparam !== -1) {
					if (param.indexOf(",") !== 0) {
						pyglass_request = pyglass_request + " -htmin " + param.substring(0, posseparatedparam) + " -htmax " + param.substring(posseparatedparam + 1, param.length);
					} else {
						pyglass_request = pyglass_request + " -htmax " + param.substring(1, param.length);
					}
				} else {
					pyglass_request = pyglass_request + " -htmin " + param;
				}
			}
			if ($routeParams.querystring.indexOf("longitude=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("longitude=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				var posseparatedparam = param.indexOf(",");
				if (posseparatedparam !== -1) {
					if (param.indexOf(",") !== 0) {
						pyglass_request = pyglass_request + " -lgmin " + param.substring(0, posseparatedparam) + " -lgmax " + param.substring(posseparatedparam + 1, param.length);
					} else {
						pyglass_request = pyglass_request + " -lgmax " + param.substring(1, param.length);
					}
				} else {
					pyglass_request = pyglass_request + " -lgmin " + param;
				}
			}
			if ($routeParams.querystring.indexOf("latitude=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("latitude=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				var posseparatedparam = param.indexOf(",");
				if (posseparatedparam !== -1) {
					if (param.indexOf(",") !== 0) {
						pyglass_request = pyglass_request + " -ltmin " + param.substring(0, posseparatedparam) + " -ltmax " + param.substring(posseparatedparam + 1, param.length);
					} else {
						pyglass_request = pyglass_request + " -ltmax " + param.substring(1, param.length);
					}
				} else {
					pyglass_request = pyglass_request + " -ltmin " + param;
				}
			}
			if ($routeParams.querystring.indexOf("installedDateMin=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("installedDateMin=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -idmin " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("installedDateMax=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("installedDateMax=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -idmax " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("removedDateMin=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("removedDateMin=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -rdmin " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("removedDateMax=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("removedDateMax=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -rdmax " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("network=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("network=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -nw " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("invertedNetworks=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("invertedNetworks=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -inw " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("agency=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("agency=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -ag " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("receiver=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("receiver=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				pyglass_request = pyglass_request + " -rt " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("antenna=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("antenna=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -at " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("radome=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("radome=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -radt " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("country=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("country=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -co " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("state=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("state=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 6, endpos);
				pyglass_request = pyglass_request + " -pv " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("city=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("city=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 5, endpos);
				pyglass_request = pyglass_request + " -ct " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("satelliteSystem=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("satelliteSystem=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 16, endpos);
				pyglass_request = pyglass_request + " -ss " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("dateRange=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("dateRange=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -dr " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("dataAvailability=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("dataAvailability=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -da " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("fileType=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("fileType=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				pyglass_request = pyglass_request + " -ft " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("samplingFrequency=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("samplingFrequency=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 18, endpos);
				pyglass_request = pyglass_request + " -sf " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("samplingWindow=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("samplingWindow=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -sw " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("minimumObservationYears=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("minimumObservationYears=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 24, endpos);
				pyglass_request = pyglass_request + " -mo " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("statusfile=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("statusfile=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 11, endpos);
				pyglass_request = pyglass_request + " -stf " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("observationtype=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("observationtype=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 16, endpos);
				pyglass_request = pyglass_request + " -obst " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("frequency=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("frequency=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -freq " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("channel=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("channel=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -chan " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("constellation=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("constellation=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 14, endpos);
				pyglass_request = pyglass_request + " -constel " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("ratioepoch=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("ratioepoch=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 11, endpos);
				pyglass_request = pyglass_request + " -epoch " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("elevangle=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("elevangle=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -elang " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("multipathvalue=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("multipathvalue=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -multpath " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("nbcycleslips=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("nbcycleslips=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 13, endpos);
				pyglass_request = pyglass_request + " -cycslps " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("spprms=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("spprms=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -ssprms " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("nbclockjumps=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("nbclockjumps=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 13, endpos);
				pyglass_request = pyglass_request + " -clkjmps " + param.replace(/,/g, " ");
			}
			if (format === 'Json') {
				$scope.currentrequest = $scope.request_json;
				$scope.pyglassrequest = pyglass_request + " -o full-json";
			} else if (format === 'Json + files') {
				$scope.currentrequest = $scope.request_json_files;
				$scope.pyglassrequest = pyglass_request + " -o full-json+files";
			} else if (format === 'GeodesyML') {
				$scope.currentrequest = $scope.request_geodesyml;
				$scope.pyglassrequest = pyglass_request + " -o full-geodesyml";
			} else if (format === 'GeodesyML + files') {
				$scope.currentrequest = $scope.request_geodesyml_files;
				$scope.pyglassrequest = pyglass_request + " -o full-geodesyml+files";
			} else if (format === 'File list') {
				$scope.currentrequest = $scope.request_files_list;
				$scope.pyglassrequest = pyglass_request + " -o full-filelist";
			} else if (format === 'SINEX') {
				$scope.currentrequest = $scope.request_sinex;
				$scope.pyglassrequest = pyglass_request + " -o full-sinex";
			} else if (format === 'GAMIT') {
				$scope.currentrequest = $scope.request_gamit;
				$scope.pyglassrequest = pyglass_request + " -o full-gamit";
			} else if (format === 'CSV') {
				$scope.currentrequest = $scope.request_csv;
				$scope.pyglassrequest = pyglass_request + " -o full-csv";
			} else if (format === 'KML') {
				$scope.currentrequest = $scope.request_kml;
				$scope.pyglassrequest = pyglass_request + " -o full-kml";
			}
			$scope.opendialog();
		};

		/******************************************************************************************
		* opendialog function allows to open a modal when the users click on "request query" button 
		*******************************************************************************************/
		$scope.opendialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalRequest.html',
				size: 'lg',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/******************************************************************************************
		* opennodownload function allows to open a modal when the users click on "Download selected" 
		* to offer alternatives to ftp download
		*******************************************************************************************/

		$scope.opennodownload = function () {
			var selectedRows = $scope.gridApi.selection.getSelectedRows();
			if(selectedRows.length > 0) {
				return $scope.modalInstance = $uibModal.open({
					templateUrl: 'views/modalNoDownload.html',
					size: 'lg',
					scope: $scope,
					windowClass: 'center-modal'
				});
			} else {
				$scope.openmessage($scope.messageinfogrid);
			}
			
		}

		/***************************************
		* cancel function allows to cancel modal
		****************************************/
		$scope.cancel = function () {
			$scope.gridApi.selection.clearSelectedRows();
			$scope.modalInstance.dismiss("cancel");
		};

		/*******************************
		* messageinfogrid alert message
		********************************/
		$scope.messageinfogrid = {
			textAlert: "You have to select the files in the grid before to download",
			mode: 'info'
		};
		/*******************************************************
		* messageinfonodata variable to advanced error message
		********************************************************/
		$scope.messageinfonodata = {
			textAlert: "No data available",
			mode: 'info'
		};

  	/*******************************************************************
	* openmessage function allows to open a modal when there's an error
	*******************************************************************/
		$scope.openmessage = function (message) {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/messageBox.html',
				scope: $scope,
				size: 'lg',
				resolve: {
					data: function () {
						$scope.messageinfo = message;
						return $scope.messageinfo;
					}
				}
			});
		};
		/**************************************
		* close function allows to close modal
		***************************************/
		$scope.close = function () {
			$scope.modalInstance.close();
		};

	});
