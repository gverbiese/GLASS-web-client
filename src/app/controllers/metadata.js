'use strict';

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:MetadataCtrl
 * @description
 * # MetadataCtrl
 * Controller of the glasswebuiApp
 */


var Highcharts = require('highcharts/highstock');
require('highcharts/modules/heatmap')(Highcharts);

require('d3');
import * as CalHeatMap from 'cal-heatmap';
import { color } from 'highcharts/highstock';


angular.module('glasswebuiApp')
	.controller('MetadataCtrl', function ($scope, serviceHistory, serviceResource, $uibModal, $window, $timeout, $resource, $routeParams, $compile, $templateCache, CONFIG, $http, uiGridConstants, tourConfig) {
		/***** AngularJS Tour *****/
		tourConfig.animation = false;


		$scope.results = '';
		$scope.navitem = 'infoitem';
		$scope.metadataviewchoice = 'blank';
		$scope.myInterval = 5000;
		$scope.noWrapSlides = false;
		$scope.active = 0;
		var slides = $scope.slides = [];
		var currIndex = 0;
		$scope.markerselecteddict = {};
		$scope.colorNetworks = {};
		$scope.networkentity = '';
		$scope.onlyonelayerid = false;
		var mainchart;
		var markerchartdict = {};
		var markerchartarray = [];
		$scope.detailflag = false;
		$scope.timeseries_dict_ingv = {};
		$scope.timeseries_dict_uga = {};
		$scope.pictures_dict = {};

		$scope.filefromdateselected = '';
		$scope.filetodateselected = '';
		$scope.fileT3fromdateselected = '';
		$scope.fileT3todateselected = '';
		$scope.shiftkeypressed = false;


		$scope.isAlone = false;
		$scope.marker = "";
		$scope.marker_long_name = "";
		$scope.series_stations = {};
		$scope.stationInfo = [];
		$scope.stationPictures = [];
		$scope.dataLicence = [];
		var pictureIndex = 0;

		$scope.json = {};

		var order = []; // sotck values of series to sort the chart
		/*********************************************
		 * addhistory function allows to add in history
		 *********************************************/
		$scope.addhistory = function () {
			serviceHistory.addhistory(CONFIG.server + CONFIG.clientname + "/#/metadata/" + $routeParams.querystring);
		};


		/*******************************************************
		 * getPictures function allows to get pictures of station
		 ********************************************************/
		/*$scope.getPictures = function() {
			for(var i=0;i<$scope.results.length;i++)
			{
				for(var j=0;j<$scope.results[i].documents.length;j++)
				{
					if ($scope.results[i].documents[j].document_type.name=="timeseries")
					{
						$scope.timeseries_dict[$scope.results[i].marker.toUpperCase()] = CONFIG.server + CONFIG.glassapi + $scope.results[i].documents[j].link;
					}else{//take the first image on the list
						if( $scope.results[i].documents.length>0)
						{
							$scope.pictures_dict[$scope.results[i].marker.toUpperCase()] = CONFIG.server + CONFIG.glassapi + $scope.results[i].documents[0].link;
						}
					}
					slides.push({
							image: CONFIG.server + CONFIG.glassapi + $scope.results[i].documents[j].link,
							text: $scope.results[i].documents[j].title,
							id: currIndex++
							});
				}	
			}
		};*/
		//Version which were getting timeseries from another address
		$scope.getPictures = function () {
			$http.get('https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/UGA-CNRS/' + $scope.results[0].marker.toUpperCase()).then(function (response) {
				if (response.data.length > 0) {
					$scope.slides.push({
						image: 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/UGA-CNRS/' + $scope.results[0].marker.toUpperCase(),
						text: 'UGA',
						id: currIndex++,
						marker: $scope.results[0].marker.toUpperCase()
					});
				}
			});
			$http.get('https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/INGV/' + $scope.results[0].marker.toUpperCase()).then(function (response) {
				if (response.data.length > 0) {
					$scope.slides.push({
						image: 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/INGV/' + $scope.results[0].marker.toUpperCase(),
						text: 'INGV',
						id: currIndex++,
						marker: $scope.results[0].marker.toUpperCase()
					});
				}
			});
			$http.get('https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/LTK/' + $scope.results[0].marker.toUpperCase()).then(function (response) {
				if (response.data.length > 0) {
					$scope.slides.push({
						image: 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/LTK/' + $scope.results[0].marker.toUpperCase(),
						text: 'LTK',
						id: currIndex++,
						marker: $scope.results[0].marker.toUpperCase()
					});
				}
			});
		};
		/**************************************************************************************
		 * getSiteLog function allows to get the DoI and Licences for each station 
		 **************************************************************************************/
		$scope.getSitelog = function () {
			$http.get(
				CONFIG.dgwapi + "/api/md_station/stations_info/?format=json&station_marker=" + $scope.marker_long_name,
				{
					headers: {
						accept:"application/json",
					},
				},
			)
			.then(function (response) {
				$scope.stationInfo = response.data.results;
			})
			.catch(function () {
				console.log('no response from API')
			})
			$http.get(
				CONFIG.dgwapi + "/api/md_station/images_per_station/" + $scope.marker_long_name,
				{
					headers: {
						accept:"application/json",
					},
				},
			)
			.then(function (response) {
				for (var i = 0; i <= response.data.results.length - 1; i++) {
					response.data.results[i].index = pictureIndex++;
				}
				$scope.stationPictures = response.data;
				$scope.stationPictures = response.data.results;
			})
			
		}


		/**************************************************************************************
		 * currentDateFormat function allows to get a current date in format yyyy-mm-dd HH:MM:SS
		 **************************************************************************************/
		$scope.currentDateFormat = function () {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var HH = today.getHours();
			var MM = today.getMinutes();
			var SS = today.getSeconds();
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}
			if (HH < 10) {
				HH = "0" + HH;
			}
			if (MM < 10) {
				MM = "0" + MM;
			}
			if (SS < 10) {
				SS = "0" + SS;
			}
			return today = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM + ':' + SS;
		};

		/*************************************************************
		 * loadingdialog function allows to open a modal with a spinner 
		 **************************************************************/
		$scope.loadingdialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalLoading.html',
				size: 'm',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};

		/*****************************************************
		 * requestMetadata function allows to request metadata
		 ******************************************************/
		$scope.requestMetadata = function () {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/json?';
			var req = $resource(adress + ':myquery', {
				myquery: '@myquery'
			}, {
				query: {
					method: 'get',
					isArray: true
				}
			});
			$scope.request_json = adress + $routeParams.querystring; //$routeParams.querystring is the parameter defined in app.js for routing
			/*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/" + $routeParams.querystring;
			$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/" + $routeParams.querystring;
			$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/" + $routeParams.querystring;
			$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/" + $routeParams.querystring;
			$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/" + $routeParams.querystring;
			$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/" + $routeParams.querystring;
			$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/" + $routeParams.querystring;
			$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/" + $routeParams.querystring;*/
			$scope.loading = true;
			$scope.loadingdialog();
			req.query({
				myquery: $routeParams.querystring
			}).$promise.then(function (data) {
				if (data.length > 0) {
					$scope.nodata = false;
					$scope.createNetworkJSON(data);
					$scope.createColorNetworksDict(data);
					if (data.length === 1) //case 1 station
					{
						$scope.marker = data[0].marker;
						$scope.marker_long_name = data[0].marker_long_name;
						$scope.isAlone = true;
						$scope.metadataviewchoice = 'onestation';
						$scope.results = data;
						$scope.currentdate = $scope.currentDateFormat();
						for (var i = 0; i < $scope.results.length; i++) {
							if ($scope.results[i].date_to === null) {
								$scope.results[i]['date_to'] = $scope.currentdate;
								$scope.status = "Active";
							} else {
								$scope.status = "Deactivated";
							}
						}

						$timeout(function () {
							$scope.mymap = L.map('metamapid', {
								minZoom: 2,
								gestureHandling: true,
								zoomDelta: 0.5
							});
							L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
							$scope.switchmap();
							$scope.showZoomMessage();
							$scope.markersHandler($scope.results);
							$scope.mymap.scrollWheelZoom.disable();
							//Scroll  with CTRL+Mouse method MultiMap
							$('#metamapid').bind('mousewheel DOMMouseScroll', function (event) {
								event.stopPropagation();
								if (event.ctrlKey == true) {
									event.preventDefault();
									$scope.mymap.scrollWheelZoom.enable();
									setTimeout(function () {
										$scope.mymap.scrollWheelZoom.disable();
									}, 2000);
								} else {
									$scope.mymap.scrollWheelZoom.disable();
								}
							});
						}, 100); //set timeout 100ms to wait the DOM is loading
						$scope.getPictures();
						$scope.getSitelog();
						$scope.station_equipments = $scope.equipment();
						$scope.modalInstance.opened.then(function () {
							$scope.loading = false;
							$scope.modalInstance.close(); //stop the spinner loading
						});
					} else if (data.length > 1) { //case several stations
						$scope.isAlone = false;
						$scope.metadataviewchoice = 'severalstation';
						$scope.results = data;
						$scope.currentdate = $scope.currentDateFormat();
						for (var i = 0; i < $scope.results.length; i++) {
							if ($scope.results[i].date_to === null) {
								$scope.results[i]['date_to'] = $scope.currentdate;
								$scope.status = "Active";
							} else {
								$scope.status = "Deactivated";
							}
						}
						$scope.results = $scope.getlastequipment();
						$scope.network = $scope.getNetwork($scope.results);

						$scope.createColorNetworksDict($scope.results);

						$timeout(function () {
							$scope.mymap = L.map('metamapid2', {
								minZoom: 2,
								gestureHandling: true,
								zoomDelta: 0.5
							});
							L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
							$scope.switchmap();
							$scope.showZoomMessage();
							$scope.markersHandler_multi($scope.results);
							$scope.mymap.scrollWheelZoom.disable();
							//Scroll  with CTRL+Mouse method Map
							$('#metamapid2').bind('mousewheel DOMMouseScroll', function (event) {
								event.stopPropagation();
								if (event.ctrlKey == true) {
									event.preventDefault();
									$scope.mymap.scrollWheelZoom.enable();
									setTimeout(function () {
										$scope.mymap.scrollWheelZoom.disable();
									}, 2000);
								} else {
									$scope.mymap.scrollWheelZoom.disable();
								}
							});
						}, 100); //set timeout 100ms to wait the DOM is loading

						//loop to display in the grid in the format string the networks like "RENAG RGP" 
						angular.forEach($scope.results, function (row) {
							row.getNetwork = function () {
								var networkStr = '';
								for (var j = 0; j < row.station_networks.length; j++) {
									networkStr = networkStr + " " + row.station_networks[j].network.name;
								}
								if (networkStr.substr(0, 1) === " ") {
									networkStr = networkStr.substr(1);
								}
								return networkStr;
							};
						});
						markerchartdict = $scope.fillmarkerdict($scope.results);
						$scope.gridHandler($scope.results);
						$timeout(function () {
							$scope.temporalcharts();
						}, 100); //set timeout 100ms to wait the DOM is loading
						//$scope.prepareTimeseries($scope.results[0].marker.toUpperCase());
					}
				} else {
					$scope.modalInstance.close(); //stop the spinner loading
					$scope.openmessage($scope.messageinfonodata);
				}
			});
			$(document).keydown(function (e) {
				if (e.keyCode == 16) {
					$scope.shiftkeypressed = true;
				} else {
					$scope.shiftkeypressed = false;
				}
			});
		};
		$scope.requestMetadata();
						

		/**
		 * Show a message on the map explaining how to zoom
		 */
		$scope.showZoomMessage = function () {
			L.Control.zoomText = L.Control.extend({
				onAdd: function (map) {
					var text = L.DomUtil.create('div');
					text.id = "zoomText";
					text.innerHTML = "<strong>Zoom : CTRL+SCROLL</strong>";
					return text;
				},
				onRemove: function (map) { }
			});
			L.control.zoomText = function (opts) { return new L.Control.zoomText(opts); }
			L.control.zoomText({ position: 'bottomright' }).addTo($scope.mymap);
		}



		/*******************************************************************************
		 * sortequipment function allows to sort receivers, antennas and radomes per date
		 *******************************************************************************/
		$scope.sortequipment = function () {
			var precreceiverdate = '';
			var precantennadate = '';
			var precradomedate = '';

			for (var i = 0; i < $scope.results.length; i++) {
				for (var j = 0; j < $scope.results[i].station_items.length; j++) {
					if ($scope.results[i].station_items[j].item.item_type.name === "antenna") {
						if (precantennadate != '') {
							var d1 = new Date(precantennadate.replace(/-/g, '/'));
							var d2 = new Date($scope.results[i].station_items[j].item.item_attributes[0].date_from.replace(/-/g, '/'));
							if (d1.getTime() > d2.getTime()) {
								var temp = $scope.results[i].station_items[j];
								$scope.results[i].station_items[j] = $scope.results[i].station_items[j - 1];
								$scope.results[i].station_items[j - 1] = temp;
							}
						}
						precantennadate = $scope.results[i].station_items[j].item.item_attributes[0].date_from;
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "receiver") {
						if (precreceiverdate != '') {
							var d1 = new Date(precreceiverdate.replace(/-/g, '/'));
							var d2 = new Date($scope.results[i].station_items[j].item.item_attributes[0].date_from.replace(/-/g, '/'));
							if (d1.getTime() > d2.getTime()) {
								var temp = $scope.results[i].station_items[j];
								$scope.results[i].station_items[j] = $scope.results[i].station_items[j - 1];
								$scope.results[i].station_items[j - 1] = temp;
							}
						}
						precreceiverdate = $scope.results[i].station_items[j].item.item_attributes[0].date_from;
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "radome") {
						if (precradomedate != '') {
							var d1 = new Date(precradomedate.replace(/-/g, '/'));
							var d2 = new Date($scope.results[i].station_items[j].item.item_attributes[0].date_from.replace(/-/g, '/'));
							if (d1.getTime() > d2.getTime()) {
								var temp = $scope.results[i].station_items[j];
								$scope.results[i].station_items[j] = $scope.results[i].station_items[j - 1];
								$scope.results[i].station_items[j - 1] = temp;
							}
						}
						precradomedate = $scope.results[i].station_items[j].item.item_attributes[0].date_from;
					}
				}
			}
			return $scope.results;
		};



		/*****************************************************************************
		 * equipment function allows to get receivers, antenna and radome to display
		 ******************************************************************************/
		$scope.equipment = function () {
			var arrtemp = [];
			$scope.antennalist = [];
			$scope.receiverlist = [];
			$scope.radomelist = [];
			$scope.humiditysensorlist = [];
			$scope.pressuresensorlist = [];
			$scope.temperaturesensorlist = [];
			$scope.watervaporsensorlist = [];
			$scope.frequencystandardsensorlist = [];
			var precdate = '';
			var count = 0;
			$scope.results = $scope.sortequipment();
			for (var i = 0; i < $scope.results.length; i++) {
				for (var j = 0; j < $scope.results[i].station_items.length; j++) {
					if ($scope.results[i].station_items[j].item.item_type.name === "antenna") {
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null) {
							$scope.antenna_dateto = $scope.currentdate;
						} else {
							$scope.antenna_dateto = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.antenna_comment = $scope.results[i].station_items[j].item.comment;
						for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_type") {
								$scope.antenna_name = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.antenna_datefrom = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								$scope.antenna_igs = $scope.results[i].station_items[j].item.item_attributes[k].igs_defined;
								$scope.antenna_model = $scope.results[i].station_items[j].item.item_attributes[k].model;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null) {
									$scope.antenna_dateto = $scope.currentdate;
								} else {
									$scope.antenna_dateto = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number") {
								$scope.antenna_serial_number = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.antenna_height = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.antenna_height = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.antenna_height = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_reference_point") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.antenna_reference_point = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.antenna_reference_point = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.antenna_reference_point = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "marker_arp_up_ecc") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.marker_arp_up_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.marker_arp_up_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.marker_arp_up_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "marker_arp_north_ecc") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.marker_arp_north_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.marker_arp_north_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.marker_arp_north_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "marker_arp_east_ecc") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.marker_arp_east_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.marker_arp_east_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.marker_arp_east_ecc = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "alignment_from_true_n") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.alignment_from_true_n = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.alignment_from_true_n = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.alignment_from_true_n = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_cable_type") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.antenna_cable_type = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.antenna_cable_type = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.antenna_cable_type = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_cable_length") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.antenna_cable_length = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.antenna_cable_length = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.antenna_cable_length = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
						}
						$scope.antennalist.push({
							datefrom: $scope.antenna_datefrom,
							dateto: $scope.antenna_dateto,
							antenna: $scope.antenna_name,
							antenna_serial_number: $scope.antenna_serial_number,
							height: $scope.antenna_height,
							igs: $scope.antenna_igs,
							model: $scope.antenna_model,
							antenna_reference_point: $scope.antenna_reference_point,
							marker_arp_up_ecc: $scope.marker_arp_up_ecc,
							marker_arp_north_ecc: $scope.marker_arp_north_ecc,
							marker_arp_east_ecc: $scope.marker_arp_east_ecc,
							alignment_from_true_n: $scope.alignment_from_true_n,
							antenna_cable_type: $scope.antenna_cable_type,
							antenna_cable_length: $scope.antenna_cable_length,
							comment: $scope.antenna_comment
						});
						for (var l = 0; l < $scope.results[i].station_items.length; l++) {

							if ($scope.results[i].station_items[l].item.item_type.name === "radome") {
								$scope.radome_comment = $scope.results[i].station_items[l].item.comment;
								for (var k = 0; k < $scope.results[i].station_items[l].item.item_attributes.length; k++) {
									if ($scope.results[i].station_items[l].item.item_attributes[k].attribute.name === "radome_type") {
										if ($scope.results[i].station_items[l].item.item_attributes[k].date_to === null) {
											$scope.radome_dateto = $scope.currentdate;
										} else {
											$scope.radome_dateto = $scope.results[i].station_items[l].item.item_attributes[k].date_to;
										}
										if (($scope.results[i].station_items[l].item.item_attributes[k].date_from === $scope.antenna_datefrom) && ($scope.radome_dateto === $scope.antenna_dateto)) {
											$scope.radome_name = $scope.results[i].station_items[l].item.item_attributes[k].value_varchar;
											$scope.radomelist.push({
												datefrom: $scope.results[i].station_items[l].item.item_attributes[k].date_from,
												dateto: $scope.radome_dateto,
												radome: $scope.radome_name,
												igs: $scope.results[i].station_items[l].item.item_attributes[k].igs_defined,
												description: $scope.results[i].station_items[l].item.item_attributes[k].description,
												comment: $scope.radome_comment
											});
										}
									}
								}

							}
						}
						for (var l = 0; l < $scope.results[i].station_items.length; l++) {
							if ($scope.results[i].station_items[l].item.item_type.name === "receiver") {
								$scope.receiver_comment = $scope.results[i].station_items[l].item.comment;
								for (var m = 0; m < $scope.results[i].station_items[l].item.item_attributes.length; m++) {
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "receiver_type") {
										$scope.receiver_name = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										$scope.receiver_datefrom = $scope.results[i].station_items[l].item.item_attributes[m].date_from;
										$scope.receiver_igs = $scope.results[i].station_items[l].item.item_attributes[m].igs_defined;
										$scope.receiver_model = $scope.results[i].station_items[l].item.item_attributes[m].model;
										if ($scope.results[i].station_items[l].item.item_attributes[m].date_to === null) {
											$scope.receiver_dateto = $scope.currentdate;
										} else {
											$scope.receiver_dateto = $scope.results[i].station_items[l].item.item_attributes[m].date_to;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "serial_number") {
										$scope.receiver_serial_number = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "firmware_version") {
										$scope.receiver_firmware_version = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "satellite_system") {
										$scope.receiver_satellites = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "elevation_cutoff_setting") {
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
											$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
											$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
											$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "temperature_stabilization") {
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
											$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
											$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
											$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "input_frequency") {
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
											$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
											$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
											$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
									if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "data_sampling_interval") {
										if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
											$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
											$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
										} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
											$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
										}
									}
								}
								if (count === 0) { //to avoid to display several time the receivers
									$scope.receiverlist.push({
										datefrom: $scope.receiver_datefrom,
										dateto: $scope.receiver_dateto,
										receiver: $scope.receiver_name,
										receiver_serial_number: $scope.receiver_serial_number,
										firmware: $scope.receiver_firmware_version,
										satellites_system: $scope.receiver_satellites,
										igs: $scope.receiver_igs,
										model: $scope.receiver_model,
										receiver_elevation_cutoff_setting: $scope.receiver_elevation_cutoff_setting,
										receiver_temperature_stabilization: $scope.receiver_temperature_stabilization,
										receiver_input_frequency: $scope.receiver_input_frequency,
										receiver_data_sampling_interval: $scope.receiver_data_sampling_interval,
										comment: $scope.receiver_comment
									});
								}
								// Part of session view
								var d1 = new Date($scope.results[i].station_items[l].item.item_attributes[0].date_from.replace(/-/g, '/'));
								var d2 = new Date($scope.antenna_dateto.replace(/-/g, '/'));
								var d3 = new Date($scope.receiver_dateto.replace(/-/g, '/'));
								var d4 = new Date($scope.antenna_datefrom.replace(/-/g, '/'));
								if ((d1.getTime() <= d2.getTime()) && (d3.getTime() >= d4.getTime())) {
									for (var m = 0; m < $scope.results[i].station_items[l].item.item_attributes.length; m++) {
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "receiver_type") {
											$scope.receiver_name = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
											$scope.receiver_datefrom = $scope.results[i].station_items[l].item.item_attributes[m].date_from;
											if ($scope.results[i].station_items[l].item.item_attributes[m].date_to === null) {
												$scope.receiver_dateto = $scope.currentdate;
											} else {
												$scope.receiver_dateto = $scope.results[i].station_items[l].item.item_attributes[m].date_to;
											}
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "serial_number") {
											$scope.receiver_serial_number = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "firmware_version") {
											$scope.receiver_firmware_version = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "satellite_system") {
											$scope.receiver_satellites = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "elevation_cutoff_setting") {
											if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
												$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
												$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
												$scope.receiver_elevation_cutoff_setting = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
											}
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "temperature_stabilization") {
											if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
												$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
												$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
												$scope.receiver_temperature_stabilization = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
											}
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "input_frequency") {
											if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
												$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
												$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
												$scope.receiver_input_frequency = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
											}
										}
										if ($scope.results[i].station_items[l].item.item_attributes[m].attribute.name === "data_sampling_interval") {
											if ($scope.results[i].station_items[l].item.item_attributes[m].value_varchar !== null) {
												$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_varchar;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_numeric !== null) {
												$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_numeric;
											} else if ($scope.results[i].station_items[l].item.item_attributes[m].value_date !== null) {
												$scope.receiver_data_sampling_interval = $scope.results[i].station_items[l].item.item_attributes[m].value_date;
											}
										}
									}
									if (precdate === '') {
										var d5 = new Date($scope.receiver_dateto.replace(/-/g, '/'));
										var d6 = new Date($scope.antenna_dateto.replace(/-/g, '/'));
										if (d5.getTime() <= d6.getTime()) {
											var dateto = $scope.receiver_dateto;
										} else {
											var dateto = $scope.antenna_dateto;
										}
										arrtemp.push({
											datefrom: $scope.receiver_datefrom,
											dateto: dateto,
											antenna: $scope.antenna_name,
											antenna_serial_number: $scope.antenna_serial_number,
											antenna_height: $scope.antenna_height,
											antenna_reference_point: $scope.antenna_reference_point,
											marker_arp_up_ecc: $scope.marker_arp_up_ecc,
											marker_arp_north_ecc: $scope.marker_arp_north_ecc,
											marker_arp_east_ecc: $scope.marker_arp_east_ecc,
											alignment_from_true_n: $scope.alignment_from_true_n,
											antenna_cable_type: $scope.antenna_cable_type,
											antenna_cable_length: $scope.antenna_cable_length,
											radome: $scope.radome_name,
											receiver: $scope.receiver_name,
											receiver_serial_number: $scope.receiver_serial_number,
											firmware: $scope.receiver_firmware_version,
											satellites_system: $scope.receiver_satellites,
											receiver_elevation_cutoff_setting: $scope.receiver_elevation_cutoff_setting,
											receiver_temperature_stabilization: $scope.receiver_temperature_stabilization,
											receiver_input_frequency: $scope.receiver_input_frequency,
											receiver_data_sampling_interval: $scope.receiver_data_sampling_interval
										});
										precdate = dateto;
									} else {
										var d7 = new Date($scope.receiver_datefrom.replace(/-/g, '/'));
										var d8 = new Date(precdate.replace(/-/g, '/'));
										if (d7.getTime() < d8.getTime()) {
											var datefrom = $scope.antenna_datefrom;
										} else {
											var datefrom = $scope.receiver_datefrom;
										}
										var d9 = new Date($scope.receiver_dateto.replace(/-/g, '/'));
										var d10 = new Date($scope.antenna_dateto.replace(/-/g, '/'));
										if (d9.getTime() <= d10.getTime()) {
											var dateto = $scope.receiver_dateto;
										} else {
											var dateto = $scope.antenna_dateto;
										}
										arrtemp.push({
											datefrom: datefrom,
											dateto: dateto,
											antenna: $scope.antenna_name,
											antenna_serial_number: $scope.antenna_serial_number,
											antenna_height: $scope.antenna_height,
											antenna_reference_point: $scope.antenna_reference_point,
											marker_arp_up_ecc: $scope.marker_arp_up_ecc,
											marker_arp_north_ecc: $scope.marker_arp_north_ecc,
											marker_arp_east_ecc: $scope.marker_arp_east_ecc,
											alignment_from_true_n: $scope.alignment_from_true_n,
											antenna_cable_type: $scope.antenna_cable_type,
											antenna_cable_length: $scope.antenna_cable_length,
											radome: $scope.radome_name,
											receiver: $scope.receiver_name,
											receiver_serial_number: $scope.receiver_serial_number,
											firmware: $scope.receiver_firmware_version,
											satellites_system: $scope.receiver_satellites,
											receiver_elevation_cutoff_setting: $scope.receiver_elevation_cutoff_setting,
											receiver_temperature_stabilization: $scope.receiver_temperature_stabilization,
											receiver_input_frequency: $scope.receiver_input_frequency,
											receiver_data_sampling_interval: $scope.receiver_data_sampling_interval
										});
										precdate = dateto;
									}

								}

							}
						}

						count += 1;
					}
					// End of part of session view
					//other instrument
					if ($scope.results[i].station_items[j].item.item_type.name === "humidity_sensor") {
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null) {
							$scope.sensor_dateto_humidity = $scope.currentdate;
						} else {
							$scope.sensor_dateto_humidity = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.sensor_comment_humidity = $scope.results[i].station_items[j].item.comment;
						for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "sensor_model") {
								$scope.sensor_model_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.sensor_datefrom_humidity = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null) {
									$scope.sensor_dateto_humidity = $scope.currentdate;
								} else {
									$scope.sensor_dateto_humidity = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer") {
								$scope.sensor_manufacturer_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number") {
								$scope.sensor_serial_number_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "data_sampling_interval") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_data_sampling_interval_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_data_sampling_interval_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_data_sampling_interval_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "accuracy") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_accuracy_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_accuracy_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_accuracy_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_height_diff_to_ant_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_height_diff_to_ant_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_height_diff_to_ant_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date") {
								$scope.sensor_calibration_date_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "aspiration") {
								$scope.sensor_aspiration_humidity = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
						}
						$scope.humiditysensorlist.push({
							datefrom: $scope.sensor_datefrom_humidity,
							dateto: $scope.sensor_dateto_humidity,
							sensor_model: $scope.sensor_model_humidity,
							sensor_manufacturer: $scope.sensor_manufacturer_humidity,
							sensor_serial_number: $scope.sensor_serial_number_humidity,
							sensor_data_sampling_interval: $scope.sensor_data_sampling_interval_humidity,
							sensor_accuracy: $scope.sensor_accuracy_humidity,
							sensor_height_diff_to_ant: $scope.sensor_height_diff_to_ant_humidity,
							sensor_calibration_date: $scope.sensor_calibration_date_humidity,
							sensor_aspiration: $scope.sensor_aspiration_humidity,
							comment: $scope.sensor_comment_humidity
						});
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "pressure_sensor") {
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null) {
							$scope.sensor_dateto_pressure = $scope.currentdate;
						} else {
							$scope.sensor_dateto_pressure = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.sensor_comment_pressure = $scope.results[i].station_items[j].item.comment;
						for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "sensor_model") {
								$scope.sensor_model_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.sensor_datefrom_pressure = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null) {
									$scope.sensor_dateto_pressure = $scope.currentdate;
								} else {
									$scope.sensor_dateto_pressure = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer") {
								$scope.sensor_manufacturer_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number") {
								$scope.sensor_serial_number_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "data_sampling_interval") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_data_sampling_interval_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_data_sampling_interval_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_data_sampling_interval_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "accuracy") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_accuracy_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_accuracy_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_accuracy_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_height_diff_to_ant_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_height_diff_to_ant_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_height_diff_to_ant_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date") {
								$scope.sensor_calibration_date_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "aspiration") {
								$scope.sensor_aspiration_pressure = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
						}
						$scope.pressuresensorlist.push({
							datefrom: $scope.sensor_datefrom_pressure,
							dateto: $scope.sensor_dateto_pressure,
							sensor_model: $scope.sensor_model_pressure,
							sensor_manufacturer: $scope.sensor_manufacturer_pressure,
							sensor_serial_number: $scope.sensor_serial_number_pressure,
							sensor_data_sampling_interval: $scope.sensor_data_sampling_interval_pressure,
							sensor_accuracy: $scope.sensor_accuracy_pressure,
							sensor_height_diff_to_ant: $scope.sensor_height_diff_to_ant_pressure,
							sensor_calibration_date: $scope.sensor_calibration_date_pressure,
							sensor_aspiration: $scope.sensor_aspiration_pressure,
							comment: $scope.sensor_comment_pressure
						});
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "temperature_sensor") {
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null) {
							$scope.sensor_dateto_temperature = $scope.currentdate;
						} else {
							$scope.sensor_dateto_temperature = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.sensor_comment_temperature = $scope.results[i].station_items[j].item.comment;
						for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "sensor_model") {
								$scope.sensor_model_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.sensor_datefrom_temperature = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null) {
									$scope.sensor_dateto_temperature = $scope.currentdate;
								} else {
									$scope.sensor_dateto_temperature = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer") {
								$scope.sensor_manufacturer_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number") {
								$scope.sensor_serial_number_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "data_sampling_interval") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_data_sampling_interval_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_data_sampling_interval_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_data_sampling_interval_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "accuracy") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_accuracy_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_accuracy_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_accuracy_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_height_diff_to_ant_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_height_diff_to_ant_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_height_diff_to_ant_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date") {
								$scope.sensor_calibration_date_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "aspiration") {
								$scope.sensor_aspiration_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "distance_to_antenna") {
								$scope.sensor_distance_to_antenna_temperature = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
							}
						}
						$scope.temperaturesensorlist.push({
							datefrom: $scope.sensor_datefrom_temperature,
							dateto: $scope.sensor_dateto_temperature,
							sensor_model: $scope.sensor_model_temperature,
							sensor_manufacturer: $scope.sensor_manufacturer_temperature,
							sensor_serial_number: $scope.sensor_serial_number_temperature,
							sensor_data_sampling_interval: $scope.sensor_data_sampling_interval_temperature,
							sensor_accuracy: $scope.sensor_accuracy_temperature,
							sensor_height_diff_to_ant: $scope.sensor_height_diff_to_ant_temperature,
							sensor_calibration_date: $scope.sensor_calibration_date_temperature,
							sensor_aspiration: $scope.sensor_aspiration_temperature,
							comment: $scope.sensor_comment_temperature
						});
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "water_vapor_radiometer") {
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null) {
							$scope.sensor_dateto_radiometer = $scope.currentdate;
						} else {
							$scope.sensor_dateto_radiometer = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.sensor_comment_radiometer = $scope.results[i].station_items[j].item.comment;
						for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {

							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "water_vapor_radiometer") {
								$scope.sensor_water_vapor_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.sensor_datefrom_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null) {
									$scope.sensor_dateto_radiometer = $scope.currentdate;
								} else {
									$scope.sensor_dateto_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "manufacturer") {
								$scope.sensor_manufacturer_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "serial_number") {
								$scope.sensor_serial_number_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "height_diff_to_ant") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_height_diff_to_ant_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_height_diff_to_ant_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_height_diff_to_ant_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "calibration_date") {
								$scope.sensor_calibration_date_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "distance_to_antenna") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_distance_to_antenna_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_distance_to_antenna_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_distance_to_antenna_radiometer = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}
						}
						$scope.watervaporsensorlist.push({
							datefrom: $scope.sensor_datefrom_radiometer,
							dateto: $scope.sensor_dateto_radiometer,
							sensor_manufacturer: $scope.sensor_manufacturer_radiometer,
							sensor_serial_number: $scope.sensor_serial_number_radiometer,
							sensor_height_diff_to_ant: $scope.sensor_height_diff_to_ant_radiometer,
							sensor_calibration_date: $scope.sensor_calibration_date_radiometer,
							sensor_distance_to_antenna: $scope.sensor_distance_to_antenna_radiometer,
							sensor_water_vapor_radiometer: $scope.sensor_water_vapor_radiometer,
							comment: $scope.sensor_comment_radiometer
						});
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "frequency_standard") {
						if ($scope.results[i].station_items[j].item.item_attributes[0].date_to === null) {
							$scope.sensor_dateto_frequency = $scope.currentdate;
						} else {
							$scope.sensor_dateto_frequency = $scope.results[i].station_items[j].item.item_attributes[0].date_to;
						}
						$scope.sensor_comment_frequency = $scope.results[i].station_items[j].item.comment;
						for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {

							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "standard_type") {
								$scope.sensor_standard_type_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								$scope.sensor_datefrom_frequency = $scope.results[i].station_items[j].item.item_attributes[k].date_from;
								if ($scope.results[i].station_items[j].item.item_attributes[k].date_to === null) {
									$scope.sensor_dateto_frequency = $scope.currentdate;
								} else {
									$scope.sensor_dateto_frequency = $scope.results[i].station_items[j].item.item_attributes[k].date_to;
								}
							}
							if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "input_frequency") {
								if ($scope.results[i].station_items[j].item.item_attributes[k].value_varchar !== null) {
									$scope.sensor_input_frequency_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_numeric !== null) {
									$scope.sensor_input_frequency_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_numeric;
								} else if ($scope.results[i].station_items[j].item.item_attributes[k].value_date !== null) {
									$scope.sensor_input_frequency_frequency = $scope.results[i].station_items[j].item.item_attributes[k].value_date;
								}
							}

						}
						$scope.frequencystandardsensorlist.push({
							datefrom: $scope.sensor_datefrom_frequency,
							dateto: $scope.sensor_dateto_frequency,
							sensor_standard_type: $scope.sensor_standard_type_frequency,
							sensor_input_frequency: $scope.sensor_input_frequency_frequency,
							comment: $scope.sensor_comment_frequency
						});
					}
				}
			}
			return arrtemp;
		};

		$scope.markers = new L.FeatureGroup();

		/**************************************************************
		 * findUnique function allows to return array with unique values
		 ***************************************************************/
		$scope.findUnique = function (arr) {
			var distinct = [];

			for (var i = 0; i < arr.length; i++) {
				var str = arr[i];
				if (distinct.indexOf(str) === -1) {
					distinct.push(str);
				}
			}
			return distinct;

		};

		/********************************************************************************************************************
		 * createColorNetworksDict function allows to have a dictionary of color for each network and get an array of networks
		 ********************************************************************************************************************/
		$scope.createColorNetworksDict = function (data) {
			var arrtemp = [];
			var arrColor = {};
			for (var i = 0; i < data.length; i++) {
				var tab = $scope.getNetworksNames(data[i].station_networks);
				tab.forEach(elem => {
					//var name = $scope.formatNetworkName(elem);
					if (!(elem in arrColor)) {
						arrtemp.push(elem);
						arrColor[elem] = {
							"color": $scope.getColor(elem)
						}
					}
				})

			}
			$scope.networkStr = arrtemp;
			$scope.colorNetworks = arrColor;
		};

		/**
		 * Create network.json which allow us to give colors and shape to markers
		 */
		$scope.createNetworkJSON = function (data) {
			var json = [];
			var registered = [];
			data.forEach(elem => {
				elem.station_networks.forEach(ntwk => {
					var ntk = ntwk.network.name;
					var type = "local"	// we assume every network is local
					var color = "#" + ('00000' + (Math.random() * (1 << 24) | 0).toString(16)).slice(-6);
					if (registered.indexOf(ntk) === -1) {
						if (["EPN", "IGS", "EPOS"].indexOf(ntk) >= 0) { // check if it is an international network
							type = "international";
							if (ntwk === "IGS") {
								color = "#FF0000";
							} else {
								color = "#EE9900";
							}
						} else if (["VOLC", "NFO_TABOO"].indexOf(ntk) >= 0) { // check if it is a virtual network
							type = "virtual";
							if (ntwk === "NFO_TABOO") {
								color = "#00FFFF";
							} else if (ntwk = "VOLC") {
								color = "#00AAFF";
							}
						}
						var obj = {
							"network": $scope.formatNetworkName(ntk),
							"color": color,
							"type": type
						};
						json.push(obj);
						registered.push(ntk);
					}
				});
			});
			$scope.json = json;
		}
		/**
		 * formatNetworkName() return the String given without some special caracters
		 */
		$scope.formatNetworkName = function (networkName) {
			var res = networkName.replace(/&/g, '');
			res = res.replace(/[\s]/g, '');
			res = res.replace('.', '');
			res = res.replace('/', '');
			return res;
		}

		/**
		 * Make a tab from the given string
		 * @param {String} str 
		 */
		$scope.makeTabFromStr = function (str) {
			var str = str.replace('.', '');
			return str.split(' & ');
		}
		/**
		 * getNetworkNames() return a tab with every network names from result.station_networks
		 * @param {String[]} array 
		 */
		$scope.getNetworksNames = function (array) {
			var tab = [];
			array.forEach(elem => {
				tab.push(elem.network.name);
			})
			return tab;
		}
		/**
		 * makeStr() return a String from the given array (result.station_networks)
		 * @param {String[]} array 
		 */
		$scope.makeStr = function (array) {
			var str = "";
			array.forEach(elem => {
				str += elem.network.name + " ";
			})
			return str;
		}

		/**
		 * Return the options needed for this station ( on the map )
		 * @param {String[]} networkNames 
		 */
		$scope.getOption = function (networkNames) {
			var options = { radius: 7, fillColor: "", color: "#000", weight: 1, opacity: 1, fillOpacity: 0.8 };
			networkNames.forEach(element => {
				var name = $scope.formatNetworkName(element);
				$scope.json.forEach(elem => {
					if (name == elem.network) {
						switch (elem.type) {
							case "local":
								options.fillColor = elem.color;
								break;
							case "international":
								if (options.fillColor === "") {
									options.fillColor = "#FFFFFF";
								}
								options.color = elem.color;
								options.weight = 3;
								break;
							case "virtual":
								if (options.fillColor === "") {
									options.fillColor = "#FFFFFF";
								}
								options.color = elem.color;
								options.weight = 3;
								break;
							default:
								options = { radius: 7, fillColor: "#00FF00", color: "#000", weight: 1, opacity: 1, fillOpacity: 0.8 };
						}
					}
				});
			});
			return options;
		}

		/**
		 * Return the network's color
		 */
		$scope.getColor = function (networks) {
			var tab = $scope.makeTabFromStr(networks);
			var res = "";
			tab.forEach(element => {
				var name = $scope.formatNetworkName(element);
				$scope.json.forEach(elem => {
					if (name == elem.network) {
						if (elem.type !== "international")
							res = elem.color;
						else if (name === "IGS")
							res = elem.color;
						else
							res = elem.color;
					}
				});
			});
			return res;
		}



		/********************************************************************
		 * markersHandler function handles the display of markers in the map
		 ********************************************************************/
		$scope.markersHandler = function (data) {
			var marker = '';
			var networkStr = '';
			var date_end = '';
			angular.forEach(data, function (result) {
				for (var j = 0; j < result.station_networks.length; j++) {
					networkStr = networkStr + " " + result.station_networks[j].network.name;
				}
				if (networkStr.substr(0, 1) === " ") {
					networkStr = networkStr.substr(1);
				}

				if (result.date_to === $scope.currentdate) {
					var optionMarker = $scope.getOption($scope.getNetworksNames(result.station_networks));
					marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon], optionMarker);
					date_end = "";
				} else {
					var LeafIcon = L.Icon.extend({
						options: {
							iconSize: [15, 15],
							iconAnchor: [15, 15]
						}
					});

					var mypathicon = "images/black.png";
					marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon], {
						icon: new LeafIcon({
							iconUrl: mypathicon
						})
					});
					date_end = result.date_to;
				}
				networkStr = '';
				marker.result = result;
				marker.bindPopup('<span><b>Marker:</b> ' + result.marker_long_name +
					'</span><br style="line-height:0px;"><span><b>Lat:</b> ' + result.location.coordinates.lat.toFixed(3) +
					'</span><br style="line-height:0px;"><span><b>Lon:</b> ' + result.location.coordinates.lon.toFixed(3) +
					'</span><br style="line-height:0px;"><span><b>Elev:</b> ' + result.location.coordinates.altitude.toFixed(3) +
					'</span><br style="line-height:0px;"><span><b>Install date:</b> ' + result.date_from +
					'</span><br style="line-height:0px;"><span><b>End date:</b> ' + date_end +
					'</span><br style="line-height:0px;"><span><b>Country:</b> ' + result.location.city.state.country.name +
					'</span><br style="line-height:0px;"><span><b>Network:</b> ' + $scope.getNetworkForPopup(result)
				);

				$scope.markers.addLayer(marker);
			});
			$scope.mymap.addLayer($scope.markers);
			$scope.mymap.fitBounds($scope.markers.getBounds(), {
				maxZoom: 3
			});
		};

		/*****************************************************************************************************************
		 * markersHandler_multi function handles the display of markers in the map when the result contains several markers
		 ******************************************************************************************************************/
		$scope.markersHandler_multi = function (data) {
			var marker = '';
			var networkStr = '';
			var markerarray = [];
			angular.forEach(data, function (result) {
				for (var j = 0; j < result.station_networks.length; j++) {
					networkStr = networkStr + " " + result.station_networks[j].network.name;
				}
				if (networkStr.substr(0, 1) === " ") {
					networkStr = networkStr.substr(1);
				}
				//active station 
				if (result.date_to === $scope.currentdate) {
					var optionMarker = $scope.getOption($scope.getNetworksNames(result.station_networks));

					marker = new L.circleMarker([result.location.coordinates.lat, result.location.coordinates.lon], optionMarker);

				} else { //desactive station
					var LeafIcon = L.Icon.extend({
						options: {
							iconSize: [15, 15],
							iconAnchor: [15, 15]
						}
					});
					// var mycolor = $scope.colorNetworks[networkStr].color;
					var mypathicon = "images/black.png";
					marker = new L.marker([result.location.coordinates.lat, result.location.coordinates.lon], {
						icon: new LeafIcon({
							iconUrl: mypathicon
						})
					});
				}


				marker.on('click', function (e) {
					var markerselectedflag = false;
					for (var key in $scope.markerselecteddict) {
						if ($scope.markerselecteddict[key] === result.marker) {
							markerselectedflag = true;
						}
					}
					if (markerselectedflag === true) { //if we click on marker always selected, we unselected this marker
						$scope.latitude = '';
						$scope.longitude = '';
						angular.forEach($scope.gridOptions.data, function (data, index) {
							if (data.marker === result.marker) {
								$scope.gridApi.selection.unSelectRow($scope.gridOptions.data[index]);
							}
						});
						//$scope.mymap.closePopup();
					} else { //if we click on marker no selected, we selected also in the grid
						$scope.latitude = result.location.coordinates.lat.toFixed(3);
						$scope.longitude = result.location.coordinates.lon.toFixed(3);
						angular.forEach($scope.gridOptions.data, function (data, index) {
							if (data.marker === result.marker) {
								$scope.gridApi.selection.selectRow($scope.gridOptions.data[index]); //select the row in the grid
								$scope.gridApi.grid.element[0].getElementsByClassName("ui-grid-viewport")[0].scrollTop = index * $scope.gridApi.grid.options.rowHeight; //the row selected in the grid is showed on the top of the grid
							}
						});
					}
					$scope.$apply(); //allow to make an update
				});


				networkStr = '';
				$scope.markers.addLayer(marker);
			});

			$scope.mymap.addLayer($scope.markers);
			$scope.mymap.fitBounds($scope.markers.getBounds(), {
				maxZoom: 3
			});

		};



		/************************************************************
		 * getNetworkForPopup function handles the display of networks
		 * in tooltip when the users click on marker in the map
		 ************************************************************/
		$scope.getNetworkForPopup = function (result) {
			var networkStr = '';

			for (var j = 0; j < result.station_networks.length; j++) {
				networkStr = networkStr + " " + result.station_networks[j].network.name;
			}
			if (networkStr.substr(0, 1) === " ") {
				networkStr = networkStr.substr(1);
			}
			return networkStr;

		};



		/*******************************************************************
		 * expandfunc function handles expand contact with the sign + and -
		 *******************************************************************/
		$scope.expandfunc = function (elem) {
			if ($scope.isElemShown(elem)) {
				if (elem === 'doc') {
					$scope.expanddoc = 'minus';
					$scope.shownElemdoc = null;
				}
				if (elem === 'log') {
					$scope.expandlog = 'minus';
					$scope.shownElemlog = null;
				}
				if (elem === 'net') {
					$scope.expandnet = 'minus';
					$scope.shownElemnet = null;
				}
				if (elem === 'receiver') {
					$scope.expandreceiver = 'minus';
					$scope.shownElemreceiver = null;
				}
				if (elem === 'antenna') {
					$scope.expandantenna = 'minus';
					$scope.shownElemantenna = null;
				}
				if (elem === 'radome') {
					$scope.expandradome = 'minus';
					$scope.shownElemradome = null;
				}
				if (elem === 'contact') {
					$scope.expandcontact = 'minus';
					$scope.shownElemcontact = null;
				}
			} else {
				if (elem === 'doc') {
					$scope.expanddoc = 'plus';
					$scope.shownElemdoc = elem;
				}
				if (elem === 'log') {
					$scope.expandlog = 'plus';
					$scope.shownElemlog = elem;
				}
				if (elem === 'net') {
					$scope.expandnet = 'plus';
					$scope.shownElemnet = elem;
				}
				if (elem === 'receiver') {
					$scope.expandreceiver = 'plus';
					$scope.shownElemreceiver = elem;
				}
				if (elem === 'antenna') {
					$scope.expandantenna = 'plus';
					$scope.shownElemantenna = elem;
				}
				if (elem === 'radome') {
					$scope.expandradome = 'plus';
					$scope.shownElemradome = elem;
				}
				if (elem === 'contact') {
					$scope.expandcontact = 'plus';
					$scope.shownElemcontact = elem;
				}
			}
		};

		/*******************************************************************
		 * isElemShown function handles expand contact with the sign + and -
		 *******************************************************************/
		$scope.isElemShown = function (elem) {
			if (elem === 'doc') {
				return $scope.shownElemdoc === elem;
			}
			if (elem === 'log') {
				return $scope.shownElemlog === elem;
			}
			if (elem === 'net') {
				return $scope.shownElemnet === elem;
			}
			if (elem === 'receiver') {
				return $scope.shownElemreceiver === elem;
			}
			if (elem === 'antenna') {
				return $scope.shownElemantenna === elem;
			}
			if (elem === 'radome') {
				return $scope.shownElemradome === elem;
			}
			if (elem === 'contact') {
				return $scope.shownElemcontact === elem;
			}
		};

		/****************************************************************
		 * download function handles downloaded files in different format  
		 ****************************************************************/
		$scope.download = function (filename, format) {
			if (format === 'Json') {
				var blob = new Blob([JSON.stringify($scope.results)], {
					type: 'text/json'
				});
				if (window.navigator.msSaveOrOpenBlob) { //IE
					window.navigator.msSaveBlob(blob, filename);
				} else {
					var elem = window.document.createElement('a');
					elem.href = window.URL.createObjectURL(blob);
					elem.download = filename;
					document.body.appendChild(elem);
					elem.click();
					document.body.removeChild(elem);
				}
			} else if ((format === 'CSV') || (format === 'XML') || (format === 'GeodesyML') || (format === 'SINEX') || (format === 'GAMIT')) {
				if (format === 'CSV') {
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/csv?';
				} else if (format === 'XML') {
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/stations/v2/combination/full/xml?';
				}

				var req = $resource(adress + ':myquery', {
					myquery: '@myquery'
				}, {
					query: {
						method: 'get',
						isArray: false
					}
				});
				//execute asynchronous query
				req.query({
					myquery: $routeParams.querystring
				}).$promise.then(function (data) {
					if (data !== '') {
						window.location = adress + $routeParams.querystring;
					} else {
						$scope.openmessage($scope.messageinfonodata);
					}
				});
			}
		};

		/****************************************************************
		 * downloadgeodesymlfile function allow to download geodesyml file
		 *****************************************************************/
		$scope.downloadgeodesymlfile = function () {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/geodesyml/' + $scope.marker.toUpperCase();
			window.open(adress);
		};

		$scope.downloadSitelogFile = function () {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/' + $scope.marker.toUpperCase();
			window.open(adress);
		};
		/*********************************************************************************************
		 * downloadselectedfilegeodesyml function allow to download geodesyml file selected in the grid
		 *********************************************************************************************/
		$scope.downloadselectedfilegeodesyml = function () {
			var selectedRows = $scope.gridApi.selection.getSelectedRows();
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/geodesyml/';
			var name = '';
			if (selectedRows.length > 0) {
				var i = 0;
				while (i < selectedRows.length) {
					if (i > 0) name += ",";
					name += selectedRows[i]['marker'];
					i++;
				}
				var elem = window.document.createElement('a');
				elem.href = adress + name;
				document.body.appendChild(elem);
				elem.click();
				document.body.removeChild(elem);
			} else {
				$scope.openmessage($scope.messageinfogrid);
			}
		};

		/*****************************************************************************************
		 * downloadselectedfilesitelog function allow to download sitelog file selected in the grid
		 ******************************************************************************************/
		$scope.downloadselectedfilesitelog = function () {
			var selectedRows = $scope.gridApi.selection.getSelectedRows();
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/log/';
			var name = "";
			if (selectedRows.length > 0) {
				var i = 0;
				while (i < selectedRows.length) {
					if (i > 0) name += ",";
					name += selectedRows[i]['marker'];
					i++;
				}
				var elem = window.document.createElement('a');
				elem.href = adress + name;
				document.body.appendChild(elem);
				elem.click();
				document.body.removeChild(elem);
			} else {
				$scope.openmessage($scope.messageinfogrid);
			}
		};

		/***************************************************************************
		 * exportquery function allows to users to get the query for command request
		 ***************************************************************************/
		$scope.exportquery = function (format) {
			var pyglass_request = "pyglass stations -u " + CONFIG.server;
			if ($routeParams.querystring.indexOf("site=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("site=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 5, endpos);
				pyglass_request = pyglass_request + " -sn " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("marker=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("marker=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -m " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("minLat=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("minLat=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsminlat " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("maxLat=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("maxLat=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsmaxlat " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("minLon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("minLon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsminlon " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("maxLon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("maxLon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rsmaxlon " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("centerLat=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("centerLat=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -rcclat " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("centerLon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("centerLon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -rcclon " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("radius=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("radius=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -rcr " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("polygon=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("polygon=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -ps " + param.replace(/[!,]/g, " ");
			}
			if ($routeParams.querystring.indexOf("altitude=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("altitude=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				var posseparatedparam = param.indexOf(",");
				if (posseparatedparam !== -1) {
					if (param.indexOf(",") !== 0) {
						pyglass_request = pyglass_request + " -htmin " + param.substring(0, posseparatedparam) + " -htmax " + param.substring(posseparatedparam + 1, param.length);
					} else {
						pyglass_request = pyglass_request + " -htmax " + param.substring(1, param.length);
					}
				} else {
					pyglass_request = pyglass_request + " -htmin " + param;
				}
			}
			if ($routeParams.querystring.indexOf("longitude=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("longitude=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				var posseparatedparam = param.indexOf(",");
				if (posseparatedparam !== -1) {
					if (param.indexOf(",") !== 0) {
						pyglass_request = pyglass_request + " -lgmin " + param.substring(0, posseparatedparam) + " -lgmax " + param.substring(posseparatedparam + 1, param.length);
					} else {
						pyglass_request = pyglass_request + " -lgmax " + param.substring(1, param.length);
					}
				} else {
					pyglass_request = pyglass_request + " -lgmin " + param;
				}
			}
			if ($routeParams.querystring.indexOf("latitude=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("latitude=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				var posseparatedparam = param.indexOf(",");
				if (posseparatedparam !== -1) {
					if (param.indexOf(",") !== 0) {
						pyglass_request = pyglass_request + " -ltmin " + param.substring(0, posseparatedparam) + " -ltmax " + param.substring(posseparatedparam + 1, param.length);
					} else {
						pyglass_request = pyglass_request + " -ltmax " + param.substring(1, param.length);
					}
				} else {
					pyglass_request = pyglass_request + " -ltmin " + param;
				}
			}
			if ($routeParams.querystring.indexOf("installedDateMin=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("installedDateMin=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -idmin " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("installedDateMax=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("installedDateMax=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -idmax " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("removedDateMin=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("removedDateMin=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -rdmin " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("removedDateMax=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("removedDateMax=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -rdmax " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("network=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("network=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -nw " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("invertedNetworks=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("invertedNetworks=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -inw " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("agency=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("agency=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -ag " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("receiver=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("receiver=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				pyglass_request = pyglass_request + " -rt " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("antenna=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("antenna=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -at " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("radome=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("radome=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -radt " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("country=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("country=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -co " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("state=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("state=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 6, endpos);
				pyglass_request = pyglass_request + " -pv " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("city=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("city=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 5, endpos);
				pyglass_request = pyglass_request + " -ct " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("satelliteSystem=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("satelliteSystem=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 16, endpos);
				pyglass_request = pyglass_request + " -ss " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("dateRange=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("dateRange=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -dr " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("dataAvailability=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("dataAvailability=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 17, endpos);
				pyglass_request = pyglass_request + " -da " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("fileType=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("fileType=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 9, endpos);
				pyglass_request = pyglass_request + " -ft " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("samplingFrequency=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("samplingFrequency=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 18, endpos);
				pyglass_request = pyglass_request + " -sf " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("samplingWindow=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("samplingWindow=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -sw " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("minimumObservationYears=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("minimumObservationYears=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 24, endpos);
				pyglass_request = pyglass_request + " -mo " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("statusfile=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("statusfile=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 11, endpos);
				pyglass_request = pyglass_request + " -stf " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("observationtype=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("observationtype=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 16, endpos);
				pyglass_request = pyglass_request + " -obst " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("frequency=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("frequency=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -freq " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("channel=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("channel=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 8, endpos);
				pyglass_request = pyglass_request + " -chan " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("constellation=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("constellation=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 14, endpos);
				pyglass_request = pyglass_request + " -constel " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("ratioepoch=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("ratioepoch=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 11, endpos);
				pyglass_request = pyglass_request + " -epoch " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("elevangle=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("elevangle=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 10, endpos);
				pyglass_request = pyglass_request + " -elang " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("multipathvalue=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("multipathvalue=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 15, endpos);
				pyglass_request = pyglass_request + " -multpath " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("nbcycleslips=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("nbcycleslips=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 13, endpos);
				pyglass_request = pyglass_request + " -cycslps " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("spprms=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("spprms=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 7, endpos);
				pyglass_request = pyglass_request + " -ssprms " + param.replace(/,/g, " ");
			}
			if ($routeParams.querystring.indexOf("nbclockjumps=") !== -1) {
				var startpos = $routeParams.querystring.indexOf("nbclockjumps=");
				var endpos = $routeParams.querystring.length;
				if ($routeParams.querystring.indexOf("&", startpos) !== -1) {
					endpos = $routeParams.querystring.indexOf("&", startpos);
				}
				var param = $routeParams.querystring.substring(startpos + 13, endpos);
				pyglass_request = pyglass_request + " -clkjmps " + param.replace(/,/g, " ");
			}
			if (format === 'Json') {
				$scope.currentrequest = $scope.request_json;
				$scope.pyglassrequest = pyglass_request + " -o full-json";
			} else if (format === 'Json + files') {
				$scope.currentrequest = $scope.request_json_files;
				$scope.pyglassrequest = pyglass_request + " -o full-json+files";
			} else if (format === 'GeodesyML') {
				$scope.currentrequest = $scope.request_geodesyml;
				$scope.pyglassrequest = pyglass_request + " -o full-geodesyml";
			} else if (format === 'GeodesyML + files') {
				$scope.currentrequest = $scope.request_geodesyml_files;
				$scope.pyglassrequest = pyglass_request + " -o full-geodesyml+files";
			} else if (format === 'File list') {
				$scope.currentrequest = $scope.request_files_list;
				$scope.pyglassrequest = pyglass_request + " -o full-filelist";
			} else if (format === 'SINEX') {
				$scope.currentrequest = $scope.request_sinex;
				$scope.pyglassrequest = pyglass_request + " -o full-sinex";
			} else if (format === 'GAMIT') {
				$scope.currentrequest = $scope.request_gamit;
				$scope.pyglassrequest = pyglass_request + " -o full-gamit";
			} else if (format === 'CSV') {
				$scope.currentrequest = $scope.request_csv;
				$scope.pyglassrequest = pyglass_request + " -o full-csv";
			} else if (format === 'KML') {
				$scope.currentrequest = $scope.request_kml;
				$scope.pyglassrequest = pyglass_request + " -o full-kml";
			}
			$scope.opendialog();
		};

		/******************************************************************************************
		 * opendialog function allows to open a modal when the users click on "request query" button 
		 *******************************************************************************************/
		$scope.opendialog = function () {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/modalRequest.html',
				size: 'lg',
				scope: $scope,
				windowClass: 'center-modal'
			});
		};
		
		/******************************************************************************************
		 * opennodownloadmonth function allows to open a modal when the users click on 
		 * "Download selected dates" button 
		 *******************************************************************************************/

		$scope.opennodownloadmonth = function() {
			var selectedDates = $scope.dateselectedarray;
			var clickdatemodal = "";
			for (var i = selectedDates.length-1; i >= 0; i--) {
				console.log("bloucle: " + i);
				clickdatemodal = $scope.convertDateFormat(selectedDates[i]) + " 00:00:00";
				clickdatemodal.toString();
				console.log(clickdatemodal, selectedDates, $scope.datesonmonth);
				
				var datemap = $scope.datesonmonth.map(el => el.reference_date);
				var include = datemap.includes(clickdatemodal);
				console.log(include);
				if (include == false) {
					selectedDates.splice(i,1);
				}
				console.log(selectedDates);
				
			}

			if (selectedDates.length > 0) {
				return $scope.modalInstance = $uibModal.open({
					templateUrl: 'views/modalNoDownloadMonth.html',
					size: 'lg',
					scope: $scope,
					windowClass: 'center-modal'
				});
			} else {
				$scope.openmessage($scope.messageinfocalendar);
			}
			
		};
		

		/***************************************
		 * cancel function allows to cancel modal
		 ****************************************/
		$scope.cancel = function () {
			$scope.modalInstance.dismiss("cancel");
		};

		/*********************************************************************************
		 * getlastequipment function allows to get the last receivers, antennas and radomes
		 *********************************************************************************/
		$scope.getlastequipment = function () {
			var lastreceiver = '';
			var lastantenna = '';
			var lastradome = '';
			for (var i = 0; i < $scope.results.length; i++) {
				for (var j = 0; j < $scope.results[i].station_items.length; j++) {
					if ($scope.results[i].station_items[j].item.item_type.name === "antenna") {
						//if($scope.results[i].station_items[j].item.item_attributes[0].date_to==='9999-12-30 00:00:00')
						if ($scope.results[i].station_items[j].date_to === null || lastantenna === '' || (lastantenna !== null && lastantenna == ! '' && lastantenna < Date.parse($scope.results[i].station_items[j].date_to))) {
							lastantenna = Date.parse($scope.results[i].station_items[j].date_to);
							for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
								if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "antenna_type") {
									$scope.results[i]["lastantenna"] = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								}
							}
						}
					}
					lastantenna = '';
					if ($scope.results[i].station_items[j].item.item_type.name === "receiver") {
						//if($scope.results[i].station_items[j].item.item_attributes[0].date_to==='9999-12-30 00:00:00')
						if ($scope.results[i].station_items[j].date_to === null || lastreceiver === '' || (lastreceiver !== null && lastreceiver == ! '' && lastreceiver < Date.parse($scope.results[i].station_items[j].date_to))) {
							for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
								if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "receiver_type") {
									$scope.results[i]["lastreceiver"] = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								}
							}
						}
					}
					if ($scope.results[i].station_items[j].item.item_type.name === "radome") {
						//if($scope.results[i].station_items[j].item.item_attributes[0].date_to==='9999-12-30 00:00:00')
						if ($scope.results[i].station_items[j].date_to === null || lastradome === '' || (lastradome !== null && lastradome == ! '' && lastradome < Date.parse($scope.results[i].station_items[j].date_to))) {
							for (var k = 0; k < $scope.results[i].station_items[j].item.item_attributes.length; k++) {
								if ($scope.results[i].station_items[j].item.item_attributes[k].attribute.name === "radome_type") {
									$scope.results[i]["lastradome"] = $scope.results[i].station_items[j].item.item_attributes[k].value_varchar;
								}
							}
						}
					}
				}
			}
			return $scope.results;
		};

		/***************************************************************
		 * getNetwork function handles the display of networks
		 * in rows of table summary station
		 ***************************************************************/
		$scope.getNetwork = function (data) {
			var networkStr = '';
			var dicttemp = {};
			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < data[i].station_networks.length; j++) {
					networkStr = networkStr + " " + data[i].station_networks[j].network.name;
				}
				if (networkStr.substr(0, 1) === " ") {
					networkStr = networkStr.substr(1);
				}
				dicttemp[data[i].marker] = networkStr;
				var networkStr = '';
			}
			return dicttemp;
		};

		/*********************************************************************
		 * metadataDetail function handles on click links to access metadata
		 **********************************************************************/
		$scope.metadataDetail = function (code) {
			var adress = CONFIG.server + CONFIG.glassapi + "/webresources/stations/v2/combination/full/json?";
			var param = "marker=";
			var request_json = adress + param + code;
			/*$scope.request_json_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-jsonplusfiles/" + $routeParams.querystring;
	$scope.request_geodesyml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesyml/" + $routeParams.querystring;
	$scope.request_geodesyml_files = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-geodesymlplusfiles/" + $routeParams.querystring;
	$scope.request_files_list = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-fileslist/" + $routeParams.querystring;
	$scope.request_sinex = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-sinex/" + $routeParams.querystring;
	$scope.request_gamit = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-gamit/" + $routeParams.querystring;
	$scope.request_csv = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-csv/" + $routeParams.querystring;
	$scope.request_kml = CONFIG.server + CONFIG.glassapi + "/webresources/stations/combination-kml/" + $routeParams.querystring;*/
			var mypass = $window.open('#/metadata/' + param + code, '_blank');
		};

		/************************************************
		 * messageinfocalendar variable to calendar error message
		 ************************************************/
		 $scope.messageinfocalendar = {
			textAlert: "You must select at least one date in the calendar or a date with a value",
			mode: 'info'
		};


		/************************************************
		 * messageinfogrid variable to grid error message
		 ************************************************/
		$scope.messageinfogrid = {
			textAlert: "You have to select at least one thing in grid or in lisboxes",
			mode: 'info'
		};
		/*******************************************************
		 * messageinfonodata variable to advanced error message
		 ********************************************************/
		$scope.messageinfonodata = {
			textAlert: "No data available",
			mode: 'info'
		};
		/*******************************************************************
		 * openmessage function allows to open a modal when there's an error
		 *******************************************************************/
		$scope.openmessage = function (message) {
			return $scope.modalInstance = $uibModal.open({
				templateUrl: 'views/messageBox.html',
				scope: $scope,
				size: 'lg',
				resolve: {
					data: function () {
						$scope.messageinfo = message;
						return $scope.messageinfo;
					}
				}
			});
		};
		/**************************************
		 * close function allows to close modal
		 ***************************************/
		$scope.close = function () {
			$scope.modalInstance.close();
		};

		/***************************************************
		 * selectText function allows to select text in modal
		 ***************************************************/
		$scope.selectText = function (containerid) {
			if (document.selection) {
				var range = document.body.createTextRange();
				range.moveToElementText(document.getElementById(containerid));
				range.select();
				document.execCommand("Copy");
			} else if (window.getSelection) {
				var range = document.createRange();
				range.selectNode(document.getElementById(containerid));
				window.getSelection().removeAllRanges();
				window.getSelection().addRange(range);
				document.execCommand("Copy");
			}

		};

		/*****************************************
		 * gridHandler function handles grid part
		 ******************************************/
		$scope.gridHandler = function (data) {
			var defaultcolor = '';
			var screenwidth = screen.width;
			//define all grid options and their columns
			$scope.gridOptions = {
				enableGridMenu: true,
				enableFiltering: false,
				enableSorting: true,
				enableRowSelection: true,
				enableSelectAll: true,
				multiSelect: true,
				exporterMenuCsv: true,
				exporterMenuPdf: false,
				enableColumnMoving: true,
				enableColumnResizing: true,
				showGridFooter: true,
				//all function regarding the ui-grid
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					$scope.idGrid = [];
					//function allows to get the row selected in the grid and display the corresponding marker into the map with a circle bigger
					$scope.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
						if (row.entity.date_to === $scope.currentdate) {
							$scope.mymap.eachLayer(function (layer) {
								if (layer.hasOwnProperty("_latlng")) {
									if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {
										var defaultoption = $scope.getOption($scope.getNetworksNames(row.entity.station_networks));
										var mylayerid = layer._leaflet_id;
										console.log(mylayerid);

										if (row.isSelected) {
											$scope.idGrid.push(mylayerid);
											var highlight = {
												radius: 12,
												fillColor: "#2CDBF6",
												color: "#000",
												weight: 1,
												opacity: 1,
												fillOpacity: 0.8
											};
											$scope.mymap._layers[mylayerid].setStyle(highlight);
											$scope.mymap._layers[mylayerid].bringToFront();
											$scope.markerselecteddict[mylayerid] = row.entity.marker; //store the marker in the dictionnary to use later
											var y = $scope.searchkey(row.entity.marker_long_name, markerchartdict);
											console.log(row.entity.marker_long_name)
											mainchart.yAxis[0].addPlotBand({
												from: parseInt(y) - 0.25,
												to: parseInt(y) + 0.25,
												id: row.entity.marker_long_name
											});
											$scope.station_timeseries_uga = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/UGA-CNRS/' + $scope.markerselecteddict[mylayerid].toUpperCase();
											$scope.station_timeseries_ltk = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/LTK/' + $scope.markerselecteddict[mylayerid].toUpperCase();
											$scope.station_timeseries_ingv = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/INGV/' + $scope.markerselecteddict[mylayerid].toUpperCase();
										} else {
											$scope.mymap._layers[mylayerid].setStyle(defaultoption);
											delete $scope.markerselecteddict[mylayerid];
											mainchart.yAxis[0].removePlotBand(row.entity.marker_long_name);

											$scope.idGrid.splice($scope.idGrid.indexOf(mylayerid));
											if ($scope.idGrid.length !== 0) {
												mylayerid = $scope.idGrid[$scope.idGrid.length - 1];
												$scope.station_timeseries_uga = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/UGA-CNRS/' + $scope.markerselecteddict[mylayerid].toUpperCase();
												$scope.station_timeseries_ltk = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/LTK/' + $scope.markerselecteddict[mylayerid].toUpperCase();
												$scope.station_timeseries_ingv = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/INGV/' + $scope.markerselecteddict[mylayerid].toUpperCase();
											}
										}
									}
								}
							});
						}

					});
					//function allows to get several row selected in the grid and display them into the map with a circle bigger
					$scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
						angular.forEach(rows, function (row, key) {
							if (row.entity.date_to === $scope.currentdate) {
								$scope.mymap.eachLayer(function (layer) {
									if (layer.hasOwnProperty("_latlng")) {
										if ((row.entity.location.coordinates.lat === layer._latlng.lat) && (row.entity.location.coordinates.lon === layer._latlng.lng)) {

											var defaultoption = $scope.getOption($scope.getNetworksNames(row.entity.station_networks));
											var mylayerid = layer._leaflet_id;
											if (row.isSelected) {
												var highlight = {
													radius: 12,
													fillColor: "#2CDBF6",
													color: "#000",
													weight: 1,
													opacity: 1,
													fillOpacity: 0.8
												};
												$scope.mymap._layers[mylayerid].setStyle(highlight);
												$scope.mymap._layers[mylayerid].bringToFront();
												$scope.markerselecteddict[mylayerid] = row.entity.marker;

												var y = $scope.searchkey(row.entity.marker_long_name, markerchartdict);
												mainchart.yAxis[0].addPlotBand({
													from: parseInt(y) - 0.25,
													to: parseInt(y) + 0.25,
													id: row.entity.marker_long_name
												});
											} else {
												$scope.mymap._layers[mylayerid].setStyle(defaultoption);
												delete $scope.markerselecteddict[mylayerid];
												mainchart.yAxis[0].removePlotBand(row.entity.marker);
											}
										}
									}
								});
							}
						});
					});

					$scope.gridApi.core.on.sortChanged($scope, function (grid) {
						var rows = grid.sortByColumn(grid.rows);
						var tab = [];
						var k = 0;
						rows.forEach(row => {
							for (var i = 0; i < markerchartarray.length; i++) {
								if (row.entity.marker_long_name === markerchartdict[markerchartarray[i]]) {
									tab[k] = i;
									k++;
								}
							}
						});
						order = tab;
						$scope.temporalcharts();
					})
				},
				//definition of each column in the grid
				columnDefs: [{
					name: 'Marker',
					field: 'marker_long_name',
					enableColumnMenu: false,
					//sort: { direction: uiGridConstants.ASC, priority: 1 },
					cellTemplate: '<div class="ui-grid-cell-contents"><a id="gridlink" ng-click="grid.appScope.metadataDetail(row.entity.marker)">{{ COL_FIELD }}</a></div>'
				},
				{
					name: 'Site name',
					field: 'name',
					enableColumnMenu: false
				},
				{
					name: 'Install Date',
					field: 'date_from',
					enableColumnMenu: false
				},
				{
					name: 'End Date',
					field: 'date_to',
					enableColumnMenu: false,
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						if (grid.getCellValue(row, col) == $scope.currentdate) {
							return 'white';
						}
					}
				},
				{
					name: 'Lat',
					field: 'location.coordinates.lat',
					type: 'number',
					cellFilter: 'number: 3',
					enableColumnMenu: false
				},
				{
					name: 'Lon',
					field: 'location.coordinates.lon',
					type: 'number',
					cellFilter: 'number: 3',
					enableColumnMenu: false
				},
				{
					name: 'Elev',
					field: 'location.coordinates.altitude',
					type: 'number',
					cellFilter: 'number: 3',
					enableColumnMenu: false
				},
				{
					name: 'Network',
					field: 'getNetwork()',
					enableColumnMenu: false
				},
				{
					name: 'Country',
					field: 'location.city.state.country.name',
					enableColumnMenu: false
				},
				{
					name: 'Last receiver',
					field: 'lastreceiver',
					enableColumnMenu: false
				},
				{
					name: 'Last antenna',
					field: 'lastantenna',
					enableColumnMenu: false
				},
				{
					name: 'Last radome',
					field: 'lastradome',
					enableColumnMenu: false
				},
				{
					name: 'Data availability',
					field: 'data_percent',
					enableColumnMenu: false
				}
				],
				data: data,
				rowTemplate: '<div /*ng-mouseover="grid.appScope.onRowHover(row);"*/ ng-mouseleave="grid.appScope.onRowLeave(row);"><div ng-repeat="col in colContainer.renderedColumns track by col.colDef.name"  class="ui-grid-cell" ui-grid-cell></div></div>'
			};
		};

		/****************************************************************
		 * onRowHover function handles the mouse over the row of grid
		 ****************************************************************/
		/*$scope.onRowHover = function (row) {
			$scope.station_timeseries_uga = '../images/ajax-loader.gif'
			$scope.station_timeseries_ltk = '../images/ajax-loader.gif'
			$scope.station_timeseries_ingv = '../images/ajax-loader.gif'

			setTimeout(() => {
					$scope.station_timeseries_uga = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/UGA-CNRS/' + row.entity.marker.toUpperCase();
					$scope.station_timeseries_ltk = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/LTK/' + row.entity.marker.toUpperCase();
					$scope.station_timeseries_ingv = 'https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/time-series-plot/INGV/' + row.entity.marker.toUpperCase();
				},10
			);
		};*/

		/***********************************************************************************
		 * fillmarkerdict function allows to create a dictionary markers to use in highcharts
		 ************************************************************************************/
		$scope.fillmarkerdict = function (data) {
			var dicttemp = {};
			var cpt = 0;
			angular.forEach(data, function (result) {
				dicttemp[cpt] = result.marker_long_name;
				markerchartarray.push(cpt);
				cpt = cpt + 1;
			});
			order = markerchartarray;
			return dicttemp;
		};

		/**************************************************************
		 * searchkey function allows to find the key of dictionary value
		 **************************************************************/
		$scope.searchkey = function (val, array) {
			for (var key in array) {
				var this_val = array[key];
				if (this_val == val) {
					return key;
					break;
				}
			}
		};
		/*************************************************************************
		 * convertDateFormat function allows to convert a date in format yyyy-mm-dd
		 **************************************************************************/
		$scope.convertDateFormat = function (date) {
			var mydate = new Date(date);
			var dd = mydate.getDate();
			var mm = mydate.getMonth() + 1;
			var yyyy = mydate.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}

			return mydate = yyyy + '-' + mm + '-' + dd;
		};
		/**************************************************************************************
		 * dateToNum function allows to convert a date in format yyyy-mm-dd to a number yyyymmdd
		 **************************************************************************************/
		$scope.dateToNum = function (d) {
			d = d.substring(0, 10);
			d = d.split("-");
			return Number(d[0] + d[1] + d[2]);
		};

		/**
		 * return the position of the given stations in grid
		 * @param {String} name 
		 */
		$scope.getPosition = function (name) {
			for (var i = 0; i < order.length; i++) {
				if (name === markerchartdict[order[i]]) {
					return i;
				}
			}
		}
		/********************************************************************************
		 * temporalcharts function allow to display the temporal chart for several station
		 ********************************************************************************/
		$scope.temporalcharts = function () {
			var defaultcolor = '';
			var globalmarker = '';
			var hasPlotBand = false;
			$scope.series_stations = [];
			var data_availability_percent = {};
			var firstDateRange = '';
			var lastDateRange = '';
			var titleRange = 'Data availability between station installation date and current date';
			var fromdateofrange = "";
			var endateofrange = "";
			//prepare the data for the timeseries
			for (var i = 0; i < $scope.results.length; i++) {
				var firstDate = $scope.results[i].date_from;
				firstDate = firstDate.substring(0, 10);
				firstDateRange = firstDate;
				firstDate = Date.UTC(firstDate.substring(0, 4), firstDate.substring(5, 7) - 1, firstDate.substring(8, 10));
				var lastDate = "";
				if ($scope.results[i].date_to === null) {
					lastDate = $scope.currentDateFormat();
					lastDate = lastDate.substring(0, 10);
					lastDateRange = lastDate;
					lastDate = Date.UTC(lastDate.substring(0, 4), lastDate.substring(5, 7) - 1, lastDate.substring(8, 10));
				} else {
					lastDate = $scope.results[i].date_to;
					lastDate = lastDate.substring(0, 10);
					lastDateRange = lastDate;
					lastDate = Date.UTC(lastDate.substring(0, 4), lastDate.substring(5, 7) - 1, lastDate.substring(8, 10));
				}
				if ($scope.results[i].rinex_files_dates.length > 0) {
					var arraydate = $scope.results[i].rinex_files_dates[0].split(",");
					var reference_dates = [];
					var new_station_object = {};
					var new_ref_date = [];
					new_station_object['name'] = markerchartdict[i];
					new_station_object['keys'] = ['x', 'y', 'name'];
					var position = $scope.getPosition(markerchartdict[i]);
					for (var j = 0; j < arraydate.length; j++) {
						if (j % 2 == 0) {
							new_ref_date = [];
							new_ref_date.push(Date.UTC(arraydate[j].slice(0, 4), arraydate[j].slice(5, 7) - 1, arraydate[j].slice(8, 10))); //month start 0-11
							new_ref_date.push(position);
							new_ref_date.push('begindate');
							reference_dates.push(new_ref_date);
						} else {
							new_ref_date = [];
							new_ref_date.push(Date.UTC(arraydate[j].slice(0, 4), arraydate[j].slice(5, 7) - 1, arraydate[j].slice(8, 10), 11, 59, 59)); //Date.UTC(year,month-1,day) start at 12h00
							new_ref_date.push(position);
							new_ref_date.push('enddate');
							reference_dates.push(new_ref_date);
							new_ref_date = [];
							new_ref_date.push(Date.UTC(arraydate[j].slice(0, 4), arraydate[j].slice(5, 7) - 1, arraydate[j].slice(8, 10), 12));
							new_ref_date.push(null);
							reference_dates.push(new_ref_date);
						}
					}
					new_station_object['data'] = reference_dates;
					$scope.series_stations.push(new_station_object);

					//calcul data percent
					var nbFiles = 0;
					var listDate = $scope.results[i].rinex_files_dates[0].split(",");
					for (var k = 0; k < listDate.length; k += 2) {
						var dt = new Date(listDate[k].slice(0, 10));
						var end = new Date(listDate[k + 1].slice(0, 10));
						while (dt <= end) {
							nbFiles++;
							dt.setUTCDate(dt.getUTCDate() + 1);
						}
					}
					var dateDiff = lastDate - firstDate;
					var totalDays = dateDiff / (1000 * 3600 * 24);
					data_availability_percent[i] = ((nbFiles * 100) / totalDays).toFixed(2) + ' % ';
					$scope.results[i].data_percent = data_availability_percent[i];
				}
			}
			//Plot the chart
			Highcharts.chart('temporalcontainer', {
				chart: {
					type: 'line',
					zoomType: 'xy',
					events: {
						load: function () {
							mainchart = this; // `this` is the reference to the chart
						}
					}
				},
				title: {
					text: titleRange
				},
				legend: {
					enabled: false
				},
				xAxis: {
					type: 'datetime',
					labels: {
						format: '{value:%e %b %Y}'
					}
				},
				yAxis: [{
					min: 0,
					max: 7,
					categories: order,
					reversed: true,
					tickPositions: order,
					gridLineWidth: 0,
					scrollbar: { enabled: true },
					dataGrouping: { enabled: false },
					showLastLabel: true,
					title: { text: '' },
					labels: {
						formatter: function () {
							var value = markerchartdict[this.value];
							return value !== 'undefined' ? value : this.value;
						}

					}
				},
				{
					opposite: true,
					linkedTo: 0,
					categories: order,
					gridLineWidth: 0,
					tickPositions: order,
					title: {
						text: ''
					},
					showLastLabel: true,
					labels: {
						enabled: true,
						formatter: function () {
							var value = data_availability_percent[this.value];
							if (value === undefined) {
								value = "";
							}
							var tooltiptext = "Percent of data between install date and current date";
							return '<span title="' + tooltiptext + '">' + value + '</span>';
						},
						useHTML: true
					}
				}
				],

				plotOptions: {
					series: {
						cursor: 'pointer',
						marker: {
							enabled: true,
							symbol: "square"
						},
						events: {
							click: function (event) {
								globalmarker = this.name;
								var y = $scope.searchkey(globalmarker, markerchartdict);
								if (!hasPlotBand) {
									mainchart.yAxis[0].addPlotBand({
										from: parseInt(y) - 0.25,
										to: parseInt(y) + 0.25,
										id: globalmarker
									});
									angular.forEach($scope.gridOptions.data, function (data, index) {
										if (data.marker_long_name === globalmarker) {
											$scope.gridApi.selection.selectRow($scope.gridOptions.data[index]);
											$scope.gridApi.grid.element[0].getElementsByClassName("ui-grid-viewport")[0].scrollTop = index * $scope.gridApi.grid.options.rowHeight;
										}
									});
									$scope.$apply();
								} else {
									mainchart.yAxis[0].removePlotBand(globalmarker);
									angular.forEach($scope.gridOptions.data, function (data, index) {
										if (data.marker_long_name === globalmarker) {
											$scope.gridApi.selection.unSelectRow($scope.gridOptions.data[index]);
										}
									});
									$scope.$apply();
								}
								hasPlotBand = !hasPlotBand;
							}
						}
					}
				},
				tooltip: {
					formatter: function () {
						var tooltip;
						if (this.key == 'begindate') {
							tooltip = '<b>Start date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
						} else if (this.key == 'enddate') {
							tooltip = '<b>End date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
						}
						return tooltip;
					}
				},
				series: $scope.series_stations
			});
			$scope.modalInstance.opened.then(function () {
				$scope.loading = false;
				$scope.modalInstance.close(); //stop the spinner loading
			});
		};

		/****************************************************************************************
		 * initdataavailable function allow to init the temporal chart for one station and heatmap
		 ****************************************************************************************/
		$scope.initdataavailable = function (data) {
			var arraydate = [];
			var results = [];
			var new_station_object = {};
			var listDate = data[0].rinex_files_dates[0].split(",");
			for (var i = 0; i < listDate.length; i += 2) {
				var dt = new Date(listDate[i].slice(0, 10));
				var end = new Date(listDate[i + 1].slice(0, 10));
				while (dt <= end) {
					arraydate.push(dt.toISOString().slice(0, 10));
					dt.setUTCDate(dt.getUTCDate() + 1);
				}
			}
			new_station_object['marker'] = data[0].marker;
			new_station_object['date_from'] = data[0].date_from;
			new_station_object['date_to'] = data[0].date_to;
			new_station_object['rinex_files_dates'] = arraydate;
			results.push(new_station_object);
			$scope.temporalchartforone(results, '');
			$scope.heatmap(results);
		};
		/*********************************************************************************
		 * temporalchartforone function allow to display the temporal chart for one station
		 *********************************************************************************/
		$scope.temporalchartforone = function (data, params) {
			var reference_dates = [];
			var fromdateofrange = "";
			var endateofrange = "";
			if (data[0].rinex_files_dates.length > 0) {
				var arraydate = data[0].rinex_files_dates;
				$scope.filefromdateselected = data[0].date_from.substring(0, 10);
				var firstDate = data[0].date_from.substring(0, 10);
				firstDate = Date.UTC(firstDate.substring(0, 4), firstDate.substring(5, 7) - 1, firstDate.substring(8, 10));
				var lastDate = "";
				if (data[0].date_to === null) {
					lastDate = $scope.currentDateFormat();
					lastDate = lastDate.substring(0, 10);
					$scope.filetodateselected = lastDate;
					lastDate = Date.UTC(lastDate.substring(0, 4), lastDate.substring(5, 7) - 1, lastDate.substring(8, 10));
				} else {
					lastDate = data[0].date_to;
					lastDate = lastDate.substring(0, 10);
					$scope.filetodateselected = lastDate;
					lastDate = Date.UTC(lastDate.substring(0, 4), lastDate.substring(5, 7) - 1, lastDate.substring(8, 10));
				}

				var data_availability_percent = {};

				for (var j = 0; j < arraydate.length; j++) {
					var year = arraydate[j].substring(0, 4);
					var month = arraydate[j].substring(5, 7);
					var day = arraydate[j].substring(8, 10);
					if (j == 0) {
						var new_ref_date = [];
						new_ref_date.push(Date.UTC(year, month - 1, day)); //month start 0-11
						new_ref_date.push(1);
						new_ref_date.push('begindate');
						reference_dates.push(new_ref_date);

					} else {
						var myday = Date.UTC(year, month - 1, day);

						if ((myday - precday) > (24 * 60 * 60 * 1000)) {
							var date_end = precday + (12 * 60 * 60 * 1000); //12 because Date.UTC(year,month-1,day) start at 12h00
							var new_ref_date = [];
							new_ref_date.push(date_end);
							new_ref_date.push(1);
							new_ref_date.push('enddate');
							reference_dates.push(new_ref_date);
							new_ref_date = [];
							new_ref_date.push(date_end + (60 * 1000));
							new_ref_date.push(null);
							reference_dates.push(new_ref_date);
							var date_start = myday;
							new_ref_date = [];
							new_ref_date.push(myday);
							new_ref_date.push(1);
							new_ref_date.push('begindate');
							reference_dates.push(new_ref_date);
						}
					}
					var precday = Date.UTC(year, month - 1, day);
					if (j == arraydate.length - 1) //case end of list
					{
						var new_ref_date = [];
						var year = arraydate[j].substring(0, 4);
						var month = arraydate[j].substring(5, 7);
						var day = arraydate[j].substring(8, 10);
						var date_end = Date.UTC(year, month - 1, day);
						new_ref_date.push(date_end);
						new_ref_date.push(1);
						new_ref_date.push('enddate');
						reference_dates.push(new_ref_date);
					}

				}
				//calcul data percent
				if (params.indexOf("date_range=") !== -1) {
					var startpos = params.indexOf("date_range=");
					var endpos = "";
					if (params.indexOf("&", startpos) !== -1) {
						endpos = params.indexOf("&", startpos);
					} else {
						endpos = params.length;
					}
					var param = params.substring(startpos + 11, endpos);
					if ((param.substring(0, 1) != ",") && (param.length != 21)) //case only from date
					{
						fromdateofrange = param.substring(0, 10);
						endateofrange = "";
					} else if (param.substring(0, 1) == ",") //case only end date
					{
						fromdateofrange = "";
						endateofrange = param.substring(1, 11);
					} else if (param.length == 21) // case from and end dates
					{
						fromdateofrange = param.substring(0, 10);
						endateofrange = param.substring(11, 21);
					}
					if (fromdateofrange != "") {
						firstDate = Date.UTC(fromdateofrange.substring(0, 4), fromdateofrange.substring(5, 7) - 1, fromdateofrange.substring(8, 10));
						$scope.filefromdateselected = fromdateofrange;
					}
					if (endateofrange != "") {
						lastDate = Date.UTC(endateofrange.substring(0, 4), endateofrange.substring(5, 7) - 1, endateofrange.substring(8, 10));
						$scope.filetodateselected = endateofrange;
					}
				}

				var dateDiff = lastDate - firstDate;
				var totalDays = dateDiff / (1000 * 3600 * 24) + 1;
				data_availability_percent[1] = ((arraydate.length * 100) / totalDays).toFixed(2) + ' % ';

				//Plot the chart
				Highcharts.chart('temporalplaceholder', {

					chart: {
						type: 'line',
						zoomType: 'xy'
					},
					title: {
						text: ''
					},
					legend: {
						enabled: false
					},
					xAxis: {

						type: 'datetime',
						labels: {
							format: '{value:%e %b %Y}'
						}
					},
					yAxis: [{
						scrollbar: {
							enabled: false
						},
						title: {
							text: ''
						},
						labels: {
							formatter: function () {
								var value = data[0].marker;
								return value !== 'undefined' ? value : this.value;
							}

						}
					},
					{
						opposite: true,
						linkedTo: 0,
						title: {
							text: ''
						},
						labels: {
							enabled: true,
							formatter: function () {
								var value = data_availability_percent[this.value];
								if (value === undefined) {
									value = "";
								}
								var tooltiptext = "Percent of data between " + $scope.filefromdateselected + " and " + $scope.filetodateselected;
								return '<span title="' + tooltiptext + '">' + value + '</span>';
							},
							useHTML: true
						}
					}
					],

					plotOptions: {
						series: {
							marker: {
								enabled: true,
								symbol: "square"
							}
						}
					},
					tooltip: {
						formatter: function () {
							var tooltip;
							if (this.key == 'begindate') {
								tooltip = '<b>Start date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
							} else if (this.key == 'enddate') {
								tooltip = '<b>End date: </b> ' + Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/><b>Marker: </b> ' + this.series.name;
							}
							return tooltip;
						}
					},
					series: [{
						name: data[0].marker,
						keys: ['x', 'y', 'name'],
						data: reference_dates
					}]

				});

			}
		};

		/************************************************************************************
		 * heatmap function allow to display the heatmap of file availability for one station
		 ************************************************************************************/
		$scope.heatmap = function (data) {
			var percent_of_data = [];
			var list_of_years = [];
			var year_indice = 0;
			var posnextmonth = 0;
			var cpt_day = 0;
			var lastdayofmonth = '';
			var startpos = 0;
			var pos = 0;
			if (data[0].rinex_files_dates.length > 0) {
				//prepare the data for the timeseries
				var arraydate = data[0].rinex_files_dates;
				while (posnextmonth < arraydate.length) {
					var percent_of_month = 0;
					var total_day = 0;
					var month_number = 0;
					var year = arraydate[posnextmonth].substring(0, 4);
					var month = arraydate[posnextmonth].substring(5, 7);
					var day = arraydate[posnextmonth].substring(8, 10);
					//calcul data percent
					switch (month) {
						case '01':
							total_day = 31;
							month_number = 0;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '02':
							if (((parseInt(year) % 4 == 0) && (parseInt(year) % 100 != 0)) || (parseInt(year) % 400 == 0)) {
								total_day = 29;
							} else {
								total_day = 28;
							}
							month_number = 1;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '03':
							total_day = 31;
							month_number = 2;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '04':
							total_day = 30;
							month_number = 3;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '05':
							total_day = 31;
							month_number = 4;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '06':
							total_day = 30;
							month_number = 5;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '07':
							total_day = 31;
							month_number = 6;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '08':
							total_day = 31;
							month_number = 7;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '09':
							total_day = 30;
							month_number = 8;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '10':
							total_day = 31;
							month_number = 9;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '11':
							total_day = 30;
							month_number = 10;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
						case '12':
							total_day = 31;
							month_number = 11;
							lastdayofmonth = year + "/" + month + "/" + total_day;
							break;
					}
					while (pos < arraydate.length) {
						var iteryear = arraydate[pos].substring(0, 4);
						var itermonth = arraydate[pos].substring(5, 7);
						if (month == itermonth && year == iteryear) {
							cpt_day = cpt_day + 1;
						} else {
							break;
						}
						pos = pos + 1;
					}

					percent_of_month = (((cpt_day) * 100) / total_day).toFixed(2);
					if (list_of_years.length == 0) {
						list_of_years.push(year);
					} else if (list_of_years[list_of_years.length - 1] != year) {
						list_of_years.push(year);
						year_indice = year_indice + 1;
					}
					var new_ref_date = [];
					new_ref_date.push(month_number);
					new_ref_date.push(year_indice);
					new_ref_date.push(percent_of_month);
					percent_of_data.push(new_ref_date);
					cpt_day = 0;
					total_day = 0;
					percent_of_month = 0;

					posnextmonth = pos;
				}
				if (list_of_years[0] % 2 == 0 && list_of_years[list_of_years[list_of_years.length - 1]] % 2 != 0 || list_of_years[0] % 2 != 0 && list_of_years[list_of_years[list_of_years.length - 1]] % 2 == 0) {
					var dateToAdd = parseInt(list_of_years[list_of_years.length -1]) + 1;
					list_of_years.push(dateToAdd.toString());
				}
				//Plot the heatmap
				Highcharts.chart('heatmapplaceholder', {
					chart: {
						type: 'heatmap',
						marginTop: 50,
						marginBottom: 80,
						plotBorderWidth: 1
					},
					title: {
						text: 'Percent of data availability in the month'
					},
					xAxis: {
						categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
					},
					yAxis: {
						categories: list_of_years,
						title: {
							text: ''
						}
					},
					colorAxis: {
						min: 0,
						max: 100,
						stops: [
							[0, '#FFFFFF'],
							[0.2, '#F30000'],
							[0.5, '#ffff00'],
							[1, '#5DBA76']
						],
						minColor: '#ffff00',
						maxColor: '#ff0000'
					},
					legend: {
						align: 'center',
						verticalAlign: 'bottom',
						floating: true,
						x: 0,
						y: 0
					},
					tooltip: {
						formatter: function () {
							return '<b>Date : ' + this.series.xAxis.categories[this.point.x] + ' ' + this.series.yAxis.categories[this.point.y] + '</b><br><b>' +
								this.point.value + '</b> %';
						}
					},
					plotOptions: {
						series: {
							events: {
								click: function (event) {
									$scope.detailflag = true;
									$timeout(function () {
										$scope.calendarheatmap(event.point.series.xAxis.categories[event.point.x], event.point.series.yAxis.categories[event.point.y]);
									}, 100);

								}
							}
						}
					},
					series: [{
						name: data[0].marker,
						borderWidth: 1,
						data: percent_of_data

					}]
				});
			}
		};


		/*****************************************************************************
		 * calendarheatmap function allow to display the calendar of the month selected
		 *****************************************************************************/
		$scope.calendarheatmap = function (month, year) {
			var month_number;
			var month_str;
			var total_day;
			var data_date = {};
			switch (month) {
				case 'Jan':
					month_number = 0;
					month_str = '01';
					total_day = 31;
					break;
				case 'Feb':
					month_number = 1;
					month_str = '02';
					if (((parseInt(year) % 4 == 0) && (parseInt(year) % 100 != 0)) || (parseInt(year) % 400 == 0)) {
						total_day = 29;
					} else {
						total_day = 28;
					}
					break;
				case 'Mar':
					month_number = 2;
					month_str = '03';
					total_day = 31;
					break;
				case 'Apr':
					month_number = 3;
					month_str = '04';
					total_day = 30;
					break;
				case 'May':
					month_number = 4;
					month_str = '05';
					total_day = 31;
					break;
				case 'Jun':
					month_number = 5;
					month_str = '06';
					total_day = 30;
					break;
				case 'Jul':
					month_number = 6;
					month_str = '07';
					total_day = 31;
					break;
				case 'Aug':
					month_number = 7;
					month_str = '08';
					total_day = 31;
					break;
				case 'Sep':
					month_number = 8;
					month_str = '09';
					total_day = 30;
					break;
				case 'Oct':
					month_number = 9;
					month_str = '10';
					total_day = 31;
					break;
				case 'Nov':
					month_number = 10;
					month_str = '11';
					total_day = 30;
					break;
				case 'Dec':
					month_number = 11;
					month_str = '12';
					total_day = 31;
					break;
			}
			//prepare the data
			var arraydate = '';
			var myparams = 'dateRange=' + year + "-" + month_str + "-01|" + year + "-" + month_str + "-" + total_day + "&marker=" + $scope.results[0].marker;
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combination/';
			var req = $resource(adress + ':params' + '/json', { params: '@params' }, { query: { method: 'get', isArray: true } });
			req.query({ params: myparams }).$promise.then(function (data) {
				if (data.length > 0) {
					arraydate = data;
					$scope.datesonmonth = data;
					for (var i = 0; i < arraydate.length; i++) {
						if (year + "-" + month_str == arraydate[i].reference_date.substring(0, 7)) {

							var year_iter = arraydate[i].reference_date.substring(0, 4);
							var month_iter = arraydate[i].reference_date.substring(5, 7);
							var day_iter = arraydate[i].reference_date.substring(8, 10);
							data_date[Date.UTC(year_iter, month_iter - 1, day_iter) / 1000] = 1;
						}
					}
					$scope.dateselectedarray = [];
					//plot the calendar
					$("#cal-heatmap").html("");
					var cal = new CalHeatMap();
					cal.init({
						itemSelector: "#cal-heatmap",
						start: new Date(year, month_number),
						data: data_date,
						domain: "month",
						range: 1,
						subDomain: "x_day",
						cellSize: 30,
						subDomainTextFormat: "%d",
						legend: [1],
						legendColors: ["#F30000", "#5DBA76"],
						displayLegend: false,
						itemName: [''],
						domainLabelFormat: "%B %Y",
						onClick: function (date, nb) {
							if ($scope.shiftkeypressed) {
								$scope.dateselectedarray.push(date);
								if ($scope.dateselectedarray.length == 2) {
									if ($scope.dateselectedarray[1].getDate() < $scope.dateselectedarray[0].getDate()) {
										var tempdate = $scope.dateselectedarray[0];
										$scope.dateselectedarray[0] = $scope.dateselectedarray[1];
										$scope.dateselectedarray[1] = tempdate;
									}
									var nextdate = $scope.dateselectedarray[0].getDate();
									var i = 1;
									while (nextdate < $scope.dateselectedarray[1].getDate()) {
										var newdate = new Date($scope.dateselectedarray[0]);
										newdate.setDate(newdate.getDate() + i);
										var dateCalformat = new Date(newdate.getFullYear(), newdate.getMonth(), newdate.getDate());
										if (dateCalformat.getDate() < $scope.dateselectedarray[1].getDate()) {
											$scope.dateselectedarray.push(dateCalformat);
										}
										nextdate = dateCalformat.getDate();
										i++;
									}
								}
								cal.options.highlight = $scope.dateselectedarray;
								cal.highlight($scope.dateselectedarray);
								$scope.shiftkeypressed = false;
							} else {
								var index = -1;
								for (var i = 0; i < $scope.dateselectedarray.length; ++i) {
									if ($scope.dateselectedarray[i].valueOf() == date.valueOf()) {
										index = i;
										break;
									}
								}
								if (index > -1) {
									$scope.dateselectedarray.splice(index, 1);
								} else {
									$scope.dateselectedarray.push(date);
								}
								if ($scope.dateselectedarray.length != 0) {
									cal.options.highlight = $scope.dateselectedarray;
									cal.highlight($scope.dateselectedarray);
								} else {
									cal.options.highlight = $scope.dateselectedarray;
									cal.highlight(new Date(1000, 0, 1));
								}
							}
						}

					});
				}
			});

		};

		/*************************************************************************
		 * downloadselectedonmonth function to handle downloading files in calendar  
		 *************************************************************************/
		$scope.downloadselectedonmonth = function () {
			var clickdate = "";
			while ($scope.dateselectedarray.length > 0) {
				clickdate = $scope.convertDateFormat($scope.dateselectedarray[0]);
				for (var i = 0; i < $scope.datesonmonth.length; i++) {
					var mydate = $scope.datesonmonth[i].reference_date.substring(0, 10);
					if (clickdate == mydate) {
						if ($scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] != "") {
							var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
						} else {
							var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
						}
						var windowvalue = window.open(link, $scope.datesonmonth[i]['name']);

						if (windowvalue.name != "") {
							$scope.dateselectedarray.shift();
						}
					}
				}
			}
		};


		/****************************************************************
		* downloadwithdate function to handle downloading files in different format  
		****************************************************************/
		$scope.downloadwithdate = function (filename, format) {
			
			var clickdate = "";
			var script_text = "";

			while ($scope.dateselectedarray.length > 0) {
				console.log($scope.dateselectedarray);
				if (format === 'sh') { 

					for (var i = 0; i < $scope.dateselectedarray.length; i++){
						clickdate = $scope.convertDateFormat($scope.dateselectedarray[i]);
						for (var i = 0; i < $scope.datesonmonth.length; i++) {
							console.log($scope.datesonmonth);
							var mydate = $scope.datesonmonth[i].reference_date.substring(0, 10);
							if (clickdate == mydate) {
							
								if ($scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] != "") {
									var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
								} else {
									var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
								}

								script_text = script_text + "wget " + link + "\n";
								
							}
						}
						$scope.dateselectedarray.shift();
					}

					$scope.modalInstance.close();

				} else if (format === 'txt') { 

					for (var i = 0; i < $scope.dateselectedarray.length; i++){
						clickdate = $scope.convertDateFormat($scope.dateselectedarray[i]);
						for (var i = 0; i < $scope.datesonmonth.length; i++) {
							var mydate = $scope.datesonmonth[i].reference_date.substring(0, 10);
							if (clickdate == mydate) {
							
								if ($scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] != "") {
									var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['data_center']['data_center_structure']['directory_naming'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
								} else {
									var link = $scope.datesonmonth[i]['data_center']['protocol'] + "://" + $scope.datesonmonth[i]['data_center']['hostname'] + "/" + $scope.datesonmonth[i]['relative_path'] + "/" + $scope.datesonmonth[i]['name'];
								}

								script_text = script_text + link + "\r\n";
							}
						}
						$scope.dateselectedarray.shift();
					}

					$scope.modalInstance.close();

				}
			}
			
			var blob = new Blob([script_text], { type: 'text/plain' });
						if (window.navigator.msSaveOrOpenBlob) {//IE
							window.navigator.msSaveBlob(blob, filename);
						} else {
							var elem = window.document.createElement('a');
							elem.href = window.URL.createObjectURL(blob);
							elem.download = filename;
							document.body.appendChild(elem);
							elem.click();
							document.body.removeChild(elem);
						};

		};


		/**********************************************************************************************
		 * changedReferenceFromDate function allows to know what's users selected in "file date install"  
		 **********************************************************************************************/
		$scope.changedReferenceFromDate = function (date, date_from) {
			if (date == null) {
				$scope.filefromdateselected = date_from.substring(0, 10);
			} else {
				$scope.filefromdateselected = date.toISOString().slice(0, 10);
			}
			var params = 'marker=' + $scope.results[0].marker + '&date_range=' + $scope.filefromdateselected + ',' + $scope.filetodateselected;
			var arraydate = [];
			var arraydatetemp = [];
			var data = [];
			var new_station_object = {};
			var listDate = $scope.results[0].rinex_files_dates[0].split(",");
			for (var i = 0; i < listDate.length; i += 2) {
				var dt = new Date(listDate[i].slice(0, 10));
				var end = new Date(listDate[i + 1].slice(0, 10));
				while (dt <= end) {
					arraydatetemp.push(dt.toISOString().slice(0, 10));
					dt.setUTCDate(dt.getUTCDate() + 1);
				}
			}
			var startindex = arraydatetemp.indexOf($scope.filefromdateselected);
			if (startindex == -1) {
				var afterdatesarray = arraydatetemp.filter(function (d) {
					return new Date(d) - new Date($scope.filefromdateselected) > 0;
				});
				startindex = arraydatetemp.indexOf(afterdatesarray[0]);
			}
			var endindex = arraydatetemp.indexOf($scope.filetodateselected);
			if (endindex == -1) {
				var beforedatesarray = arraydatetemp.filter(function (d) {
					return new Date(d) - new Date($scope.filetodateselected) < 0;
				});
				endindex = arraydatetemp.indexOf(beforedatesarray[beforedatesarray.length - 1]);
			}
			arraydate = arraydatetemp.slice(startindex, endindex + 1);
			new_station_object['marker'] = $scope.results[0].marker;
			new_station_object['date_from'] = $scope.results[0].date_from;
			new_station_object['date_to'] = $scope.results[0].date_to;
			new_station_object['rinex_files_dates'] = arraydate;
			data.push(new_station_object);
			$scope.temporalchartforone(data, params);
			$scope.heatmap(data);
			$("#cal-heatmap").html("");
		};
		/*******************************************************************************************
		 * changedReferenceToDate function allows to know what's users selected in "file date remove"  
		 *******************************************************************************************/
		$scope.changedReferenceToDate = function (date) {
			if (date == null) {
				var lastDate = $scope.currentDateFormat();
				$scope.filetodateselected = lastDate.substring(0, 10);
			} else {
				$scope.filetodateselected = date.toISOString().slice(0, 10);
			}
			var params = 'marker=' + $scope.results[0].marker + '&date_range=' + $scope.filefromdateselected + ',' + $scope.filetodateselected;
			var arraydate = [];
			var arraydatetemp = [];
			var data = [];
			var new_station_object = {};
			var listDate = $scope.results[0].rinex_files_dates[0].split(",");
			for (var i = 0; i < listDate.length; i += 2) {
				var dt = new Date(listDate[i].slice(0, 10));
				var end = new Date(listDate[i + 1].slice(0, 10));
				while (dt <= end) {
					arraydatetemp.push(dt.toISOString().slice(0, 10));
					dt.setUTCDate(dt.getUTCDate() + 1);
				}
			}
			var startindex = arraydatetemp.indexOf($scope.filefromdateselected);
			if (startindex == -1) {
				var afterdatesarray = arraydatetemp.filter(function (d) {
					return new Date(d) - new Date($scope.filefromdateselected) > 0;
				});
				startindex = arraydatetemp.indexOf(afterdatesarray[0]);
			}
			var endindex = arraydatetemp.indexOf($scope.filetodateselected);
			if (endindex == -1) {
				var beforedatesarray = arraydatetemp.filter(function (d) {
					return new Date(d) - new Date($scope.filetodateselected) < 0;
				});
				endindex = arraydatetemp.indexOf(beforedatesarray[beforedatesarray.length - 1]);
			}
			arraydate = arraydatetemp.slice(startindex, endindex + 1);
			new_station_object['marker'] = $scope.results[0].marker;
			new_station_object['date_from'] = $scope.results[0].date_from;
			new_station_object['date_to'] = $scope.results[0].date_to;
			new_station_object['rinex_files_dates'] = arraydate;
			data.push(new_station_object);
			$scope.temporalchartforone(data, params);
			$scope.heatmap(data);
			$("#cal-heatmap").html("");
		};

		/*******************************************************************
		 * switchmap function allow to switch between satellite or simple map
		 *******************************************************************/
		$scope.switchmap = function () {
			var MyControl = L.Control.extend({
				options: {
					position: 'bottomleft'
				},
				onAdd: function (map) {
					var container = L.DomUtil.create('div', 'switchmap-control');
					container.innerHTML += '<button id="mapbuttonid" style="background-image:url(images/satellitemap.png);width:70px;height:70px;"></button>';
					$scope.imagemap = "satellitemap";
					$compile(container)($scope);
					return container;
				}
			});
			$scope.mymap.addControl(new MyControl());
			$('.switchmap-control').click(function (event) {
				if ($scope.imagemap == "satellitemap") {
					L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					document.getElementById("mapbuttonid").style.backgroundImage = "url(images/simplemap.png)";
					$scope.imagemap = "simplemap";
				} else {
					L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo($scope.mymap);
					document.getElementById("mapbuttonid").style.backgroundImage = "url(images/satellitemap.png)";
					$scope.imagemap = "satellitemap";
				}
			});
		};

		//T3

		/*************************************************************
		 * requestQualityCheck function allows to request quality check
		 *************************************************************/
		$scope.requestQualityCheck = function () {
			var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combinationT3/';
			var params = 'marker=' + $scope.results[0].marker + '&dateRange=' + $scope.fileT3fromdateselected + '|' + $scope.fileT3todateselected;
			var req = $resource(adress + ':myquery' + '/json', {
				myquery: '@myquery'
			}, {
				query: {
					method: 'get',
					isArray: true
				}
			});
			req.query({
				myquery: params
			}).$promise.then(function (data) {
				if (data.length > 0) {
					$scope.qualitycheckplot(data);
				}
			});
		};

		/*****************************************************************************
		 * initQualityCheck function allows to request quality check for initialization
		 ******************************************************************************/
		$scope.initQualityCheck = function (data) {
			if (data[0].rinex_files_dates.length > 0) {
				var arraydate = data[0].rinex_files_dates[0].split(",");
				if (arraydate[1] !== "null") {
					$scope.fileT3todateselected = arraydate[arraydate.length - 1].substring(0, 10);
					var datetemp = Date.UTC($scope.fileT3todateselected.substring(0, 4), $scope.fileT3todateselected.substring(5, 7) - 1, $scope.fileT3todateselected.substring(8, 10)) - (2 * 24 * 60 * 60 * 1000);
					$scope.fileT3fromdateselected = new Date(datetemp).toISOString().slice(0, 10);
					var adress = CONFIG.server + CONFIG.glassapi + '/webresources/files/combinationT3/';
					var params = 'marker=' + $scope.results[0].marker + '&dateRange=' + $scope.fileT3fromdateselected + '|' + $scope.fileT3todateselected;
					var req = $resource(adress + ':myquery' + '/json', {
						myquery: '@myquery'
					}, {
						query: {
							method: 'get',
							isArray: true
						}
					});
					req.query({
						myquery: params
					}).$promise.then(function (data) {
						if (data.length > 0) {
							$scope.qualitycheckplot(data);
						}
					});
				} else {
					console.log("!!! rinex_files_dates are null !!!");
				}
			}
		};

		/*********************************************************************************
		 * qualitycheckplot function allow to display the quality check plot for one station
		 *********************************************************************************/
		$scope.qualitycheckplot = function (data) {
			var obs_have = [];
			var epoch_compl = [];
			var cyc_slips = [];
			var mp1 = [];
			var mp2 = [];
			// var firstDate = '';
			var dateArray = [];
			var mp1_temp = [];
			var mp2_temp = [];
			var obs_have_L2 = [];
			// firstDate = data[0].reference_date.substring(0, 10);
			// firstDate = Date.UTC(firstDate.substring(0, 4), firstDate.substring(5, 7) - 1, firstDate.substring(8, 10));
			for (var i = 0; i < data.length; i++) {
				obs_have.push(data[i]["qc_report_summary"].obs_have * 100 / data[i]["qc_report_summary"].obs_expt);
				cyc_slips.push(data[i]["qc_report_summary"].cyc_slps);
				var dateToPush = data[i].reference_date.substring(0, 10);
 			    dateToPush = Date.UTC(dateToPush.substring(0, 4), dateToPush.substring(5, 7) - 1, dateToPush.substring(8, 10));
				dateArray.push(dateToPush);
				for (var j = 0; j < data[i]["qc_report_summary"]["qc_constellation_summary"].length; j++) {
					for (var k = 0; k < data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"].length; k++) {
						var path = data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_c"];
						if (((path[k]["obsnames"].name == "C1") || path[k]["obsnames"].name == "C1C" || path[k]["obsnames"].name == "C1X") && (data[i]["qc_report_summary"]["qc_constellation_summary"][j]["constellation"].name == "GPS")) {
							mp1_temp.push(path[k].cod_mpth);
						}
						if ((path[k]["obsnames"].name == "P2" || path[k]["obsnames"].name == "C2P" || path[k]["obsnames"].name == "C2W") && (data[i]["qc_report_summary"]["qc_constellation_summary"][j]["constellation"].name == "GPS")) {
							mp2_temp.push(path[k].cod_mpth);
						}
					}
					
					if (data[i]["qc_report_summary"]["qc_constellation_summary"][j]["constellation"].name == "GPS") {
						epoch_compl.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j].epo_have * 100 / data[i]["qc_report_summary"]["qc_constellation_summary"][j].epo_expt);
						for (var k = 0; k < data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"].length; k++) {
							var path = data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"];
							if (path[k].obsnames.name == "L2W") {
								obs_have_L2.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_have * 100 / data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_expt);
								break;
							} else if (path[k].obsnames.name == "L2X"){
								obs_have_L2.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_have * 100 / data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_expt);
								break;
							} else if (path[k].obsnames.name == "L2C") {
								obs_have_L2.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_have * 100 / data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_expt);
								break;
							} else if (path[k].obsnames.name == "L2P") {
								obs_have_L2.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_have * 100 / data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_expt);
								break;
							} else if (path[k].obsnames.name == "L2") {
								obs_have_L2.push(data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_have * 100 / data[i]["qc_report_summary"]["qc_constellation_summary"][j]["qc_observation_summary_l"][k].user_expt);
								break;
							} else {
								console.log("no data to display");
							}
						}
					}
				}
				if (mp1_temp.length > 0) {
					var sum = mp1_temp.reduce(function (a, b) {
						return a + b;
					});
					var avg = sum / mp1_temp.length;
					mp1.push(avg);
				}
				if (mp2_temp.length > 0) {
					var sum = mp2_temp.reduce(function (a, b) {
						return a + b;
					});
					var avg = sum / mp2_temp.length;
					mp2.push(avg);
				}
			}

			//Plot the chart
			Highcharts.chart('qcplaceholder', {

				chart: {
					type: 'column',
					zoomType: 'x'
				},
				title: {
					text: ""
				},
				legend: {
					enabled: true
				},
				xAxis: {
					categories: dateArray,
					type: 'datetime',
					labels: {
						format: '{value:%e %b %Y}'
					}
				},
				yAxis: [{
					scrollbar: {
						enabled: false
					},
					title: {
						text: ''
					}
				}

				],

				plotOptions: {

				},
				tooltip: {

				},
				series: [{
					name: 'Epoch completeness (%)',
					data: epoch_compl,
					// pointStart: firstDate,
					// pointInterval: 24 * 3600 * 1000, // one day
					dataLabels: {
						enabled: true,
						color: '#FFFFFF',
						y: 25, // 10 pixels down from the top
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				}, {
					name: 'Obs have L2 GPS (%)',
					data: obs_have_L2,
					// pointStart: firstDate,
					// pointInterval: 24 * 3600 * 1000, // one day
					dataLabels: {
						enabled: true,
						color: '#FFFFFF',
						format: '{point.y:.1f}', // one decimal
						y: 25, // 10 pixels down from the top
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				}, {
					name: 'Obs have all GNSS (%)',
					data: obs_have,
					// pointStart: firstDate,
					// pointInterval: 24 * 3600 * 1000, // one day
					dataLabels: {
						enabled: true,
						color: '#FFFFFF',
						format: '{point.y:.1f}', // one decimal
						y: 25, // 10 pixels down from the top
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				}, {
					name: 'Cyc slips (#)',
					data: cyc_slips,
					// pointStart: firstDate,
					// pointInterval: 24 * 3600 * 1000, // one day
					dataLabels: {
						enabled: true,
						color: '#FFFFFF',
						y: 25, // 10 pixels down from the top
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				}, {
					name: 'Mp1 (cm)',
					data: mp1,
					// pointStart: firstDate,
					// pointInterval: 24 * 3600 * 1000, // one day
					dataLabels: {
						enabled: true,
						color: '#FFFFFF',
						format: '{point.y:.1f}', // one decimal
						y: 25, // 10 pixels down from the top
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				}, {
					name: 'Mp2 (cm)',
					data: mp2,
					// pointStart: firstDate,
					// pointInterval: 24 * 3600 * 1000, // one day
					dataLabels: {
						enabled: true,
						color: '#FFFFFF',
						format: '{point.y:.1f}', // one decimal
						y: 25, // 10 pixels down from the top
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				}]

			});

		};


		/**********************************************************************************************
		 * changedT3ReferenceFromDate function allows to know what's users selected in "file date install"  
		 **********************************************************************************************/
		$scope.changedT3ReferenceFromDate = function (item) {
			if (item == undefined) {
				$scope.fileT3fromdateselected = undefined;
			} else {
				$scope.fileT3fromdateselected = $scope.convertDateFormat(item);
			}
		};
		/*******************************************************************************************
		 * changedT3ReferenceToDate function allows to know what's users selected in "file date remove"  
		 *******************************************************************************************/
		$scope.changedT3ReferenceToDate = function (item) {
			if (item == undefined) {
				$scope.fileT3todateselected = undefined;
			} else {
				$scope.fileT3todateselected = $scope.convertDateFormat(item);
			}
		};
	
	});
	