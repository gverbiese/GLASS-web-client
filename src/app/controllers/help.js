
'use strict';

const { sub } = require("angular-route");
const { scaleBand } = require("d3");

/**
 * copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
 *
 * Licence:XXXXXXXXXXXX
 *
 * Authors:
 *   Khai-Minh Ngo <ngo@geaozur.unice.fr>
 *   Arthur Fontaine <arthur.fontaine@geoazur.unice.fr>
 * Participates:
 *   Mathilde Vergnolle <mathilde.vergnolle@geaozur.unice.fr>
 *   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
 *   Lucie Rolland <lrolland@geaozur.unice.fr>
 *   Maurin Vidal <maurin.vidal@geaozur.unice.fr>
 *
 * @ngdoc function
 * @name glasswebuiApp.controller:HelpCtrl
 * @description
 * # HelpCtrl
 * Controller of the glasswebuiApp
 */
angular.module('glasswebuiApp')
    .controller('HelpCtrl', function ($scope, serviceHistory, uiGridConstants, serviceResource, $uibModal, $window, $resource, $routeParams, $templateCache, CONFIG, $http, $location, $q, /*$cookie,*/ $interval, $timeout, VIDEO, DOC) {
        /**
         * create a table of video or doc
         */
        $scope.createView = function(type) {
            var container = document.getElementById("helpcontainer");
            if(type === "VIDEO"){
                var title = document.createElement("p"); title.classList.add("titleHelp"); title.innerText = "Videos"; container.appendChild(title);
                var guicontainer = document.createElement("div");
                var ldccontainer = document.createElement("div");
                    var subtitle = document.createElement("p"); subtitle.classList.add("subtitleHelp"); subtitle.innerText = "Web Client"; guicontainer.appendChild(subtitle);
                    var subtitle2 = document.createElement("p");subtitle2.classList.add("subtitleHelp"); subtitle2.innerText = "Command Line Client"; ldccontainer.appendChild(subtitle2);
                    for(var y=0; y<VIDEO.table.length;y++){
                        var tab = document.createElement("div");tab.classList.add("tab");
                        for(var i=1;i<VIDEO.table[y].length-1;i++){
                            var value = document.createElement("p"); value.innerText = VIDEO.table[y][i];
                            tab.appendChild(value)
                        }
                        var value = document.createElement("p");
                        var link = document.createElement("a");link.innerText = "See Video"; link.href = VIDEO.table[y][3];link.target = "_blank";value.appendChild(link);
                        tab.appendChild(value);
                        if(VIDEO.table[y][0] === "GUI")
                            guicontainer.appendChild(tab)
                        else
                            ldccontainer.appendChild(tab)
                    }
                container.appendChild(guicontainer);
                container.appendChild(ldccontainer);
                var br = document.createElement("br"); container.appendChild(br);
            }else{
                var title = document.createElement("p"); title.classList.add("titleHelp"); title.innerText = "Documentation"; container.appendChild(title);
                var guicontainer = document.createElement("div");
                var ldccontainer = document.createElement("div");
                    var subtitle = document.createElement("p"); subtitle.classList.add("subtitleHelp"); subtitle.innerText = "Web Client"; guicontainer.appendChild(subtitle);
                    var subtitle2 = document.createElement("p");subtitle2.classList.add("subtitleHelp"); subtitle2.innerText = "Command Line Client"; ldccontainer.appendChild(subtitle2);
                    for(var y=0; y<DOC.table.length;y++){
                        var tab = document.createElement("div");tab.classList.add("tab");
                        for(var i=1;i<DOC.table[y].length-1;i++){
                            var value = document.createElement("p"); value.innerText = DOC.table[y][i];
                            tab.appendChild(value)
                        }
                        var value = document.createElement("p");
                        var link = document.createElement("a");link.innerText = "Download"; link.href = DOC.table[y][3];link.target = "_blank";value.appendChild(link);
                        tab.appendChild(value);
                        if(VIDEO.table[y][0] === "GUI")
                            guicontainer.appendChild(tab)
                        else
                            ldccontainer.appendChild(tab)
                    }
                container.appendChild(guicontainer);
               // var br = document.createElement("br"); container.appendChild(br);
                container.appendChild(ldccontainer);
            }
        }


        $scope.createView("VIDEO");
        $scope.createView("DOC");



        /*************************************************************
         * loadingdialog function allows to open a modal with a spinner 
         **************************************************************/
        $scope.loadingdialog = function () {
            return $scope.modalInstance = $uibModal.open({
                templateUrl: 'views/modalLoading.html',
                size: 'm',
                scope: $scope,
                windowClass: 'center-modal'
            });
        };


        /******************************************************************************************
         * opendialog function allows to open a modal when the users click on "request query" button 
         *******************************************************************************************/
        $scope.opendialog = function () {
            return $scope.modalInstance = $uibModal.open({
                templateUrl: 'views/modalRequest.html',
                size: 'lg',
                scope: $scope,
                windowClass: 'center-modal'
            });
        };

        /***************************************
         * cancel function allows to cancel modal
         ****************************************/
        $scope.cancel = function () {
            $scope.modalInstance.dismiss("cancel");
        };

        /*******************************
         * messageinfogrid alert message
         ********************************/
        $scope.messageinfogrid = {
            textAlert: "You have to select the files in the grid before to download",
            mode: 'info'
        };
        /*******************************************************
         * messageinfonodata variable to advanced error message
         ********************************************************/
        $scope.messageinfonodata = {
            textAlert: "No data available",
            mode: 'info'
        };

        /*******************************************************************
         * openmessage function allows to open a modal when there's an error
         *******************************************************************/
        $scope.openmessage = function (message) {
            return $scope.modalInstance = $uibModal.open({
                templateUrl: 'views/messageBox.html',
                scope: $scope,
                size: 'lg',
                resolve: {
                    data: function () {
                        $scope.messageinfo = message;
                        return $scope.messageinfo;
                    }
                }
            });
        };
        /**************************************
         * close function allows to close modal
         ***************************************/
        $scope.close = function () {
            $scope.modalInstance.close();
        };

    });