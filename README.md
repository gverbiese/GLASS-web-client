# Glass Web Ui

Glass Web client is a graphical user interface for the Data and Metadata provide by the API **GLASS Framework**.
Glass Web client is developed for EPOS-IP WP10.

**Author:**

* Guillaume Verbiese (guillaume.verbiese@oca.eu)

**Former Contributors**
* Khai-Minh NGO (Khai-Minh.NGO@geoazur.unice.fr)
* Arthur Fontaine (arthur.fontaine@geoazur.unice.fr)

## Installation

1. Download the war file : "glasswebui.war" and config.js file on Gitlab at <https://gitlab.com/gpseurope/GLASS-web-client/tree/master/dist> 

2. Open config.js and change the line : server:"http://\<IP\>:\<port\>/" with you server's address and port. Replace <mail> by the mail of the node contact

<!-- 3. If you want to configure Google Analytics, please change the value **tracker_id** from config.js to yours (You need to create a google analytics account first). -->

3. Open a terminal and execute the command:  "jar -uvf glasswebui.war config.js" to update in the war file with your new config.js

4. Deploy the war file on Glassfish

5. Check the addresss : "http://\<IP\>:\<port\>/glasswebui"

To rename the clientname:"glasswebui" by another one for example "french_node":

At step 4) before to deploy your war file, you have to rename also your war file "glasswebui.war" to "french_node.war"

and at step 5) Check with the browser "http://\<IP\>:\<port\>/french_node" 

## Documentation

To install from the source see the document **2021-02-26_technical_doc_web_client.pdf**

For the functionality, see the document **20161110_Specification_web_client.pdf**

## Acknowledgments
* This project has received funding from the European Union's Horizon 2020 research and innovation programm under grant agreement N° 676564

## Development knowledge

See the document **2021-02-26_technical_doc_web_client.pdf**

There are two files '*.config.js' which allow webpack to build the application for local node or for the Data Gateway.

* Local node
1. build : npm run build
2. web server : npm run server

* DGW
1. build : npm run dgwBuild
2. web server : npm run dewServer

The config.js file load a different *index.html* where the *body* has a different *id*, and the logos are different.
Inside the code, the differences are made with the function **$scope.isDgw()** but every files stays the same in both versions.
