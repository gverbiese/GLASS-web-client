# Newest Release

## 1.0 - 29/09/2020

----------------------------------------- 

* Build tools have been updated (from bower and grunt to webpack)

* Fixed bug with the network selector (some network were impossible to interact with)

* The station are now ordered by name at the start, you can choose to change by clicking on the column name you want

* The stations names are now displayed with the 9 characters

* Fixed the zoom issues, you now need to press CTRL+SCROLL to zoom in or out. 

* Advanced search is now a bit more esthetic
