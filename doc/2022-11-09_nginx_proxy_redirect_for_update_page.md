#  <span style='color: #85c1e9;'>**Nginx proxy redirect for update page**</span>

## <span style='color:#a2d9ce;'>**1. Modification of the html template**</span>

If necessary, the first step is to update the html template representing the page to display while the service is disabled for update.


### <span style="color:#d7bde2;">**Connect to dgw.unice.fr**</span>

Connect in ssh as root to the server dgw, enter the corresponding password.

```sh
ssh username@server_adress

```

### <span style="color:#d7bde2;">**Go to the path containing the html templates**</span>

***Go to :***

```sh
cd /usr/share/nginx/html
```
### <span style="color:#d7bde2;">**Ajust the template**</span>

Adjust the temp.html file if necessary with the nano command. If the changes are too large, it's possible to create the new page locally and send it to the server using the scp command. 

***As a reminder:***

```sh
scp /path/from/local/file/to/copy/ username@server_address:/path/to/copy/file/on/server
```

## <span style='color:#a2d9ce;'>**2. Modification of the configuration file**</span>


### <span style="color:#d7bde2;">**Go to the path containing the html templates**</span>

***Go to:***

```sh
cd /etc/nginx/conf.d
```

### <span style="color:#d7bde2;">**Modify the configuration file server.conf**</span>

***Open the file with nano:***

```sh
nano server.conf
```

***Comment out the following lines:***

```sh
include conf.d/proxy.inc
```

***to :***

```sh
#include conf.d/proxy.inc
```

***Uncomment the lines allowing the redirection:***

```sh
#if ($request_uri != /temp.html) { return 302 temp.html; }
```

***To:***

```sh
if ($request_uri != /temp.html) { return 302 temp.html; }
```

## <span style='color:#a2d9ce;'>**3. Reload Nginx**</span>

***For reload Nginx, launch the command:***

```sh
systemctl reload nginx
```

## <span style='color:#a2d9ce;'>**4. Check that the redirect works**</span>


Go to https://gnssdata-epos.oca.eu/ and check that the rendering corresponds to your expectations.

## <span style='color:#a2d9ce;'>**5. Bring the site back online**</span>



### <span style="color:#d7bde2;">**Follow the steps of Chapter 2 in reverse**</span>

**To know:**


***Open the file with nano:***

```sh
nano server.conf
```

***Uncomment out the following lines:***

```sh
#include conf.d/proxy.inc
```

***to :***

```sh
include conf.d/proxy.inc
```

***Comment the lines allowing the redirection:***

```sh
if ($request_uri != /temp.html) { return 302 temp.html; }
```

***To:***

```sh
#if ($request_uri != /temp.html) { return 302 temp.html; }
```

### <span style="color:#d7bde2;">**Reload Nginx**</span>
***Launch the command:***

```sh
systemctl reload nginx
```

### <span style="color:#d7bde2;">**Check that the redirect works**</span>

Go to https://gnssdata-epos.oca.eu/ and check that the rendering corresponds to your expectations.